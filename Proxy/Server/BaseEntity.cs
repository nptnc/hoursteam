﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Server {
    public class BaseEntity {
        public Vector3 position;
        public Vector3 rotation;
    }
}

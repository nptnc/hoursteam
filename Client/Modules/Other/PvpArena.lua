local rs = game:GetService("RunService")
local UserInputService = game:GetService("UserInputService")

local module = {}

module.setModules = function(framework)
    networker = framework("Networker")
    networkPlayer = framework("NetworkPlayer")
    networkEntity = framework("NetworkEntity")
end

module.inRound = false

-- this codes pretty bad, but it works so who cares if its messy
local mapSize = 100
local startRound = function(sky,cf)
    if module.inRound then
        return
    end
    module.inRound = true

    setthreadidentity(2)
    local current = getrenv()._G.Map
    current:Die()

    local mapName = "Arena"
    getrenv()._G.LoadMap(mapName)

    local map = workspace[mapName]

    -- this is for hellion so he doesnt just get stuck on the wall
    local walls = 25
    local wallHeight = 50

    local preview = function(i)
        local angle = i/walls * 360
        local lookVector = CFrame.Angles(0,math.rad(angle),0)
        return lookVector
    end
    for i = 1,walls do
        local nextLookVector = preview(i+1)
        
        local logic = CFrame.new(0,wallHeight/2-4,-(mapSize/2+1))
        local nextCF = nextLookVector*logic
        
        local wall = Instance.new("Part")
        wall.Parent = map
        wall.CFrame = preview(i)*logic
        wall.Size = Vector3.new((wall.CFrame.Position - nextCF.Position).Magnitude,50,1)
        wall.Anchored = true
        wall.CanCollide = true
        wall.Transparency = 1
    end

    local loops = {}

    local localEntity = getrenv()._G.Entities[1]
    localEntity.RootPart.CFrame = cf

    if localEntity.Name == "Class1338" then
        --localEntity.Cooldowns.SkillCar.MaxCooldown = 10
    end

    map.Spawn.Shape = Enum.PartType.Cylinder
    map.Spawn.Size = Vector3.new(4,mapSize*2,mapSize*2)
    map.Spawn.Rotation = Vector3.new(0,0,90)
    
    local arenaSizeDisplayer = map.Spawn:Clone()
    arenaSizeDisplayer.Parent = map
    arenaSizeDisplayer.Size = Vector3.new(4,mapSize,mapSize)
    arenaSizeDisplayer.Position += Vector3.new(0,0.1,0)
    arenaSizeDisplayer.Transparency = 0.7
    arenaSizeDisplayer.Color = Color3.fromRGB(255,255,255)
    arenaSizeDisplayer.Material = Enum.Material.ForceField
    arenaSizeDisplayer.CanCollide = false

    map.Baseplate.Size = Vector3.new(10,mapSize*2,mapSize*2)
    map.Rocks:Destroy()
    map.Model:Destroy()
    map.Twee:Destroy()

    local newGui = Instance.new("ScreenGui")
    newGui.Parent = game.Players.LocalPlayer.PlayerGui
    newGui.IgnoreGuiInset = true
    newGui.DisplayOrder = 999

    map:GetPropertyChangedSignal("Parent"):Connect(function()
        module.inRound = false
        print("MAP DESTROYED")
        for _,loop in loops do
            loop:Disconnect()
        end
        if newGui then
            newGui:Destroy()
        end
    end)
    
    local frame = Instance.new("Frame")
    frame.Parent = newGui
    frame.Size = UDim2.new(1,0,0.2,0)
    frame.Position = UDim2.new(0.5,0,0.74,0)
    frame.AnchorPoint = Vector2.new(0.5,0.5)
    frame.BackgroundTransparency = 1
    
    local uiListLayout = Instance.new("UIListLayout")
    uiListLayout.Parent = frame
    uiListLayout.Padding = UDim.new(0.05,0)
    uiListLayout.HorizontalAlignment = Enum.HorizontalAlignment.Center
    uiListLayout.VerticalAlignment = Enum.VerticalAlignment.Center
    uiListLayout.FillDirection = Enum.FillDirection.Horizontal
    
    local ts = game:GetService("TweenService")
    local talentUI = {}
    local createTalent = function(talentConfig,delayness)
        delayness = delayness or 0
        local newFrame = Instance.new("Frame")
        newFrame.Parent = frame
        newFrame.Size = UDim2.new(0.08,0,0.7,0)
        newFrame.AnchorPoint = Vector2.new(0.5,0.5)
        newFrame.Position = UDim2.new(0.5,0,0.5,0)
        newFrame.BackgroundTransparency = 1
        table.insert(talentUI,newFrame)
        
        local image = Instance.new("ImageLabel")
        image.Parent = newFrame
        image.Name = "Image"
        image.Size = UDim2.new(1,0,1,0)
        image.Image = talentConfig.Image
        image.Position = UDim2.new(0.5,0,0.5,0)
        image.AnchorPoint = Vector2.new(0.5,0.5)
        image.BackgroundTransparency = 1
        image.ImageTransparency = 1
        ts:Create(image,TweenInfo.new(0.3,Enum.EasingStyle.Quad,Enum.EasingDirection.In),{ImageTransparency = 0}):Play()
    
        local textLabel = Instance.new("TextLabel")
        textLabel.Parent = image
        textLabel.Name = "Title"
        textLabel.Text = talentConfig.Title
        textLabel.Size = UDim2.new(1,0,0.2,0)
        textLabel.TextScaled = true
        textLabel.AnchorPoint = Vector2.new(0.5,1)
        textLabel.Position = UDim2.new(0.5,0,0,0)
        textLabel.BackgroundTransparency = 1
        textLabel.TextColor3 = Color3.fromRGB(255,255,255)
        textLabel.TextTransparency = 1
        textLabel.TextXAlignment = Enum.TextXAlignment.Center
        textLabel.TextYAlignment = Enum.TextYAlignment.Center
        ts:Create(textLabel,TweenInfo.new(0.3,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),{TextTransparency = 0}):Play()
        
        local description = Instance.new("TextLabel")
        description.Parent = image
        description.Name = "Description"
        description.Text = talentConfig.Desc
        description.Size = UDim2.new(0.8,0,0.8,0)
        description.TextScaled = true
        description.AnchorPoint = Vector2.new(0.5,0.5)
        description.Position = UDim2.new(0.5,0,0.5,0)
        description.BackgroundTransparency = 1
        description.TextColor3 = Color3.fromRGB(255,255,255)
        description.TextTransparency = 1
        description.TextXAlignment = Enum.TextXAlignment.Center
        description.TextYAlignment = Enum.TextYAlignment.Center
        ts:Create(description,TweenInfo.new(0.3,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),{TextTransparency = 0}):Play()       

        local uiOutline = Instance.new("UIStroke")
        uiOutline.Parent = textLabel
        uiOutline.Transparency = 1
        uiOutline.Name = "outline"
        ts:Create(uiOutline,TweenInfo.new(0.3,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),{Transparency = 0}):Play()       
        
        local uiOutlineDesc = Instance.new("UIStroke")
        uiOutlineDesc.Parent = description
        uiOutlineDesc.Name = "outline"
        uiOutlineDesc.Transparency = 1
        ts:Create(uiOutlineDesc,TweenInfo.new(0.3,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),{Transparency = 0}):Play()       
    end

    local talentConfig = require(game.ReplicatedStorage.TalentConfig)
    local targetConfig = talentConfig[localEntity.Name]

    local continueText = nil
    local gainedTalents = {}
    for i = 1,3 do
        local selection = {}
        for index,_ in targetConfig do
            if table.find(gainedTalents,index) then
                continue
            end
            table.insert(selection,index)
        end
        local selected = selection[math.random(1,#selection)]
        table.insert(gainedTalents,selected)
        getrenv()._G.AddTalent(selected)

        task.delay(3,function()
            createTalent(targetConfig[selected],(i-1)*0.5)
        end)
    end
    task.delay(3,function()
        continueText = Instance.new("TextLabel")
        continueText.Parent = newGui
        continueText.Name = "Continue"
        continueText.Text = "press space to continue..."
        continueText.Size = UDim2.new(1,0,0.2,0)
        continueText.TextScaled = false
        continueText.AnchorPoint = Vector2.new(0.5,1)
        continueText.Position = UDim2.new(0.5,0,0.95,0)
        continueText.BackgroundTransparency = 1
        continueText.TextColor3 = Color3.fromRGB(255,255,255)
        continueText.TextTransparency = 1
        continueText.TextXAlignment = Enum.TextXAlignment.Center
        continueText.TextYAlignment = Enum.TextYAlignment.Center
        continueText.TextSize = 24

        local uiOutline = Instance.new("UIStroke")
        uiOutline.Parent = continueText
        uiOutline.Transparency = 1
        uiOutline.Name = "outline"

        ts:Create(uiOutline,TweenInfo.new(0.3,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),{Transparency = 0}):Play()
        ts:Create(continueText,TweenInfo.new(0.3,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),{TextTransparency = 0}):Play()
    end)

    local uisloop;uisloop = UserInputService.InputBegan:Connect(function(i, g)
        if i.KeyCode ~= Enum.KeyCode.Space then
            return
        end
        for _,talentFrame in talentUI do
            local image = talentFrame.Image
            local textLabel = image.Title
            local description = image.Description
            local uiOutline = textLabel.outline
            local uiOutlineDesc = description.outline
            ts:Create(image,TweenInfo.new(1,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),{Position = UDim2.new(0.5,0,1,0),ImageTransparency = 1}):Play()
            ts:Create(textLabel,TweenInfo.new(1,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),{TextTransparency = 1}):Play()
            ts:Create(description,TweenInfo.new(1,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),{TextTransparency = 1}):Play()
            ts:Create(uiOutline,TweenInfo.new(1,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),{Transparency = 1}):Play()
            ts:Create(uiOutlineDesc,TweenInfo.new(1,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),{Transparency = 1}):Play()
        end
        ts:Create(continueText,TweenInfo.new(0.1,Enum.EasingStyle.Linear,Enum.EasingDirection.Out),{TextTransparency = 1}):Play()
        ts:Create(continueText.outline,TweenInfo.new(0.1,Enum.EasingStyle.Linear,Enum.EasingDirection.Out),{Transparency = 1}):Play()
        task.delay(1,function()
            newGui:Destroy()
            newGui = nil
        end)
        uisloop:Disconnect()
    end)
    table.insert(loops,uisloop)

    local pvpChanges = function(targetEntity)
        if targetEntity.Name == "Class5318008" then
            targetEntity.NoobSpawnCooldown = 99
        end
    end
    
    table.insert(loops,rs.Heartbeat:Connect(function(dt)
        pvpChanges(localEntity)
        for _,player_ in networkPlayer.getPlayers() do
            if not player_.entity then
                continue
            end
            pvpChanges(player_.entity)
        end

        local direction = localEntity.RootPart.Position
        if direction.Magnitude > mapSize/2 then
            local humrp = localEntity.RootPart
            humrp.CFrame = CFrame.new(direction.Unit*mapSize/2)*CFrame.Angles(math.rad(humrp.Rotation.X),math.rad(humrp.Rotation.Y),math.rad(humrp.Rotation.Z))
        end
    end))

    game.Lighting:FindFirstChildWhichIsA("Sky"):Destroy()
    game.ReplicatedStorage.Skies[sky]:Clone().Parent = game.Lighting

    networkEntity.disposeAll()
    for _,entity in getrenv()._G.Entities do
        if entity.IsPlayer then
            continue
        end
        networkEntity.destroyEntity(entity)
    end

    local playergui = game.Players.LocalPlayer.PlayerGui
    playergui.AllGui.MapText.Text = "COMPETITIVE ARENA"
    playergui.AllGui.StageText.Text = "STAND OFF"
end

module.start = function()
    local skies = {
        "Pink",
        "Purple",
    }

    local random = Random.new()
    local chosenSky = skies[math.random(1,#skies)]
    local teleportCF = CFrame.new(random:NextNumber(-mapSize/2,mapSize/2),0,random:NextNumber(-mapSize/2,mapSize/2))
    startRound(chosenSky,teleportCF)
end

return module
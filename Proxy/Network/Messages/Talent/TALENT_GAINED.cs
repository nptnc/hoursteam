﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;

namespace Proxy.Network.Messages {
    /// <summary>
    /// occurs when a talent is gained by a player
    /// </summary>
    public class TALENT_GAINED : NetworkMessage {
        public override byte MessageId() {
            return MessageIds.TALENT_GAINED;
        }

        public override void RobloxReceived(RobloxReader data) {
            string talentName = data.ReadString();

            NetworkWriter writer = new();
            writer.Write(talentName);

            NetworkSender.SendToServer(MessageId(),SendType.Reliable,writer.Create());
        }

        public override void ReceivedServer(SteamId by,byte[] data) {
            NetworkReader reader = new(data);
            string talentName = reader.ReadString();

            Player? player = PlayerManager.GetPlayerBySteamId(by);
            if (player == null) {
                return;
            }

            Console.WriteLine($"{player.robloxUserId} gained talent {talentName} [server]");

            NetworkWriter writer = new();
            writer.Write(player.smallId);
            writer.Write(talentName);

            NetworkSender.SendToClients(MessageId(), writer.Create(), SendType.Reliable, new ulong[] { by });
        }

        public override void ReceivedClient(byte[] data) {
            NetworkReader reader = new(data);
            byte id = reader.ReadByte();
            string talentName = reader.ReadString();

            Player? player = PlayerManager.GetPlayerByByteId(id);
            if (player == null) {
                return;
            }

            Console.WriteLine($"{player.robloxUserId} gained talent {talentName}");

            RobloxWriter writer = new(MessageId());
            writer.WriteString(id.ToString());
            writer.WriteString(talentName.ToString());

            NetworkSender.SendToRoblox(writer);
        }
    }
}

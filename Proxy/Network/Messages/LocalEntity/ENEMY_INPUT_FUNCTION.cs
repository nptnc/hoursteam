﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;

namespace Proxy.Network.Messages.Enemies
{
    /// <summary>
    /// for enemy (HOST ONLY) attacks, also known as inputs
    /// a "host entity" is a entity you can play as
    /// sen (developer of hours) didnt use input functions on non host entities
    /// which makes most attacks unsyncable
    /// </summary>
    public class ENEMY_INPUT_FUNCTION : NetworkMessage
    {
        public override byte MessageId()
        {
            return MessageIds.ENEMY_INPUT_FUNCTION;
        }

        // higher means loss of precision which i guess is fine
        float positionCap = 250;
        float rotationCap = 180; // roblox euler angles is between -180 and 180
        float cameraPositionCap = 20;

        public override void RobloxReceived(RobloxReader data)
        {
            int entityId = data.ReadInt();
            string input = data.ReadString();
            Vector3 position = data.ReadVector3();
            float rotation = data.ReadFloat();
            Vector3 camposition = data.ReadVector3();
            Vector3 camrotation = data.ReadVector3();

            NetworkWriter writer = new NetworkWriter();
            writer.Write((byte)entityId);
            writer.Write((short)(position.X / positionCap * short.MaxValue));
            writer.Write((short)(position.Y / positionCap * short.MaxValue));
            writer.Write((short)(position.Z / positionCap * short.MaxValue));
            writer.Write((sbyte)(camposition.X / cameraPositionCap * sbyte.MaxValue));
            writer.Write((sbyte)(camposition.Y / cameraPositionCap * sbyte.MaxValue));
            writer.Write((sbyte)(camposition.Z / cameraPositionCap * sbyte.MaxValue));
            writer.Write((short)(rotation / rotationCap * short.MaxValue));
            writer.Write((short)(camrotation.X / rotationCap * short.MaxValue));
            writer.Write((short)(camrotation.Y / rotationCap * short.MaxValue));
            writer.Write((short)(camrotation.Z / rotationCap * short.MaxValue));
            writer.Write(input);

            NetworkSender.SendToServer(MessageId(), SendType.Reliable, writer.Create());
        }

        public override void ReceivedServer(SteamId by, byte[] data)
        {
            if (by != SteamClient.SteamId)
                return; // host only, might make it entity ownership only though

            NetworkReader reader = new(data);
            byte entityId = reader.ReadByte();
            short positionX = reader.ReadShort();
            short positionY = reader.ReadShort();
            short positionZ = reader.ReadShort();
            short campositionX = reader.ReadShort();
            short campositionY = reader.ReadShort();
            short campositionZ = reader.ReadShort();
            short rotation = reader.ReadShort();
            short camrotationX = reader.ReadShort();
            short camrotationY = reader.ReadShort();
            short camrotationZ = reader.ReadShort();
            string input = reader.ReadString();

            Logger.Log($"entity {entityId} {input}");

            NetworkWriter writer = new NetworkWriter();
            writer.Write(entityId);
            writer.Write(positionX);
            writer.Write(positionY);
            writer.Write(positionZ);
            writer.Write(campositionX);
            writer.Write(campositionY);
            writer.Write(campositionZ);
            writer.Write(rotation);
            writer.Write(camrotationX);
            writer.Write(camrotationY);
            writer.Write(camrotationZ);
            writer.Write(input);

            NetworkSender.SendToClients(MessageId(), writer.Create(), SendType.Reliable, new ulong[] { by });
        }

        public override void ReceivedClient(byte[] data)
        {
            NetworkReader reader = new(data);
            byte entityId = reader.ReadByte();
            float positionX = (float)reader.ReadShort() / short.MaxValue * positionCap;
            float positionY = (float)reader.ReadShort() / short.MaxValue * positionCap;
            float positionZ = (float)reader.ReadShort() / short.MaxValue * positionCap;
            float camPositionX = (float)reader.ReadShort() / short.MaxValue * cameraPositionCap;
            float camPositionY = (float)reader.ReadShort() / short.MaxValue * cameraPositionCap;
            float camPositionZ = (float)reader.ReadShort() / short.MaxValue * cameraPositionCap;
            float rotation = (float)reader.ReadShort() / short.MaxValue * rotationCap;
            float camRotationX = (float)reader.ReadShort() / short.MaxValue * rotationCap;
            float camRotationY = (float)reader.ReadShort() / short.MaxValue * rotationCap;
            float camRotationZ = (float)reader.ReadShort() / short.MaxValue * rotationCap;
            string input = reader.ReadString();

            RobloxWriter writer = new(MessageId());
            writer.WriteString(entityId.ToString());
            writer.WriteString(input);
            writer.WriteVector3(new Vector3(positionX, positionY, positionZ));
            writer.WriteString(rotation.ToString());
            writer.WriteVector3(new Vector3(camPositionX, camPositionY, camPositionZ));
            writer.WriteVector3(new Vector3(camRotationX, camRotationY, camRotationZ));

            NetworkSender.SendToRoblox(writer);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Hours
{
    internal static class HoursEntityIds
    {
        // i dont want to use an enum.
        public const string
            drifter = "Class5318008",
            hellion = "Class1338",
            hotrash = "Class0",
            dreamer = "Class8",
            bloxxer = "Class999",
            buffoon = "ClassDuck",
            equinox = "Class01",
            invader = "Class1",
            prophet = "Class2",
            witness = "Class3",
            artisan = "Class4",
            vestige = "Class5",
            visitor = "Class6",
            subject = "Class7",
            parting = "Class9";

        public static string[] getAll() {
            return new string[] { drifter, hellion, hotrash, dreamer, bloxxer, buffoon, equinox, invader, prophet, witness, artisan, vestige, visitor, subject, parting };
        }
    }
}

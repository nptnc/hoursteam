﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;

namespace Proxy.Network.Messages
{
    /// <summary>
    /// verbatim
    /// definition: "in exactly the same words as were used originally."
    /// nptnc said verbatim, 'Entity Health Changed'
    /// </summary>
    public class ENEMY_HEALTH_CHANGE : NetworkMessage
    {
        public override byte MessageId()
        {
            return MessageIds.ENEMY_HEALTH_CHANGE;
        }

        public override void RobloxReceived(RobloxReader data)
        {
            float entityId = data.ReadFloat();
            float health = data.ReadFloat();

            NetworkWriter writer = new();
            writer.Write((ushort)entityId);
            writer.Write((ushort)health);

            NetworkSender.SendToServer(MessageId(), SendType.Reliable, writer.Create());
        }

        public override void ReceivedServer(SteamId by, byte[] data)
        {
            if (by != SteamClient.SteamId)
                return; // they arent the host

            NetworkReader reader = new(data);
            ushort entityId = reader.ReadUShort();
            ushort health = reader.ReadUShort();

            Entity? entity = EntityManager.GetEntityById(entityId);
            if (entity == null) {
                Logger.Log($"entity {entityId} is null can't set the health!, (sending to clients)");
            }
            else {
                if (entity.dead)
                    return;
                if (health <= 0)
                    entity?.Died();
            }

            Logger.Log($"{entityId} health is {health}!");

            NetworkSender.SendToClients(MessageId(), data, SendType.Reliable, new ulong[] { by });
        }

        public override void ReceivedClient(byte[] data)
        {
            NetworkReader reader = new(data);
            ushort id = reader.ReadUShort();
            ushort health = reader.ReadUShort();

            RobloxWriter writer = new(MessageId());
            writer.WriteString(id.ToString());
            writer.WriteString(health.ToString());

            NetworkSender.SendToRoblox(writer);
        }
    }
}
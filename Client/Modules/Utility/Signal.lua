local Connection = {}
Connection.__index = Connection

Connection.new = function(func,id,sig)
	local self = setmetatable({},Connection)
	self.callback = func
	self.id = id
	self.sig = sig
	return self
end

function Connection:Disconnect()
	self.sig._connections[self.id] = nil
end

Connection.Destroy = Connection.Disconnect

local Signal = {}
Signal.__index = Signal

Signal.new = function()
	local self = setmetatable({},Signal)
	self._connections = {}
	self._connectionIndex = 0
	return self
end

function Signal:Connect(func)
	self._connections[self._connectionIndex] = Connection.new(func,self._connectionIndex,self)
	self._connectionIndex += 1
	return self._connections[self._connectionIndex-1]
end

function Signal:Fire(...)
	for _,con in self._connections do
		con.callback(...)
	end
end

function Signal:Destroy()
	
end

return Signal
﻿using Proxy.Server;
using Steamworks;
using Steamworks.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Network {
    internal class QueuedLocalMessage {
        internal int messageId;
        internal byte[] data;
        internal bool server;

        internal QueuedLocalMessage(int messageId, byte[] data, bool server) {
            this.messageId = messageId;
            this.data = data;
            this.server = server;
        }
    }

    internal static class NetworkSender {
        internal static void SendToServer(byte messageId, SendType sendType, byte[] data) {
            if (NetworkHandler.clientSocket == null)
                return;
            if (NetworkHandler.socket != null) {
                NetworkHandler.HandleMessage(messageId, data, true, SteamClient.SteamId);
                return;
            }
            byte[] newData = new byte[data.Length + 1];
            newData[0] = messageId;
            for (int i = 0; i < data.Length; i++) {
                newData[i + 1] = data[i];
            }
            NetworkHandler.clientSocket.Connection.SendMessage(newData, sendType);
        }

        internal static void SendToClient(byte messageId,ulong id, SendType sendType, byte[] data) {
            if (NetworkHandler.socket == null)
                return;
            byte[] newData = new byte[data.Length + 1];
            newData[0] = messageId;
            for (int i = 0; i < data.Length; i++) {
                newData[i + 1] = data[i];
            }
            if (id == SteamClient.SteamId) {
                NetworkHandler.HandleMessage(messageId, data, false);
                return;
            }
            NetworkHandler.socket.connectedSteamIds[id].SendMessage(newData, sendType);
        }

        internal static void SendToClients(byte messageId,byte[] data, SendType sendType, ulong[]? except = null) {
            if (NetworkHandler.socket == null)
                return;

            foreach (var pair in NetworkHandler.socket.connectedSteamIds) {
                if (except != null && except.Contains(pair.Key))
                    continue;
                SendToClient(messageId,pair.Key,sendType,data);
            }
        }

        internal static void SendToRegisteredClients(byte messageId, byte[] data, SendType sendType, ulong[]? except = null) {
            if (NetworkHandler.socket == null)
                return;

            foreach (var pair in PlayerManager.players) {
                if (except != null && except.Contains(pair.steamId))
                    continue;
                SendToClient(messageId, pair.steamId, sendType, data);
            }
        }

        internal static void SendToRoblox(RobloxWriter writer) {
            Main.websocket?.Send(writer.Get());
        }
    }
}

﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;
using Proxy.Server.Stage;
using Proxy.Network.DataTypes;

namespace Proxy.Network.Messages
{
    /// <summary>
    /// every time a player registers this gets sent to them
    /// the client stores this data
    /// when the game tries to spawn an entity it replaces the entity type with the corresponding server entity type
    /// this data on the server gets set in Proxy/Server/Stage
    /// </summary>
    public class ENEMY_STAGE_DATA : NetworkMessage
    {
        public override byte MessageId()
        {
            return MessageIds.ENEMY_STAGE_DATA;
        }

        public override void RobloxReceived(RobloxReader data) {
            return;
        }

        public override void ReceivedServer(SteamId by, byte[] data)
        {
            return;
        }

        public override void ReceivedClient(byte[] data)
        {
            StageData stageData = new(data);

            RobloxWriter writer = new(MessageId());
            writer.WriteString(stageData.fullStageName!);
            writer.WriteString(stageData.entityCount.ToString()!);
            for (int i = 0; i < stageData.entityCount; i++) {
                writer.WriteString(stageData.entityData![i].entityType);
                writer.WriteVector3(stageData.entityData[i].position);
            }

            NetworkSender.SendToRoblox(writer);
        }
    }
}
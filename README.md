# HourSteam
An **unofficial** multiplayer mod for [roblox hours](https://www.roblox.com/games/5732973455/HOURS) using [websockets](https://github.com/statianzo/Fleck) and [steam networking](https://github.com/Facepunch/Facepunch.Steamworks)    

This is a "cheat" that modifies HOURS a ton to support multiplayer.    
Endless is the most stable, the normal game doesn't feature attack sync due to how the developer programmed them but it's still extremely playable.

# Todo List 📃
https://trello.com/b/e1gvvbzK/hoursteam
# Features👍
- Chromashift is not gamepass locked and can be configured in the HourSteam UI
### Player
- Types
- Talents
- Position and Rotation
- Inputs
- Camera Position and Rotation
- Damage Dealt
- Damage Taken
- Health
- Movement
### Entities
- Types
- Talents (only endless and clone fight)
- Health
- Death
- Position and Rotation
- Attacks (only endless and clone fight)
- Movement
- Damage Taken
### Multiplayer Features
- Picking talents waits for others
- Dying allows other players to clutch up
- Friendly Fire if you and the target player turn it on
- PvP Arena
### PvP Arena Balancing
- Drifter doesn't summon noobs

# Issues ❌
### Balancing
- Due to tempos being disabled, the balancing might be very weird and will be tweaked a lot  
- Boss Health Multiplier: 0.7  
- Enemy Health Multiplier: 0.8  
- Resident Mind Health Multiplayer: 0.5 (due to dodging and having to kill (X) players)  
- Clone Health Multiplier: dependant on players, but cant go below 50 health.
- Endless Health Multiplier: 0.6
### Players
- Player cooldowns aren't synced **yet**, which might cause desync  
### Enemies
- Enemies spawned by the cheat menu will mess up enemy sync if you spawn the right enemy, they also will not sync.
- Summoned enemies (Turrets, Minions, Etc...) aren't synced **yet**.
### Tempos
- Tempos are disabled due to how the developer made them work internally. (will never be synced, and would be too overpowered).
### Modifiers
- Multiplayer modifier does not work and will break the game for you. (i couldn't care less).
- Swarm will break the game, it doesnt work **yet**.
### Proxy
- Seems to take a lot of cpu usage, I do not know why, I havent looked into it and i'm not planning to fix it.
- I use fleck for ease of use, not because it's good.

# How to setup 🔨 
If you do not mess with exploits and don't know what you're doing, stop.  
If you know how to script, you can add your own Exploit Layer if the executor you're using is not supported. 
### This currently only works with AWP 
- Download this repository
- Place HourSteam in %localappdata%/ui/workspace
- **THE PATH MUST BE HourSteam/Client, NOT HourSteam/HourSteam/Client**
- Run HourSteam/Proxy/bin/Release/Proxy.exe
- Execute HourSteam/Client/boot.lua in hours

## How to host
1. Click "create steam server"
2. Invite your friends on steam (you will need to if your status is hidden)

## How to join
1. Join your friend on steam (may require an invitation)
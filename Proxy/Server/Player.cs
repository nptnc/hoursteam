﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Server {
    public class Player : BaseEntity {
        public static byte globalSmallId = 0;

        public ulong robloxUserId;
        public ulong steamId;
        public string? playerClass = null;
        public byte smallId;

        public Vector2 moveDirection = Vector2.Zero;
        public Vector3 velocity = Vector3.Zero;

        public bool hasSelectedTalent = false;

        public Player(ulong robloxUserId, ulong steamId) {
            this.robloxUserId = robloxUserId;
            this.steamId = steamId;
            PlayerManager.players.Add(this);
        }
    }
}

local api = {}

api.setModules = function(framework)
    networker = framework("Networker")
    pvparena = framework("PvpArena")
end

api.getEntityById = function(id)
   for _,entity in getrenv()._G.Entities do
        if entity.Id == id then
            return entity
        end
   end 
end

local convertTableStringsToNumbers = function(t)
    local newT = table.clone(t)
    for index,value in newT do
        if type(value) == "string" then
            newT[index] = tonumber(value) or value
        end
    end
    return newT
end

local checkIfStringStartsWith = function(haystack,needle)
    local needleSplit = string.split(needle,"")
    local startsWith = ""
    for i,strchar in string.split(haystack,"") do
        if not needleSplit[i] then
            return false
        end
        if needleSplit[i] ~= strchar then
            return false
        end
        startsWith ..= strchar
        if startsWith == needle then
            return true
        end
    end
    return false
end

local encoding = nil

local http = game:GetService("HttpService")
api.encodeJson = function(decoded)
    local deepCopy; deepCopy = function(t)
        local copy = {}
        for index,value in t do
            if type(value) == "table" then
                value = deepCopy(value)
            end
            if encoding[typeof(value)] then
                local encoder = encoding[typeof(value)]
                value = encoder.Encode(value,encoder.Identifier)
            end
            copy[index] = value
        end
        return copy
    end
    local readyToEncode = deepCopy(decoded)
    return http:JSONEncode(readyToEncode)
end

api.decodeJson = function(encoded)
    local notFullyDecoded = http:JSONDecode(encoded)
    local deepCopy; deepCopy = function(t)
        local copy = {}
        for index,value in t do
            if type(value) == "table" then
                value = deepCopy(value)
            elseif type(value) == "string" then
                for TYPE,methods in encoding do
                    local success,newValue = methods.Decode(value,methods.Identifier)
                    if not success then
                        continue
                    end
                    value = newValue
                    break
                end
            end
            copy[index] = value
        end
        return copy
    end
    local decoded = deepCopy(notFullyDecoded)
    return decoded
end

api.deepCheckEqual = function(a,b)
    local jsonEncodedA = api.encodeJson(a)
    local jsonEncodedB = api.encodeJson(b)
    return jsonEncodedA == jsonEncodedB
end

api.isTableNull = function(hoursEntity)
    if hoursEntity == nil then
        return true
    end
    local success = pcall(function()
        hoursEntity.NULL_CHECK = "seventy-three"
        hoursEntity.NULL_CHECK = nil
    end)
    if success == false then
        return true
    end
    return hoursEntity == nil
end

api.deepCopy = function(original)
	local copy = {}
	for k, v in pairs(original) do
		if type(v) == "table" then
			v = api.deepCopy(v)
		end
		copy[k] = v
	end
	return copy
end

api.getEntitySafe = function(e)
    for _,targetEntity in getrenv()._G.Entities do
        if targetEntity == e then
            return targetEntity
        end
    end
end

api.__module = {
    [0] = function()
        local encodeSeperator = "+"
        encoding = {
            Color3 = {
                Identifier = "c";
                Encode = function(value,id)
                    return `{id}{encodeSeperator}{value.R},{value.G},{value.B}`
                end,
                Decode = function(value,id)
                    local does = checkIfStringStartsWith(value,`{id}{encodeSeperator}`)
                    if not does then
                        return false
                    end
                    value = string.gsub(value,`{id}{encodeSeperator}`,"")
                    local splitted = string.split(value,",")
                    splitted = convertTableStringsToNumbers(splitted)
                    return true,Color3.new(splitted[1],splitted[2],splitted[3])
                end,
            },
            Vector3 = {
                Identifier = "v3";
                Encode = function(value,id)
                    return `{id}{encodeSeperator}{value.X},{value.Y},{value.Z}`
                end,
                Decode = function(value,id)
                    local does = checkIfStringStartsWith(value,`{id}{encodeSeperator}`)
                    if not does then
                        return false
                    end
                    value = string.gsub(value,`{id}{encodeSeperator}`,"")
                    local splitted = string.split(value,",")
                    splitted = convertTableStringsToNumbers(splitted)
                    return true,Vector3.new(splitted[1],splitted[2],splitted[3])
                end,
            },
        }
    end
}

api.directionToDegreesY = function(direction)
    local heading = math.atan2(direction.x, direction.z)
    heading = math.deg(heading)
    heading = (heading + 0.5) - (heading + 0.5) % 1
    heading = heading + 180
    return heading
end

api.anglesToLookVector = function(angles)
    if typeof(angles) == "Vector3" then
        angles = CFrame.Angles(math.rad(angles.X),math.rad(angles.Y),math.rad(angles.Z))
    end
    local x,y,z = angles:ToEulerAnglesXYZ()
    return CFrame.Angles(x,y,z).LookVector
end

api.isPvpAllowed = function()
    if pvparena.inRound or networker.pvpEnabled then
        return true
    end
    return false
end

api.hueShift = function(ent,shift)
    for index,partProp in ent.PartProps do
        local part = ent.Parts[index]

        local h, s, v = partProp[1]:ToHSV()
        h = (h + shift / 360) % 1
        part.Color = Color3.fromHSV(h, s, v)
    end
end

return api
﻿using Proxy.Hours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Server.Stage.Stages {
    public class MiraiMirror : Stage {
        public static MiraiMirror? miraiMirror { get; private set; } = null;
        public List<string> randomEnemies = new() { "Army", "Healer", "Giant", "Rider", "Egg", "Lol", "Dread", "Diver", "Horse", "Cube" };

        public override void CreateStageData() {
            miraiMirror = this;

            List<string> remainingRandomEnemies = new();
            foreach (string enemy in randomEnemies) {
                remainingRandomEnemies.Add(enemy);
            }

            stageData = new();
            for (int i = 0; i < Game.GetEntityCount(); i++) {
                int randomIndex = new Random().Next(0, remainingRandomEnemies.Count - 1);
                stageData.Add(new EntityStageData(remainingRandomEnemies[randomIndex]));
                remainingRandomEnemies.RemoveAt(randomIndex);
                remainingRandomEnemies.Sort();
            }

            // TODO: on the client it spawns the corresponding stage boss if the boss is noob
            stageData.Add(new EntityStageData("Noob"));
        }

        public override HoursStageIds StageId() {
            return HoursStageIds.miraiMirror;
        }

        public override string[] Name() {
            return new[] { "Lagoon", "Black", "Water" };
        }
    }
}

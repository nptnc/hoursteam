# Summary

Date : 2024-05-05 14:12:07

Directory d:\\ro-exec\\workspace\\HourSteam

Total : 70 files,  5109 codes, 115 comments, 923 blanks, all 6147 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 57 | 2,609 | 81 | 517 | 3,207 |
| Lua | 10 | 2,387 | 31 | 392 | 2,810 |
| Markdown | 1 | 67 | 0 | 7 | 74 |
| XML | 2 | 46 | 3 | 7 | 56 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 70 | 5,109 | 115 | 923 | 6,147 |
| . (Files) | 1 | 67 | 0 | 7 | 74 |
| Client | 10 | 2,387 | 31 | 392 | 2,810 |
| Client (Files) | 1 | 35 | 1 | 2 | 38 |
| Client\\Modules | 9 | 2,352 | 30 | 390 | 2,772 |
| Client\\Modules\\Network | 4 | 1,008 | 8 | 172 | 1,188 |
| Client\\Modules\\Other | 3 | 709 | 4 | 100 | 813 |
| Client\\Modules\\Sync | 2 | 635 | 18 | 118 | 771 |
| Proxy | 59 | 2,655 | 84 | 524 | 3,263 |
| Proxy (Files) | 5 | 138 | 1 | 26 | 165 |
| Proxy\\Network | 36 | 2,075 | 80 | 420 | 2,575 |
| Proxy\\Network (Files) | 7 | 438 | 1 | 88 | 527 |
| Proxy\\Network\\Messages | 27 | 1,541 | 79 | 312 | 1,932 |
| Proxy\\Network\\Messages (Files) | 4 | 109 | 12 | 23 | 144 |
| Proxy\\Network\\Messages\\Enemies | 8 | 474 | 15 | 83 | 572 |
| Proxy\\Network\\Messages\\Player | 10 | 743 | 36 | 146 | 925 |
| Proxy\\Network\\Messages\\Talent | 5 | 215 | 16 | 60 | 291 |
| Proxy\\Network\\Roblox | 2 | 96 | 0 | 20 | 116 |
| Proxy\\Properties | 1 | 16 | 3 | 0 | 19 |
| Proxy\\Properties\\PublishProfiles | 1 | 16 | 3 | 0 | 19 |
| Proxy\\Server | 17 | 426 | 0 | 78 | 504 |
| Proxy\\Server (Files) | 6 | 134 | 0 | 26 | 160 |
| Proxy\\Server\\Stage | 11 | 292 | 0 | 52 | 344 |
| Proxy\\Server\\Stage (Files) | 3 | 83 | 0 | 12 | 95 |
| Proxy\\Server\\Stage\\Stages | 8 | 209 | 0 | 40 | 249 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)
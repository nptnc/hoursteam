﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Network {
    public static class MessageIds {
        /// <summary>
        /// if you dont know what the messages do based off the names, go to the message cs file
        /// </summary>
        public static readonly byte
            PLAYER_UPDATE = 2,
            PLAYER_REGISTER = 3,
            PLAYER_CLASS = 4,
            DISCONNECT = 5,
            PLAYER_INPUT_FUNCTION = 6,
            CONNECTED_TO_STEAM = 7,
            TALENT_POPUP_START = 8,
            ENEMY_STAGE_DATA = 9,
            ENEMY_UPDATE = 10,
            ENEMY_DAMAGE = 11,
            PLAYER_DAMAGE = 12,
            TALENT_CHOSEN = 13,
            ENEMY_HEALTH_CHANGE = 14,
            TALENT_GAINED = 19,
            TALENT_CLEAR = 20,
            PLAYER_HEALTH = 22,
            GAME_END = 24,
            LEAVE_STEAM = 25,
            PLAYER_PVP_DAMAGE = 26,
            ENEMY_DEATH = 27,
            FETCH_LOBBY_LIST = 28,
            PLAYER_ACTION_FUNCTION = 29,
            ENEMY_INPUT_FUNCTION = 30,
            ENEMY_TALENT_GAINED = 31,
            ROUND_END = 32;
    }
}

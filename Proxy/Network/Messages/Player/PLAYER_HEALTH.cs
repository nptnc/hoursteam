﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;

namespace Proxy.Network.Messages
{
    /// <summary>
    /// verbatim
    /// definition: "in exactly the same words as were used originally."
    /// nptnc said verbatim, 'Player Health'
    /// </summary>
    public class PLAYER_HEALTH : NetworkMessage
    {
        public override byte MessageId()
        {
            return MessageIds.PLAYER_HEALTH;
        }

        public override void RobloxReceived(RobloxReader data)
        {
            int health = data.ReadInt();

            NetworkWriter writer = new();
            writer.Write((ushort)health);

            NetworkSender.SendToServer(MessageId(), SendType.Reliable, writer.Create());
        }

        public override void ReceivedServer(SteamId by, byte[] data)
        {
            NetworkReader reader = new(data);
            ushort health = reader.ReadUShort();

            Player? player = PlayerManager.GetPlayerBySteamId(by);
            if (player == null) {
                return;
            }

            NetworkWriter writer = new();
            writer.Write(health);
            writer.Write(player.smallId);

            NetworkSender.SendToClients(MessageId(), writer.Create(), SendType.Reliable, new ulong[] { by });
        }

        public override void ReceivedClient(byte[] data)
        {
            NetworkReader reader = new(data);
            float health = reader.ReadUShort();
            byte smallId = reader.ReadByte();

            RobloxWriter writer = new(MessageId());
            writer.WriteString(smallId.ToString());
            writer.WriteString(health.ToString());

            NetworkSender.SendToRoblox(writer);
        }
    }
}

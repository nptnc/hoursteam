local module = {}

module.setModules = function(framework)
    util = framework("Util")
    networkEntity = framework("NetworkEntity")
    networkStage = framework("NetworkStage")
    networkPlayerModule = framework("NetworkPlayer")
    networker = framework("Networker")
    hoursGame = framework("Game")
    pvpArena = framework("PvpArena")
end

local rs = game:GetService("RunService")
local uis = game:GetService("UserInputService")
local connections = {}

module.localPlayerData = {}
module.talentsToAdd = {}

module.__module = {
    [2] = function()
        networker.OnConnectionSuccess:Connect(function()
            networker.bindFunctionToMessage(networker.messageIds.PLAYER_REGISTER,function(playerId,playerSmallId)
                if playerId ~= game.Players.LocalPlayer.UserId then
                    return
                end
                module.localPlayerData = {
                    id = playerId,
                    smallId = playerSmallId,
                }
            end)
        
            networker.bindFunctionToMessage(networker.messageIds.PLAYER_PVP_DAMAGE,function(damagedBy,json)
                if util.isPvpAllowed() == false then
                    return
                end

                local damagedByPlayer = networkPlayerModule.getPlayer(damagedBy)
                if damagedByPlayer == nil or util.isTableNull(damagedByPlayer) then
                    return
                end

                local localEntity = getrenv()._G.Entities[1]
                if not localEntity then
                    return
                end

                print(`{damagedByPlayer.username} is trying to damage you {json}`)
                local decoded = util.decodeJson(json)
                decoded.Target = localEntity.Id
                decoded.Source = damagedByPlayer.entity.Id
                decoded.WasDamageNetworked = true
                getrenv()._G.DamageRequest(decoded)
            end)
        
            networker.bindFunctionToMessage(networker.messageIds.CONNECTED_TO_STEAM,function()
                local writer = networker.writer(networker.messageIds.PLAYER_REGISTER)
                writer:write(game.Players.LocalPlayer.UserId)
                networker.sendToServer(writer:free())
            end)
        
            local sinceLastUpdate = 0
            local lastLocalEntity = nil
            local lastNetworkedUpdate = {
                position = Vector3.zero,
                rotation = Vector3.zero,
                velocity = Vector3.zero,
                moveDir = Vector2.zero,
                health = 10000,
            }
        
            local updateConditions = {
                move = function(humanoid,humrp,rotation)
                    if (lastNetworkedUpdate.position - humrp.Position).Magnitude > 0.3 then
                        return true
                    end
                    return false
                end,
                moveDir = function(humanoid,humrp,rotation)
                    if (lastNetworkedUpdate.moveDir - Vector2.new(humanoid.MoveDirection.X,humanoid.MoveDirection.Z)).Magnitude > 0.1 then
                        return true
                    end
                    return false
                end,
                velocityZero = function(humanoid,humrp,rotation)
                    if (lastNetworkedUpdate.velocity ~= Vector3.zero and humrp.Velocity == Vector3.zero) then
                        return true
                    end
                    return false
                end,
                velocity = function(humanoid,humrp,rotation)
                    if (lastNetworkedUpdate.velocity - humrp.Velocity).Magnitude > 0.5 then
                        return true
                    end
                    return false
                end,
                rotation = function(humanoid,humrp,rotation)
                    if (lastNetworkedUpdate.rotation - rotation).Magnitude > 5 then
                        -- our rotation changed more than 5 degrees since the last packet
                        return true
                    end
                    return false
                end
            }
        
            local currentlyDead = false
            networker.bindFunctionToMessage(networker.messageIds.LEAVE_SERVER,function()
                lastLocalEntity = nil
            end)
        
            local patchedInputFunctions = {}
            rs.Heartbeat:Connect(function(dt)--rs:BindToRenderStep("player_update",Enum.RenderPriority.Character.Value-1,function(dt)
                networker.IsServer = module.localPlayerData and module.localPlayerData.smallId == 0 or false
        
                if not networker.connected then
                    return
                end
        
                local localEntity = nil
                for _,entity in getrenv()._G.Entities do
                    if entity.Id == 1 and getrenv()._G.GameState ~= "MainScreen" then
                        localEntity = entity
                    end
                end
        
                if currentlyDead then
                    local allDead = true
                    for _,otherPlayer in networkPlayerModule.getPlayers() do
                        if otherPlayer.health <= 0 then
                            continue
                        end
                        allDead = false
                    end
                    if allDead or pvpArena.inRound == true then
                        getrenv()._G.EndGame()
                        currentlyDead = false
                    end
                end
        
                if localEntity == nil then
                    return
                end
                
                util.hueShift(localEntity,networker.hue or 0)
        
                local humanoid = localEntity.Humanoid
                local moveDir = humanoid.MoveDirection
                local humrp = localEntity.RootPart
        
                local calculatedVelocity = humrp.AssemblyLinearVelocity
                local calculatedRVelocity = humrp.AssemblyAngularVelocity
        
                for _,talentName in module.talentsToAdd do
                    local writer6 = networker.writer(networker.messageIds.TALENT_GAINED)
                    writer6:write(talentName)
                    networker.sendToServer(writer6:free())
                    print(`networking talent gained {talentName}`)
                end
                module.talentsToAdd = {}
        
                if lastLocalEntity ~= localEntity then
                    networkStage.resetSpawnId()
        
                    currentlyDead = false
                    lastNetworkedUpdate.health = 10000
        
                    print("new local entity")
                    patchedInputFunctions = {}
        
                    -- enables spectating
                    local oldDieBase = localEntity.DieBase
                    localEntity.DieBase = function(...)
                        print("die base was called")
                        hoursGame.justDied = true
                        currentlyDead = true
                        local returned = table.pack(oldDieBase(...))
                        return table.unpack(returned)
                    end
        
                    if localEntity.Name == "Class5318008" then
                        local oldSkillParry = localEntity.ActionFunctions["SkillParry"]
                        localEntity.ActionFunctions["SkillParry"] = function(...)
                            local writer1 = networker.writer(networker.messageIds.PLAYER_ACTION_FUNCTION)
                            writer1:write("SkillParry")
                            networker.sendToServer(writer1:free())
                            return oldSkillParry(...)
                        end
                    elseif localEntity.Name == "Class1338" then
                        local oldSkillParry = localEntity.ActionFunctions["EndCar"]
                        localEntity.ActionFunctions["EndCar"] = function(...)
                            local writer1 = networker.writer(networker.messageIds.PLAYER_ACTION_FUNCTION)
                            writer1:write("EndCar")
                            networker.sendToServer(writer1:free())
                            return oldSkillParry(...)
                        end
                    end
        
                    local writer1 = networker.writer(networker.messageIds.PLAYER_CLASS)
                    writer1:write(localEntity == nil)
                    if localEntity ~= nil then
                        writer1:write(localEntity.Name)
                    end
                    networker.sendToServer(writer1:free())
        
                    lastLocalEntity = localEntity
                end
        
                for inputName,inputFunction in localEntity.InputFunctions or {} do
                    if table.find(patchedInputFunctions,inputName) then
                        continue
                    end
                    table.insert(patchedInputFunctions,inputName)
            
                    localEntity.InputFunctions[inputName] = function(...)
                        local oldInput = localEntity.Input
                        inputFunction(...)
                        if oldInput ~= false and localEntity.Input == false then
                            local actualInput = nil
                            for inputReference,inputFunctionName in localEntity.Inputs do
                                if inputFunctionName == inputName then
                                    actualInput = inputReference
                                end
                            end
                            if actualInput == nil then
                                warn("actual input is null, how tf was this called?")
                                return
                            end
        
                            print(`did input {inputName}`)
                            local writer2 = networker.writer(networker.messageIds.PLAYER_INPUT_FUNCTION)
                            writer2:write(actualInput)
                            writer2:write(humrp.Position)
                            writer2:write(util.directionToDegreesY(util.anglesToLookVector(humrp.Rotation)))
                            writer2:write(workspace.CurrentCamera.CFrame.Position - humrp.Position)
                            local x,y,z = workspace.CurrentCamera.CFrame:ToEulerAnglesXYZ()
                            writer2:write(Vector3.new(math.deg(x),math.deg(y),math.deg(z)))
                            writer2:write(Vector2.new(humanoid.MoveDirection.X,humanoid.MoveDirection.Z))
                            networker.sendToServer(writer2:free())
                        end
                    end
                end
        
                if tick() - sinceLastUpdate < 1/(networkPlayerModule.playerUpdateRate or 10) then
                    return
                end
        
                sinceLastUpdate = tick()
        
                local rotation = humrp.Rotation
        
                local curHealth = math.ceil(localEntity.Resources.Health)
                if curHealth < 0 then
                    curHealth = 0
                end
                if curHealth ~= lastNetworkedUpdate.health then
                    local writer8 = networker.writer(networker.messageIds.PLAYER_HEALTH)
                    writer8:write(curHealth)
                    networker.sendToServer(writer8:free())
                    lastNetworkedUpdate.health = curHealth
                end
        
                local anyTrue = nil
                for reason,conditionMethod in updateConditions do
                    if conditionMethod(humanoid,humrp,rotation) then
                        anyTrue = reason
                    end
                end
                if anyTrue == nil then
                    return
                end
        
                local writer = networker.writer(networker.messageIds.PLAYER_UPDATE)
                writer:write(humrp.Position)
                writer:write(calculatedVelocity)
                writer:write(util.directionToDegreesY(util.anglesToLookVector(rotation)))
                writer:write(calculatedRVelocity.Magnitude > 0.07 and util.directionToDegreesY(util.anglesToLookVector(calculatedRVelocity)) or 0)
                writer:write(Vector2.new(moveDir.X,moveDir.Z))
                networker.sendToServer(writer:free())
        
                lastNetworkedUpdate.position = humrp.Position
                lastNetworkedUpdate.moveDir = Vector2.new(moveDir.X,moveDir.Z)
                lastNetworkedUpdate.rotation = rotation
                lastNetworkedUpdate.velocity = humrp.Velocity
            end)
        end)
    end
}

return module
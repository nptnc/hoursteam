﻿using Minesweeper.Multiplayer;
using Proxy.Server.Stage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Network.DataTypes {
    public class StageData : NetworkDataType {
        public byte? entityCount = null;
        public List<EntityStageData>? entityData = null;
        public string? fullStageName = null;

        public Stage? stage = null;

        public StageData(byte[]? data = null) {
            if (data == null)
                return;
            Decode(data);
        }

        public override void Decode(byte[] data) {
            entityData = new();

            NetworkReader reader = new(data);
            entityCount = reader.ReadByte();

            for (int i = 0; i < entityCount; i++) {
                byte nameLength = reader.ReadByte();
                Vector3 position = reader.ReadVector3();
                string entityName = reader.ReadString(nameLength);
                entityData.Add(new(entityName, position));
            }

            fullStageName = reader.ReadString();
        }

        public override NetworkWriter Encode() {
            NetworkWriter stageWriter = new();

            // ENEMIES
            stageWriter.Write((byte)stage!.stageData!.Count);
            for (int i = 0; i < stage.stageData.Count; i++) {
                stageWriter.Write((byte)stage.stageData[i].entityType.Length);
                stageWriter.Write(stage.stageData[i].position);
                stageWriter.Write(stage.stageData[i].entityType);
            }

            // STAGE NAME
            string stageName = "";
            for (int i = 0; i < stage.Name().Length; i++) {
                string targetName = stage.Name()[i];
                if (i == stage.Name().Length - 1) {
                    stageName += targetName;
                    continue;
                }
                stageName += targetName + ".";
            }
            stageWriter.Write(stageName);

            return stageWriter;
        }
    }
}

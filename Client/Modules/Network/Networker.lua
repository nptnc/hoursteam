local module = {}

module.setModules = function(framework)
    layermanager = framework("LayerManager")
    signal = framework("Signal")
    ui = framework("UI")

    module.OnConnectionSuccess = signal.new()
end

local seperator = "|" -- must be one character also must be the same on the server

local messages = {}

local messageIds = {
    PLAYER_UPDATE = {2,{"number","Vector3","Vector3","number","number","Vector2"}},
    PLAYER_REGISTER = {3,{"number","number"}},
    PLAYER_CLASS = {4,{"number","string"}}, -- this is for despawning the player if they go into the menu, or they changed class
    DISCONNECT = {5,{"number"}},
    PLAYER_INPUT_FUNCTION = {6,{"number","string","Vector3","number","Vector3","Vector3","Vector2"}},
    CONNECTED_TO_STEAM = {7,{}},
    TALENT_POPUP_START = {8,{"boolean"}}, -- this is different from TALENT_POPUP, the reason this exists is because of how game start works, bit messed up tbf
    ENEMY_STAGE_DATA = {9,{},true}, -- reason why true is here is because there is an unknown amount of entities, so we stop assuming types
    ENEMY_UPDATE = {10,{"number","Vector3","number","Vector3","number","Vector2"}},
    ENEMY_DAMAGE = {11,{"number","number","string"}},
    PLAYER_DAMAGE = {12,{"number","number","boolean","string"}},
    TALENT_CHOSEN = {13,{"number","number"}},
    ENEMY_HEALTH_CHANGE = {14,{"number","number"}},
    ENEMY_ANIMATION_CHANGE = {15,{"number","number","string"}},
    TALENT_POPUP = {16,{}},
    TALENT_GAINED = {19,{"number","number"}},
    TALENT_CLEAR = {20,{"number"}},
    ENEMY_STAT_CHANGE = {21,{"number","string","string"}},
    PLAYER_HEALTH = {22,{"number","number"}},
    PLAYER_STAT_CHANGE = {23,{"number","string","string"}},
    GAME_END = {24,{}},
    LEAVE_SERVER = {25,{}},
    PLAYER_PVP_DAMAGE = {26,{"number","string"}},
    ENEMY_DEATH = {27,{"number"}},
    FETCH_LOBBY_LIST = {28,{},true},
    PLAYER_ACTION_FUNCTION = {29,{"number","string"}}, -- drifter ap
    ENEMY_INPUT_FUNCTION = {30,{"number","string","Vector3","number","Vector3","Vector3"}},
    ENEMY_TALENT_GAINED = {31,{"number","number"}},
    ROUND_END = {32,{"number","number"}}
}

module.messageIds = messageIds

for _,num in messageIds do
    messages[num[1]] = {}
end

module.connected = false
module.useDisplayName = false
module.IsServer = false

module.seperator = seperator
module.pvpEnabled = false

module.hue = 0

local socket = nil
module.sendToServer = function(str)
    if socket == nil then
        return
    end
    socket:Send(str)
end

module.bindFunctionToMessage = function(id,method)
    if id == nil then
        warn("cannot bind function to nil!")
        return
    end
    id = id[1]
    if messages[id] == nil then
        print(`cant bind function to message {id}`)
        return
    end
    print(`successfully bound a function to message {id}`)
    table.insert(messages[id],method)
end

local writer = {}
writer.__index = writer

writer.new = function(msgid)
    if typeof(msgid) == "table" then
        msgid = msgid[1]
    end
    local self = setmetatable({},writer)
    self.msgid = msgid
    self.wrote = {}
    table.insert(self.wrote,tostring(msgid))
    return self
end

function writer:write_Vector3(v3)
    table.insert(self.wrote,`{v3.X}_{v3.Y}_{v3.Z}`)
end

function writer:write_Vector2(v3)
    table.insert(self.wrote,`{v3.X}_{v3.Y}`)
end

function writer:write_Value(val)
    table.insert(self.wrote,val)
end

function writer:write(obj)
    local t = typeof(obj)
    if t == "Vector3" then
        self:write_Vector3(obj)
    elseif t == "Vector2" then
        self:write_Vector2(obj)
    elseif t == "string" then
        self:write_Value(obj)
    elseif t == "number" then
        self:write_Value(tostring(obj))
    elseif t == "boolean" then
        self:write_Value(tostring(obj == true and 1 or 0))
    else
        warn(`{t} is unsupported, cannot write to writer!`)
        return
    end
end

function writer:free()
    local wrote = table.concat(self.wrote,seperator)

    setmetatable(self,nil)
    table.clear(self)

    return wrote
end

module.writer = function(...)
    return writer.new(...)
end

module.createSteamServer = function()
    local startServerWriter = module.writer(messageIds.CONNECTED_TO_STEAM)
    module.sendToServer(startServerWriter:free())
end

module.leaveSteamServer = function()
    local leaveServerWriter = module.writer(messageIds.LEAVE_SERVER)
    module.sendToServer(leaveServerWriter:free())
end

local host = nil

module.attemptToCreateSocket = function()
    local success,err = pcall(function()
        socket = layermanager.getLayer().createWebsocket(host)
    end)
    if not success and err then
        warn(err)
        ui.onFailedToConnect()
        socket = nil
        return
    end

    print("connection success... so... why's it not working?")

    module.OnConnectionSuccess:Fire()

    local cutTable = function(t,i)
        local newT = {}
        for index,val in ipairs(t) do
            if index <= i then
                continue
            end
            table.insert(newT,val)
        end
        return newT
    end

    local convert = function(val,typ)
        if typ == "Vector3" then
            local split = string.split(val,"_")
            if #split ~= 3 then
                warn(`failed to read Vector3, data is not as expected!\n{val}`)
                return nil
            end
            return Vector3.new(tonumber(split[1]),tonumber(split[2]),tonumber(split[3]))
        elseif typ == "Vector2" then
            local split = string.split(val,"_")
            if #split ~= 2 then
                warn(`failed to read Vector2, data is not as expected!\n{val}`)
                return nil
            end
            return Vector2.new(tonumber(split[1]),tonumber(split[2]))
        elseif typ == "boolean" then
            if val == "1" then
                return true
            else
                return false
            end
            warn(`failed to read boolean, data is not as expected!\n{val}`)
            return nil
        elseif typ == "string" then
            return val
        elseif typ == "number" then
            if tonumber(val) == nil then
                warn(`failed to read number, data is not as expected!\n{val}`)
            end
            return tonumber(val)
        end
        return nil
    end

    socket:Received(function(message)
        local seperations = string.split(message,"|")
        local msgId = tonumber(seperations[1])
        
        local actualData = cutTable(seperations,1)
        local msgConfig = nil
        for _,msgData in messageIds do
            if msgData[1] == msgId then
                msgConfig = msgData[2]
            end
        end
        if not msgConfig then
            warn(`message config not found for message id {msgId}`)
            return
        end

        local shouldConvert = msgConfig[3] ~= true

        for index,t in shouldConvert and msgConfig or {} do
            actualData[index] = convert(actualData[index],t)
        end
        
        for id,callbackArray in messages do
            if id ~= msgId then
                continue
            end
            for _,callback in callbackArray do
                callback(table.unpack(actualData))
            end
        end
    end)

    socket:Closed(function()
        socket = nil
        module.connected = false
    end)
end

module.__module = {
    [3] = function(o,extraData)
        --[[local contents = readfile(`{extraData.buildLocation}/proxy_ip.txt`)--{_G.Multiplayer.localBuild}/proxy_ip.txt`)
        local firstLine = string.split(contents,"\n")[1]
        firstLine = string.gsub(firstLine," ","")
        host = firstLine--]]

        host = "ws://127.0.0.1:8181"
        warn(`host is {host}!`)
    end,
    [100] = function()
        module.bindFunctionToMessage(module.messageIds.CONNECTED_TO_STEAM,function()
            module.connected = true
        end)
        
        module.bindFunctionToMessage(module.messageIds.LEAVE_SERVER,function()
            module.connected = false
        end)
    end
}

return module
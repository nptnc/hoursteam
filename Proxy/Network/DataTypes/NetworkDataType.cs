﻿using Minesweeper.Multiplayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Network.DataTypes {
    public abstract class NetworkDataType {
        public abstract void Decode(byte[] data);
        public abstract NetworkWriter Encode();
    }
}

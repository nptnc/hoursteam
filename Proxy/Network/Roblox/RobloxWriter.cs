﻿using Proxy.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Proxy {
    public class RobloxWriter {
        private List<string> strings = new();

        public RobloxWriter(int msgId) {
            strings.Add(msgId.ToString());
        }

        public string Get() {
            string finishedString = "";
            int index = 0;
            foreach (string s in strings) {
                if (index > 0) {
                    finishedString += Main.seperator;
                }
                finishedString += s;
                index++;
            }
            return finishedString;
        }

        public void WriteVector3(Vector3 v3) {
            strings.Add($"{v3.X}_{v3.Y}_{v3.Z}");
        }

        public void WriteVector2(Vector2 v3) {
            strings.Add($"{v3.X}_{v3.Y}");
        }

        public void WriteString(string str) {
            strings.Add(str);
        }

        public void WriteBool(bool bol) {
            strings.Add(bol == true ? "1" : "0");
        }
    }
}

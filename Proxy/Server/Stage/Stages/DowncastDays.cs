﻿using Proxy.Hours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Server.Stage.Stages {
    public class DowncastDays : Stage {
        public List<string> randomEnemies = new() { "Archer", "Astral", "Feder", "RedCliff", "Darkage", "Ekotss", "Korblox", "Lightage", "Ori", "Overseer" };

        public override HoursStageIds StageId() {
            return HoursStageIds.downcastDays;
        }

        public override void CreateStageData() {
            List<string> remainingRandomEnemies = new();
            foreach (string enemy in randomEnemies) {
                remainingRandomEnemies.Add(enemy);
            }

            stageData = new();
            for (int i = 0; i < Game.GetEntityCount(); i++) {
                int randomIndex = new Random().Next(0, remainingRandomEnemies.Count - 1);
                stageData.Add(new EntityStageData(remainingRandomEnemies[randomIndex]));
                remainingRandomEnemies.RemoveAt(randomIndex);
                remainingRandomEnemies.Sort();
            }

            stageData.Add(new EntityStageData("Bloxxer"));
        }

        public override string[] Name() {
            return new[] { "Fallout", "Menger", "Shower" };
        }
    }
}

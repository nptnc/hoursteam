# Diff Details

Date : 2024-05-05 14:12:07

Directory d:\\ro-exec\\workspace\\HourSteam

Total : 10 files,  178 codes, 10 comments, 31 blanks, all 219 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [Client/Modules/Network/NetworkEntity.lua](/Client/Modules/Network/NetworkEntity.lua) | Lua | 69 | 1 | 16 | 86 |
| [Client/Modules/Network/NetworkPlayer.lua](/Client/Modules/Network/NetworkPlayer.lua) | Lua | 0 | 0 | 1 | 1 |
| [Client/Modules/Network/Networker.lua](/Client/Modules/Network/Networker.lua) | Lua | 7 | 0 | 0 | 7 |
| [Client/Modules/Other/UI.lua](/Client/Modules/Other/UI.lua) | Lua | 10 | 0 | 1 | 11 |
| [Client/Modules/Sync/Game.lua](/Client/Modules/Sync/Game.lua) | Lua | 0 | 1 | 0 | 1 |
| [Proxy/Network/MessageIds.cs](/Proxy/Network/MessageIds.cs) | C# | 1 | 0 | 0 | 1 |
| [Proxy/Network/Messages/Enemies/ENEMY_ANIMATION_CHANGE.cs](/Proxy/Network/Messages/Enemies/ENEMY_ANIMATION_CHANGE.cs) | C# | -1 | 4 | 0 | 3 |
| [Proxy/Network/Messages/Enemies/ENEMY_INPUT_FUNCTION.cs](/Proxy/Network/Messages/Enemies/ENEMY_INPUT_FUNCTION.cs) | C# | 103 | 4 | 13 | 120 |
| [Proxy/Network/Messages/Enemies/ENEMY_UPDATE.cs](/Proxy/Network/Messages/Enemies/ENEMY_UPDATE.cs) | C# | -10 | 0 | 0 | -10 |
| [Proxy/Network/Messages/Player/PLAYER_INPUT_FUNCTION.cs](/Proxy/Network/Messages/Player/PLAYER_INPUT_FUNCTION.cs) | C# | -1 | 0 | 0 | -1 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details
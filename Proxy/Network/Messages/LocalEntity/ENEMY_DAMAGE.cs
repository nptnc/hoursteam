﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;

namespace Proxy.Network.Messages
{
    /// <summary>
    /// local entity damage
    /// </summary>
    public class ENEMY_DAMAGE : NetworkMessage
    {
        public override byte MessageId()
        {
            return MessageIds.ENEMY_DAMAGE;
        }

        public override void RobloxReceived(RobloxReader data)
        {
            float entityId = data.ReadFloat();
            string encodedDamageJson = data.ReadString();

            NetworkWriter writer = new NetworkWriter();
            writer.Write((ushort)entityId);
            writer.Write(encodedDamageJson);

            NetworkSender.SendToServer(MessageId(), SendType.Reliable, writer.Create());
        }

        public override void ReceivedServer(SteamId by, byte[] data)
        {
            Player? player = PlayerManager.GetPlayerBySteamId(by);
            if (player == null)
                return;

            NetworkReader reader = new(data);
            ushort entityId = reader.ReadUShort();
            string encodedDamageJson = reader.ReadString();

            NetworkWriter writer = new();
            writer.Write(player.smallId);
            writer.Write(entityId);
            writer.Write(encodedDamageJson);

            ulong[] ignoreList = player.smallId == 0 ? new ulong[] { by } : new ulong[] { };
            NetworkSender.SendToClients(MessageId(), writer.Create(), SendType.Reliable, ignoreList);
        }

        public override void ReceivedClient(byte[] data)
        {
            NetworkReader reader = new NetworkReader(data);
            byte id = reader.ReadByte();
            ushort entityId = reader.ReadUShort();
            string encodedDamageJson = reader.ReadString();

            RobloxWriter writer = new(MessageId());
            writer.WriteString(id.ToString());
            writer.WriteString(entityId.ToString());
            writer.WriteString(encodedDamageJson);

            NetworkSender.SendToRoblox(writer);
        }
    }
}
# Diff Summary

Date : 2024-05-05 14:12:07

Directory d:\\ro-exec\\workspace\\HourSteam

Total : 10 files,  178 codes, 10 comments, 31 blanks, all 219 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 5 | 92 | 8 | 13 | 113 |
| Lua | 5 | 86 | 2 | 18 | 106 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 10 | 178 | 10 | 31 | 219 |
| Client | 5 | 86 | 2 | 18 | 106 |
| Client\\Modules | 5 | 86 | 2 | 18 | 106 |
| Client\\Modules\\Network | 3 | 76 | 1 | 17 | 94 |
| Client\\Modules\\Other | 1 | 10 | 0 | 1 | 11 |
| Client\\Modules\\Sync | 1 | 0 | 1 | 0 | 1 |
| Proxy | 5 | 92 | 8 | 13 | 113 |
| Proxy\\Network | 5 | 92 | 8 | 13 | 113 |
| Proxy\\Network (Files) | 1 | 1 | 0 | 0 | 1 |
| Proxy\\Network\\Messages | 4 | 91 | 8 | 13 | 112 |
| Proxy\\Network\\Messages\\Enemies | 3 | 92 | 8 | 13 | 113 |
| Proxy\\Network\\Messages\\Player | 1 | -1 | 0 | 0 | -1 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)
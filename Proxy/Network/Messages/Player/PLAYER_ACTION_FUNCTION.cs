﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;
using Proxy.Hours;

namespace Proxy.Network.Messages
{
    /// <summary>
    /// this synchronizes when action functions are called
    /// used when hellion's car hits a wall
    /// used when drifter parries
    /// </summary>
    public class PLAYER_ACTION_FUNCTION : NetworkMessage
    {
        public override byte MessageId()
        {
            return MessageIds.PLAYER_ACTION_FUNCTION;
        }

        public override void RobloxReceived(RobloxReader data)
        {
            string input = data.ReadString();

            NetworkWriter writer = new();
            writer.Write(input);

            NetworkSender.SendToServer(MessageId(), SendType.Reliable, writer.Create());
        }

        public override void ReceivedServer(SteamId by, byte[] data)
        {
            NetworkReader reader = new(data);
            string input = reader.ReadString();

            Player? player = PlayerManager.GetPlayerBySteamId(by);
            if (player == null)
                return;

            if (player.playerClass != HoursEntityIds.drifter && player.playerClass != HoursEntityIds.hellion)
                return;

            NetworkWriter writer = new();
            writer.Write(player.smallId);
            writer.Write(input);

            NetworkSender.SendToClients(MessageId(), writer.Create(), SendType.Reliable, new ulong[] { by });
        }

        public override void ReceivedClient(byte[] data)
        {
            NetworkReader reader = new(data);
            byte smallId = reader.ReadByte();
            string input = reader.ReadString();

            Player? player = PlayerManager.GetPlayerByByteId(smallId);
            if (player == null)
                return;

            Logger.Log($"{player.robloxUserId} action function: {input}");

            RobloxWriter writer = new(MessageId());
            writer.WriteString(smallId.ToString());
            writer.WriteString(input);

            NetworkSender.SendToRoblox(writer);
        }
    }
}

﻿using Steamworks;
using Steamworks.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Network {
    internal class LobbyMetadata {
        internal string? ownerName;
        internal ulong? ownerId;

        internal Lobby? lobby;

        internal LobbyMetadata(Lobby? lobby = null) {
            this.lobby = lobby;
            if (lobby != null)
                Parse(lobby.Value);
        }

        private string? SafeGetData(string key) {
            foreach (KeyValuePair<string,string> keyvalue in lobby.Value.Data) {
                if (keyvalue.Key == key) {
                    return keyvalue.Value;
                }
            }
            return null;
        }

        internal void Parse(Lobby l) {
            ownerName = l.GetData(nameof(ownerName));
            string? tempOwnerData = SafeGetData(nameof(tempOwnerData));
            if (tempOwnerData != null)
                ownerId = ulong.Parse(tempOwnerData);
        }

        internal static void Create(Lobby l) {
            l.SetData(nameof(ownerName), SteamClient.Name);
            l.SetData(nameof(ownerId), SteamClient.SteamId.ToString());
        }
    }
}

﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;

namespace Proxy.Network.Messages.LocalEntity
{
    /// <summary>
    /// occurs when a talent is gained by a enemy, like in endless
    /// </summary>
    public class TALENT_GAINED : NetworkMessage
    {
        public override byte MessageId()
        {
            return MessageIds.ENEMY_TALENT_GAINED;
        }

        public override void RobloxReceived(RobloxReader data)
        {
            int networkId = data.ReadInt();
            int talentIndex = data.ReadInt();

            NetworkWriter writer = new();
            writer.Write((ushort)networkId);
            writer.Write((ushort)talentIndex);
            
            NetworkSender.SendToServer(MessageId(), SendType.Reliable, writer.Create());
        }

        public override void ReceivedServer(SteamId by, byte[] data)
        {
            NetworkReader reader = new(data);
            int networkId = reader.ReadUShort();
            int talentIndex = reader.ReadUShort();

            Console.WriteLine($"enemy {networkId} gained talent {talentIndex} [server]");

            NetworkSender.SendToClients(MessageId(), data, SendType.Reliable, new ulong[] { by });
        }

        public override void ReceivedClient(byte[] data)
        {
            NetworkReader reader = new(data);
            int networkId = reader.ReadUShort();
            int talentIndex = reader.ReadUShort();

            Console.WriteLine($"enemy {networkId} gained talent {talentIndex} [client]");

            RobloxWriter writer = new(MessageId());
            writer.WriteString(networkId.ToString());
            writer.WriteString(talentIndex.ToString());

            NetworkSender.SendToRoblox(writer);
        }
    }
}

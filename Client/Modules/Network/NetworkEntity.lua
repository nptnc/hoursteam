-- im gonna need to rewrite
-- 6/12/2024: well, not really just the framework does.

local rs = game:GetService("RunService")

local updateRate = 20

local classes = {"Class01","Class0","Class1","Class2","Class3","Class4","Class5","Class6","Class7","Class8","Class9","Class5318008","Class999","ClassDuck","Class1338"}
local module = {}

module.setModules = function(framework)
    util = framework("Util")
    networker = framework("Networker")
    networkPlayer = framework("NetworkPlayer")
    networkStage = framework("NetworkStage")
    localPlayerModule = framework("Player")
    gameModule = framework("Game")
end

local entities = {}
local entity = {}
entity.__index = entity

entity.new = function(name,networkId,damageTeam,hoursEntity)
    if module.getEntityById(networkId) then
        warn(`network entity {networkId} already exists!`)
        return
    end
    local self = setmetatable({},entity)
    
    self.entity = hoursEntity
    
    self.team = damageTeam
    self.id = networkId
    self.name = name

    self.isHostEntity = table.find(classes,self.name) ~= nil

    self.position = Vector3.zero
    self.rotation = 0

    self.velocity = Vector3.zero
    self.rotvelocity = 0

    self.interpolatedPosition = self.rotation
    self.interpolatedRotation = 0

    self.characterAtUpdateReceived = self.position
    self.rotationAtUpdateReceived = self.rotation

    self.owner = 0 -- never transfer ownership, although it is a nice system to write around
    
    -- this means they will always die if a player networks their death
    -- 12/12/2024 this can be removed now, i'll probably do it later.
    self.dead = false
    self.networkedHealth = 9999

    self.moveDirection = Vector2.zero

    self.sinceUpdate = tick()
    self.sinceDamaged = 0

    self.talents = {}
    self.patchedInputFunctions = {}

    self.input = false
    self.inputTimer = 0
    self.inputCameraCFrame = CFrame.new()

    self.alreadyAppliedTalents = false

    table.insert(entities,self)
    return self
end

local destroyEntity = function(targetEntity)
    if targetEntity == nil then
        return
    end
    targetEntity:Interrupt()
    targetEntity.Character:Destroy()
    getrenv()._G.Entities[targetEntity.Id] = nil
end

function entity:UpdateCFrame(position,rotation,vel,rotvelocity,moveDir,instant)
    instant = instant or false
    self.position = position
    self.rotation = rotation
    self.velocity = vel
    self.rotvelocity = rotvelocity

    self.interpolatedPosition = instant and self.position or self.interpolatedPosition
    self.interpolatedRotation = instant and self.position or self.interpolatedRotation

    self.characterAtUpdateReceived = instant and position or (self.entity and self.entity.RootPart.Position or self.position)
    self.rotationAtUpdateReceived = instant and rotation or (self.entity and self.entity.RootPart.Orientation.Y or self.rotation)

    self.sinceUpdate = tick()
    self.moveDirection = moveDir
end

function entity:Input(input,inputTimer,inputCF)
    if not self.isHostEntity then
        return
    end
    self.input = input
    self.inputTimer = inputTimer
    self.inputCameraCFrame = inputCF
end

function entity:OnAttackStarted(input)
    if not self.isHostEntity then
        return
    end
    self:UpdateCFrame(self.position,self.rotation,self.velocity,self.rotvelocity,Vector2.zero,true)
end

function entity:ApplyNetworkedTalents()
    if self.alreadyAppliedTalents then
        warn("already applied talents!")
        return
    end
    if not self.isHostEntity then
        warn("isn't host entity!")
        return
    end
    if self.entity == nil then
        warn("entity doesn't exist!")
        return
    end

    self.alreadyAppliedTalents = true

    print("trying to apply talents to enemy!")
    for _,talentId in ipairs(self.talents) do
        talentId = tonumber(talentId)
        
        warn(`APPLIED TALENT {talentId} TO ENEMY {self.id}!`)

        self.entity.Talents[talentId] = true
        if self.entity.TalentFunctions[talentId] then
            self.entity.TalentFunctions[talentId]()
        end
    end
end

function entity:OwnerUpdate(dt,canUpdate)
    for inputName,inputFunction in self.entity.InputFunctions or {} do
        if table.find(self.patchedInputFunctions,inputName) then
            continue
        end
        table.insert(self.patchedInputFunctions,inputName)

        self.entity.InputFunctions[inputName] = function(...)
            local oldInput = self.entity.Input
            inputFunction(...)
            if oldInput ~= false and self.entity.Input == false then
                local actualInput = nil
                for inputReference,inputFunctionName in self.entity.Inputs do
                    if inputFunctionName == inputName then
                        actualInput = inputReference
                    end
                end
                if actualInput == nil then
                    warn("actual input is null, how tf was this called?")
                    return
                end

                local humrp = self.entity.RootPart
                warn(`entity actually did attack {self.entity.Name} {actualInput} {inputName}`)

                local writer2 = networker.writer(networker.messageIds.ENEMY_INPUT_FUNCTION)
                writer2:write(self.id)
                writer2:write(actualInput)
                writer2:write(humrp.Position)
                writer2:write(util.directionToDegreesY(util.anglesToLookVector(humrp.Rotation)))
                writer2:write(self.entity.InputCameraCFrame.Position - humrp.Position)
                local x,y,z = self.entity.InputCameraCFrame:ToEulerAnglesXYZ()
                writer2:write(Vector3.new(math.deg(x),math.deg(y),math.deg(z)))
                networker.sendToServer(writer2:free())
            end
        end
    end

    if not canUpdate then
        return
    end
    
    local lastNetworkedHealth = math.ceil(self.networkedHealth)
    local currentHealth = math.ceil(self.entity.Resources.Health)
    if currentHealth ~= lastNetworkedHealth then
        -- network entity health
        local healthWriter = networker.writer(networker.messageIds.ENEMY_HEALTH_CHANGE)
        healthWriter:write(self.id)
        healthWriter:write(currentHealth)
        networker.sendToServer(healthWriter:free())
        self.networkedHealth = currentHealth
    end

    local updateWriter = networker.writer(networker.messageIds.ENEMY_UPDATE)
    updateWriter:write(self.id)
    updateWriter:write(self.entity.RootPart.Position)
    updateWriter:write(util.directionToDegreesY(util.anglesToLookVector(self.entity.RootPart.Rotation)))
    updateWriter:write(self.entity.RootPart.Velocity)
    updateWriter:write(self.entity.RootPart.RotVelocity.Y)
    updateWriter:write(Vector2.new(self.entity.MoveDirection[1],self.entity.MoveDirection[2]))
    networker.sendToServer(updateWriter:free())
end

local lerp = function(a,b,t)
    return a+(b-a)*t
end

local _repeat = function(t: number, length: number)
    return math.clamp(t - math.floor(t / length) * length, 0, length)
end

local deltaAngle = function(current: number, target: number)
    local delta = _repeat((target - current), 360)
    if (delta > 180) then
        delta -= 360
    end
    return delta
end

function entity:OwnerlessUpdate(dt)
    self.entity.Resources.Health = self.networkedHealth

    if self.isHostEntity then
        -- host entity attacks are synced, lets lobotomize the entity
        self.entity.ProcessAI = function() end
    end

    self.entity.MoveDirection = {self.moveDirection.X,self.moveDirection.Y}

    local timeSinceUpdate = tick() - self.sinceUpdate
    local humrp = self.entity.RootPart

    local predictedVelocity = timeSinceUpdate < (1/updateRate+0.25) and self.velocity * dt or Vector3.zero
    local predictedRVelocity = timeSinceUpdate < (1/updateRate+0.25) and self.rotvelocity * dt or 0

    self.position += predictedVelocity

    self.characterAtUpdateReceived += predictedVelocity
    self.rotationAtUpdateReceived += predictedRVelocity

    self.interpolatedPosition = self.characterAtUpdateReceived:Lerp(self.position,math.clamp(((timeSinceUpdate)*updateRate)*0.75,0,1))
    self.interpolatedRotation = lerp(self.rotationAtUpdateReceived,self.rotationAtUpdateReceived + deltaAngle(self.rotationAtUpdateReceived,self.rotation),math.clamp(((timeSinceUpdate)*updateRate)*0.75,0,1))

    humrp.CFrame = CFrame.new(self.interpolatedPosition) * CFrame.Angles(0,math.rad(self.interpolatedRotation),0)

    self.entity.MovePosition = humrp.Position + (humrp.CFrame.LookVector*100)
    self.entity.FacingPosition = humrp.Position + (humrp.CFrame.LookVector*100)
    self.entity.TimeSpeed = 1
    self.entity.Facing = true

    if not self.isHostEntity then
        return
    end

    self.inputTimer -= dt

    local x,y,z = self.inputCameraCFrame:ToEulerAnglesXYZ()
    self.entity.InputCameraCFrame = CFrame.new(humrp.Position + self.inputCameraCFrame.Position) * CFrame.Angles(x,y,z)
    self.entity.Input = self.input
    self.entity.InputTimer = self.inputTimer

    local inputInput = self.entity.Inputs[self.input]
    if type(self.input) == "string" and inputInput ~= nil and type(inputInput) == "string" and self.inputTimer > 0 then
        self.entity.InputFunctions[inputInput]()
        if self.entity.Input == false then
            self.input = false
            self.inputTimer = 0
            self:OnAttackStarted(inputInput)
        end
    end
end

module.destroyEntity = destroyEntity

module.findByEntity = function(e)
    for _,targetEntity in entities do
        if targetEntity.entity == e then
            return targetEntity
        end
    end
end

module.findByEntityId = function(id)
    for _,targetEntity in entities do
        if targetEntity.entity and targetEntity.entity.Id == id then
            return targetEntity
        end
    end
end

module.findEntity = function(network)
    for _,targetEntity in getrenv()._G.Entities do
        if targetEntity == network.entity then
            return targetEntity
        end
    end
end

module.getEntityById = function(id)
    for _,targetEntity in entities do
        if targetEntity.id == id then
            return targetEntity
        end
    end
end

module.getEntityCount = function()
    local count = 0
    for _,_ in entities do
        count+=1
    end
    return count
end

module.disposeAll = function()
    entities = {}
end

module.__module = {
    [2] = function()
        networker.OnConnectionSuccess:Connect(function()
            networker.bindFunctionToMessage(networker.messageIds.ENEMY_DAMAGE,function(id,entityId,damageJson)
                local targetEntity = module.getEntityById(entityId)
                if not targetEntity then
                    warn(`entity {id} does not exist, cant damage!`)
                    return
                end
                module.localPlayerData = {
                    id = playerId,
                    smallId = playerSmallId,
                }

                local gotPlayer = networkPlayer.getPlayer(id)
                local damagedByPlayer = id == localPlayerModule.localPlayerData.smallId and getrenv()._G.Entities[1] or (gotPlayer and gotPlayer.entity or nil)
                if damagedByPlayer == nil then
                    return
                end

                local name = gotPlayer and gotPlayer.username or "YOU"

                print(`{name}: dealing damage to network entity {entityId}`)
                local decoded = util.decodeJson(damageJson)
                decoded.Target = targetEntity.entity.Id
                decoded.Source = damagedByPlayer.Id
                decoded.BypassHook = true
                decoded.WasDamageNetworked = true
                getrenv()._G.DamageRequest(decoded)
            end)
        
            networker.bindFunctionToMessage(networker.messageIds.ENEMY_UPDATE,function(id,position,rotation,velocity,rotVelocity,moveDir)
                local targetEntity = module.getEntityById(id)
                if not targetEntity then
                    warn(`entity {id} does not exist!`)
                    return
                end

                targetEntity:UpdateCFrame(position,rotation,velocity,rotVelocity,moveDir,false)
            end)
        
            networker.bindFunctionToMessage(networker.messageIds.ENEMY_INPUT_FUNCTION,function(id,input,position,rotation,cameraPosition,cameraRotation,moveDir)
                local targetEntity = module.getEntityById(id)
                if not targetEntity then
                    warn(`entity {id} does not exist, cannot call input function!`)
                    return
                end
            
                targetEntity:Input(input,0.5,CFrame.new(cameraPosition) * CFrame.Angles(math.rad(cameraRotation.X),math.rad(cameraRotation.Y),math.rad(cameraRotation.Z)))
                targetEntity:UpdateCFrame(position,rotation,targetEntity.velocity,targetEntity.rotvelocity,Vector2.zero,true)
            end)
        
            networker.bindFunctionToMessage(networker.messageIds.ENEMY_DEATH,function(id)
                local targetEntity = module.getEntityById(id)
                if not targetEntity then
                    warn(`entity {id} does not exist, cant kill!`)
                    return
                end
                targetEntity.dead = true
            end)
        
            networker.bindFunctionToMessage(networker.messageIds.ENEMY_HEALTH_CHANGE,function(id,health)
                local targetEntity = module.getEntityById(id)
                if not targetEntity then
                    warn(`entity {id} does not exist, cant sync health!`)
                    return
                end
                targetEntity.networkedHealth = health
            end)

            networker.bindFunctionToMessage(networker.messageIds.ENEMY_TALENT_GAINED,function(id,talentIndex)
                local targetEntity = module.getEntityById(id)
                if not targetEntity then
                    warn(`entity {id} does not exist, cant sync talent!`)
                    return
                end
                table.insert(targetEntity.talents,talentIndex)
                print(`enemy {id} gained talent {talentIndex}!`)

                if not targetEntity.alreadyAppliedTalents and targetEntity.entity then
                    return
                end

                targetEntity.entity.Talents[talentIndex] = true
                if targetEntity.entity.TalentFunctions[talentIndex] then
                    targetEntity.entity.TalentFunctions[talentIndex]()
                end
            end)
        
            local lastStage = nil
            local sinceLastUpdate = tick()
            rs.Heartbeat:Connect(function(dt)
                if not networker.connected then
                    return
                end
                if getrenv()._G.GameState == "Combat" and lastStage ~= gameModule.currentStage then
                    entities = {}
                    local targetStage = networkStage.getStageByName(gameModule.currentStage)
                    for index,entityData in targetStage and targetStage.entities or {} do
                        warn(`created network entity for id {index}`)
                        module.new(entityData.name,index,2,nil)
                    end
                    if targetStage == nil then
                        warn(`cannot create network entities, target stage {gameModule.currentStage} is not a network stage!`)
                    end
                    lastStage = gameModule.currentStage
                end
                if getrenv()._G.GameState ~= "Combat" then
                    lastStage = nil
                    entities = {}
                    return
                end
                local canUpdate = tick() - sinceLastUpdate > 1/updateRate
                for _,targetEntity in entities do
                    if targetEntity.markedDisposed then
                        continue
                    end
                    targetEntity.entity = module.findEntity(targetEntity)
        
                    if targetEntity.entity and targetEntity.entity.Dead then
                        if targetEntity.dead == false then
                            local deathWriter = networker.writer(networker.messageIds.ENEMY_DEATH)
                            deathWriter:write(targetEntity.id)
                            networker.sendToServer(deathWriter:free())
                        end
                        targetEntity.markedDisposed = true
                        continue
                    end
                    if targetEntity.entity == nil then
                        continue
                    end
                    if targetEntity.dead then
                        targetEntity.entity.Resources.Health = 0
                        continue
                    end
                    if targetEntity.owner == localPlayerModule.localPlayerData.smallId then
                        targetEntity:OwnerUpdate(dt,canUpdate)
                        continue
                    end
                    targetEntity:OwnerlessUpdate(dt)
                end
                if canUpdate then
                    sinceLastUpdate = tick()
                end
            end)
        end)
    end,
}

module.new = function(...)
    return entity.new(...)
end

module.setEntityData = function(replaceEnemy,networkId,hoursEntity,spawnCFrame)
    task.spawn(function()
        repeat
            task.wait()
        until #entities > 0
        for _,entityData in entities do
            if entityData.id ~= networkId then
                continue
            end
            warn(`set hours entity for network entity {networkId}`)
            entityData.name = replaceEnemy
            entityData.entity = hoursEntity
            entityData.interpolatedPosition = spawnCFrame
            entityData.position = spawnCFrame
            entityData.characterAtUpdateReceived = spawnCFrame
        end
    end)
end

return module
﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;
using Proxy.Server.Stage;
using Proxy.Network.DataTypes;

namespace Proxy.Network.Messages
{
    /// <summary>
    /// when a player connects they identify themselves
    /// registered players get visually created in the roblox client code
    /// </summary>
    public class PLAYER_REGISTER : NetworkMessage
    {
        public override byte MessageId()
        {
            return MessageIds.PLAYER_REGISTER;
        }

        public override void RobloxReceived(RobloxReader data)
        {
            ulong userId = data.ReadUlong();

            if (PlayerManager.GetPlayerByUserId(userId) != null)
            {
                Console.WriteLine($"player {userId} already exists!");
                return; // we already registered
            }

            Console.WriteLine($"{userId}, registering...");

            NetworkWriter writer = new();
            writer.Write(userId);

            NetworkSender.SendToServer(MessageId(), SendType.Reliable, writer.Create());
        }

        public override void ReceivedServer(SteamId by, byte[] data) {
            if (PlayerManager.GetPlayerBySteamId(by) != null) {
                return; // this player already registered
            }

            NetworkReader reader = new(data);
            ulong playerUserId = reader.ReadUlong();

            Console.WriteLine($"registered player\nroblox userid: {playerUserId}\nsteam id: {by}");

            Player player = new(playerUserId, by) {
                smallId = Player.globalSmallId,
            };
            Player.globalSmallId++;

            foreach (Player otherPlayer in PlayerManager.players) {
                if (otherPlayer == player)
                    continue;
                NetworkWriter writer1 = new PlayerRegisterData() {
                    robloxId = otherPlayer.robloxUserId,
                    steamId = otherPlayer.steamId,
                    smallId = otherPlayer.smallId,
                }.Encode();

                NetworkSender.SendToClient(MessageId(), by, SendType.Reliable, writer1.Create());

                if (otherPlayer.playerClass != null) {
                    NetworkWriter writer2 = new();
                    writer2.Write(otherPlayer.smallId);
                    writer2.Write(false);
                    writer2.Write(otherPlayer.playerClass);

                    NetworkSender.SendToClient(MessageIds.PLAYER_CLASS, by, SendType.Reliable, writer2.Create());
                } else {
                    Logger.Log($"{otherPlayer.robloxUserId} does not have a class!, cannot catch up!");
                }
            }

            NetworkWriter writer = new PlayerRegisterData() {
                robloxId = playerUserId,
                steamId = by,
                smallId = player.smallId,
            }.Encode();

            NetworkSender.SendToClients(MessageId(), writer.Create(), SendType.Reliable);

            foreach (Stage stage in StageManager.stages) {
                if (stage.stageData == null)
                    continue;

                NetworkWriter stageWriter = new StageData() {
                    stage = stage,
                }.Encode();

                NetworkSender.SendToClient(MessageIds.ENEMY_STAGE_DATA, by, SendType.Reliable, stageWriter.Create());
            }
        }

        public override void ReceivedClient(byte[] data)
        {
            PlayerRegisterData playerRegister = new(data);

            if (!NetworkHandler.IsServer)
                new Player(playerRegister.robloxId, playerRegister.steamId) {
                    smallId = playerRegister.smallId,
                };

            Console.WriteLine($"received register player from server {playerRegister.robloxId} {playerRegister.steamId} {playerRegister.smallId}");

            RobloxWriter writer = new(MessageId());
            writer.WriteString(playerRegister.robloxId.ToString());
            writer.WriteString(playerRegister.smallId.ToString());

            NetworkSender.SendToRoblox(writer);
        }
    }
}

﻿using Proxy.Hours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Server.Stage
{
    public class Stage
    {
        public List<EntityStageData>? stageData = null;
        public virtual void CreateStageData() { }

        public virtual string[] Name() {
            return new[] {""};
        }

        public virtual HoursStageIds StageId() {
            return HoursStageIds.sinkingSands;
        }

        private static readonly double arenaSize = 60.0;
        public static Vector3 GetRandomPosition() {
            Random random = new();
            return new Vector3((float)(((random.NextDouble() - 0.5)*2) * arenaSize),0,(float)(((random.NextDouble() - 0.5)*2) * arenaSize));
        }
    }
}

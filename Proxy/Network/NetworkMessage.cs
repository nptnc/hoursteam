﻿using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Network {
    public class NetworkMessage {
        public virtual byte MessageId() { return 0; }
        public Type originalType;

        public virtual void RobloxReceived(RobloxReader data) { }
        public virtual void ReceivedServer(SteamId steamId,byte[] data) { }
        public virtual void ReceivedClient(byte[] data) { }
    }
}

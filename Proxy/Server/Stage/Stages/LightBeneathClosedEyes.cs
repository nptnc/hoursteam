﻿using Proxy.Hours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Server.Stage.Stages {
    public class LightBeneathClosedEyes : Stage {
        public override void CreateStageData() {
            stageData = new() {
                new EntityStageData("Residenta", Vector3.Zero),
                new EntityStageData("Residentc", Vector3.Zero),
            };
        }

        public override HoursStageIds StageId() {
            return HoursStageIds.lightBeneathClosedEyes;
        }

        public override string[] Name() {
            return new[] { "Dream" };
        }
    }
}

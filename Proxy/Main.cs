﻿using Fleck;
using Proxy.Network;
using Proxy.Network.Messages;
using Proxy.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy
{
    internal static class Main {
        internal static string seperator { get; private set; } = "|";
        internal static IWebSocketConnection? websocket = null;
        internal static ulong robloxUserId = 0;

        internal static void Init() {
            Game.ReturnedToMenu();

            var server = new WebSocketServer("ws://0.0.0.0:8181");
            server.Start(socket =>
            {
                socket.OnOpen = () => {
                    Logger.Log("Connected to proxy",ConsoleColor.Green);
                    websocket = socket;

                    if (NetworkHandler.clientSocket != null && NetworkHandler.clientSocket.Connected) {
                        RobloxWriter writer = new(MessageIds.CONNECTED_TO_STEAM);
                        NetworkSender.SendToRoblox(writer);
                    }
                };
                socket.OnClose = () => {
                    Logger.Log("Disconnected from proxy", ConsoleColor.Green);

                    NetworkHandler.Disconnect();

                    websocket = null;
                };
                socket.OnMessage = message => {
                    try {
                        RobloxReader reader = new RobloxReader(message);
                        int messageId = reader.ReadInt();

                        NetworkMessage? networkmessage = NetworkHandler.messages.FirstOrDefault(a => a.MessageId() == messageId);
                        if (networkmessage == null) {
                            Logger.Error($"message {messageId} could not be found.");
                            return;
                        }

                        networkmessage.RobloxReceived(reader);
                    } catch(Exception ex) {
                        Logger.Error($"an error occured during message \"{message}\"\n{ex}");
                    }
                };
            });
        }

        internal static void Update() {
            NetworkHandler.Update();
            Game.Update();
        }
    }
}

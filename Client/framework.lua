local getListOfModules = function()
	local result = {}

    for _,moduleFolder in listfiles(`HourSteam/Client/Modules`) do
        moduleFolder = string.gsub(moduleFolder,"\\","/")

        local folderName = string.split(moduleFolder,"/")
        folderName = folderName[#folderName]
        for _,module in listfiles(moduleFolder) do
            module = string.gsub(module,"\\","/")
			
			local moduleName = string.split(module,"/")
			moduleName = moduleName[#moduleName]
			moduleName = string.gsub(moduleName,".lua","")

			local success = pcall(function()
				local source = loadstring(readfile(module))()
	
				warn(moduleName)
	
				table.insert(result,{
					moduleName = moduleName,
					source = source,
				})
			end)
			if not success then
				error(moduleName)
			end
        end
    end

	return result
end

if _G.list == nil then
	_G.list = getListOfModules()

	_G.alreadyInitialized = {}
	_G.initialized = false
end

-- exploits fucked, it does something weird so we insert instead of setting index directly
local getByName = function(name)
	for _,data in _G.list do
		if data.moduleName == name then
			return data
		end
	end
end

local getByNameAlready = function(name)
	for _,data in _G.alreadyInitialized do
		if data.moduleName == name then
			return data
		end
	end
end

local getModule = function(target)
	local found = getByName(target)
	if found then
		return found.source
	end
	error(`fuck couldnt find {target}`)
end
	
local res = function(target)
	if _G.initialized then
		return
	end
	_G.framework = res
	_G.initialized = true

	print("-----------------------------")

	-- after the chaos, section each function order
	
	local functionOrder = {}
	
	for _,module in ipairs(_G.list) do
		if module.source.setModules then
			module.source.setModules(getModule)
		end

		if not module.source.__module then
			continue
		end
		
		for order,func in module.source.__module do
			if not functionOrder[order] then
				functionOrder[order] = {}
			end
			table.insert(functionOrder[order],func)
		end
	end
	
	-- do some cursed shit here...
	local orders = {}
	for order,_ in functionOrder do
		table.insert(orders,order)
	end
	
	table.sort(orders,function(a,b)
		return a < b
	end)

	for _,order in orders do
		print(`running order {order}`)
		for _,func in functionOrder[order] do
			func()
		end
	end
end

return res
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Hours {
    public enum HoursStageIds {
        sinkingSands,
        peacockPeaks,
        downcastDays,
        miraiMirror,
        lightBeneathClosedEyes,
        endlessTimeless,
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Proxy.Hours;
using Proxy.Server;
using Proxy.Server.Stage.Stages;
using Steamworks;
using Steamworks.Data;

namespace Proxy.Network.Messages {
    /// <summary>
    /// (start, stop) steam server!
    /// </summary>
    public class GAME_END : NetworkMessage {
        public override byte MessageId() {
            return MessageIds.GAME_END;
        }

        public override void RobloxReceived(RobloxReader data) {
            NetworkSender.SendToServer(MessageId(), SendType.Reliable, Array.Empty<byte>());
        }

        public override void ReceivedServer(SteamId by, byte[] data) {
            if (by != SteamClient.SteamId)
                return;
            Game.ReturnedToMenu();

            NetworkSender.SendToClients(MessageId(), Array.Empty<byte>(), SendType.Reliable, new ulong[] { by });
        }

        public override void ReceivedClient(byte[] data) {
            NetworkSender.SendToRoblox(new(MessageId()));
        }
    }
}

﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;
using Proxy.Hours;

namespace Proxy.Network.Messages {
    // TODO: make this message input a boolean for whether or not the player started endless
    // migrating to this boolean instead of vector3 means that in the tutorial level a player couldn't teleport all other players to a target position

    /// <summary>
    /// while in Sandy Blue, we send this message to teleport players to the edge of the map so the game would begin
    /// </summary>
    public class ROUND_END : NetworkMessage {
        public override byte MessageId() {
            return MessageIds.ROUND_END;
        }

        public override void RobloxReceived(RobloxReader data) {
            NetworkWriter writer = new();
            NetworkSender.SendToServer(MessageId(),SendType.Reliable,writer.Create());
        }

        public override void ReceivedServer(SteamId by,byte[] data) {
            if (by != SteamClient.SteamId)
                return;

            foreach (Entity ent in EntityManager.entities) {
                ent.Died();
            }
            EntityManager.entities.Clear();

            NetworkSender.SendToClients(MessageId(), new byte[] { }, SendType.Reliable,new ulong[] { by });
        }

        public override void ReceivedClient(byte[] data) {
            foreach (Entity ent in EntityManager.entities) {
                ent.Died();
            }
            EntityManager.entities.Clear();
        }
    }
}

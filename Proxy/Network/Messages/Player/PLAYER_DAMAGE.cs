﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;

namespace Proxy.Network.Messages
{
    /// <summary>
    /// when a player took damage, it syncs their stun and effects
    /// </summary>
    public class PLAYER_DAMAGE : NetworkMessage
    {
        public override byte MessageId()
        {
            return MessageIds.PLAYER_DAMAGE;
        }

        public override void RobloxReceived(RobloxReader data)
        {
            int entityId = data.ReadInt();
            bool isPlayer = data.ReadBool();
            string encodedDamageJson = data.ReadString();

            NetworkWriter writer = new NetworkWriter();
            writer.Write((ushort)entityId);
            writer.Write(isPlayer);
            writer.Write(encodedDamageJson);

            NetworkSender.SendToServer(MessageId(), SendType.Reliable, writer.Create());
        }

        public override void ReceivedServer(SteamId by, byte[] data)
        {
            Player? player = PlayerManager.GetPlayerBySteamId(by);
            if (player == null)
                return;

            NetworkReader reader = new(data);
            ushort entityId = reader.ReadUShort();
            bool isPlayer = reader.ReadBool();
            string encodedDamageJson = reader.ReadString();

            NetworkWriter writer = new();
            writer.Write(player.smallId);
            writer.Write(entityId);
            writer.Write(isPlayer);
            writer.Write(encodedDamageJson);

            NetworkSender.SendToClients(MessageId(), writer.Create(), SendType.Reliable, new ulong[] { by });
        }

        public override void ReceivedClient(byte[] data)
        {
            NetworkReader reader = new NetworkReader(data);
            byte id = reader.ReadByte();
            ushort entityId = reader.ReadUShort();
            bool isPlayer = reader.ReadBool();
            string encodedDamageJson = reader.ReadString();

            RobloxWriter writer = new(MessageId());
            writer.WriteString(id.ToString());
            writer.WriteString(entityId.ToString());
            writer.WriteBool(isPlayer);
            writer.WriteString(encodedDamageJson);

            NetworkSender.SendToRoblox(writer);
        }
    }
}
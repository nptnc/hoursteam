﻿using Proxy.Hours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Server.Stage.Stages {
    public class PeacockPeaks : Stage {
        public List<string> randomEnemies = new() { "Maul","Wraith","Blossom","Nowing","Agni","Stubbs","Vile","Infinity","Shatter","Gale" };

        public override void CreateStageData() {
            List<string> remainingRandomEnemies = new();
            foreach (string enemy in randomEnemies) {
                remainingRandomEnemies.Add(enemy);
            }

            stageData = new();
            for (int i = 0; i < Game.GetEntityCount(); i++) {
                int randomIndex = new Random().Next(0,remainingRandomEnemies.Count-1);
                stageData.Add(new EntityStageData(remainingRandomEnemies[randomIndex]));
                remainingRandomEnemies.RemoveAt(randomIndex);
                remainingRandomEnemies.Sort();
            }

            stageData.Add(new EntityStageData("Dusk"));
        }

        public override HoursStageIds StageId() {
            return HoursStageIds.peacockPeaks;
        }

        public override string[] Name() {
            return new[] { "RoseCity", "CloudCity", "DreadCity" };
        }
    }
}

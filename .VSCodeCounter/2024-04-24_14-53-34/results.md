# Summary

Date : 2024-04-24 14:53:34

Directory d:\\ro-exec\\workspace\\HourSteam

Total : 69 files,  4931 codes, 105 comments, 892 blanks, all 5928 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 56 | 2,517 | 73 | 504 | 3,094 |
| Lua | 10 | 2,301 | 29 | 374 | 2,704 |
| Markdown | 1 | 67 | 0 | 7 | 74 |
| XML | 2 | 46 | 3 | 7 | 56 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 69 | 4,931 | 105 | 892 | 5,928 |
| . (Files) | 1 | 67 | 0 | 7 | 74 |
| Client | 10 | 2,301 | 29 | 374 | 2,704 |
| Client (Files) | 1 | 35 | 1 | 2 | 38 |
| Client\\Modules | 9 | 2,266 | 28 | 372 | 2,666 |
| Client\\Modules\\Network | 4 | 932 | 7 | 155 | 1,094 |
| Client\\Modules\\Other | 3 | 699 | 4 | 99 | 802 |
| Client\\Modules\\Sync | 2 | 635 | 17 | 118 | 770 |
| Proxy | 58 | 2,563 | 76 | 511 | 3,150 |
| Proxy (Files) | 5 | 138 | 1 | 26 | 165 |
| Proxy\\Network | 35 | 1,983 | 72 | 407 | 2,462 |
| Proxy\\Network (Files) | 7 | 437 | 1 | 88 | 526 |
| Proxy\\Network\\Messages | 26 | 1,450 | 71 | 299 | 1,820 |
| Proxy\\Network\\Messages (Files) | 4 | 109 | 12 | 23 | 144 |
| Proxy\\Network\\Messages\\Enemies | 7 | 382 | 7 | 70 | 459 |
| Proxy\\Network\\Messages\\Player | 10 | 744 | 36 | 146 | 926 |
| Proxy\\Network\\Messages\\Talent | 5 | 215 | 16 | 60 | 291 |
| Proxy\\Network\\Roblox | 2 | 96 | 0 | 20 | 116 |
| Proxy\\Properties | 1 | 16 | 3 | 0 | 19 |
| Proxy\\Properties\\PublishProfiles | 1 | 16 | 3 | 0 | 19 |
| Proxy\\Server | 17 | 426 | 0 | 78 | 504 |
| Proxy\\Server (Files) | 6 | 134 | 0 | 26 | 160 |
| Proxy\\Server\\Stage | 11 | 292 | 0 | 52 | 344 |
| Proxy\\Server\\Stage (Files) | 3 | 83 | 0 | 12 | 95 |
| Proxy\\Server\\Stage\\Stages | 8 | 209 | 0 | 40 | 249 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)
﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;

namespace Proxy.Network.Messages
{
    /// <summary>
    /// this does not work and isnt implemented
    /// </summary>
    public class PLAYER_DISCONNECT : NetworkMessage
    {
        public override byte MessageId()
        {
            return MessageIds.DISCONNECT;
        }

        public override void RobloxReceived(RobloxReader data)
        {
            return; // shouldnt do anything
        }

        public override void ReceivedServer(SteamId by, byte[] data)
        {
            return;
        }

        public override void ReceivedClient(byte[] data)
        {
            NetworkReader reader = new(data);
            byte smallId = reader.ReadByte();

            RobloxWriter writer = new(MessageId());
            writer.WriteString(smallId.ToString());

            NetworkSender.SendToRoblox(writer);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy {
    public class Logger {
        public static void Log(string txt, ConsoleColor color = ConsoleColor.White) {
            Console.ForegroundColor = color;
            Console.WriteLine(txt);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void LogRaw(string txt, ConsoleColor color = ConsoleColor.White) {
            Console.ForegroundColor = color;
            Console.Write(txt);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void Error(string txt) {
            LogRaw("| ",ConsoleColor.White);
            Log(txt, ConsoleColor.Red);
        }
    }
}

local module = {}

module.setModules = function(framework)
    util = framework("Util")
    ui = framework("UI")
    networker = framework("Networker")
    localPlayerModule = framework("Player")
end

local players = {}
local player = {}
player.__index = player

player.new = function(position,rotation,serverId)
    position = position or Vector3.zero
    rotation = rotation or Vector3.zero
    username = "Pending Username"
    serverId = serverId or 1
    
    local self = setmetatable({},player)
    self.entity = nil

    self.position = position
    self.rotation = 0
    self.interpolatedPosition = self.position
    self.interpolatedRotation = self.rotation
    self.positionAtNetworked = self.position
    self.rotationAtNetworked = self.rotation
    self.sinceUpdate = 0
    self.moveDirection = {0,0}
    self.health = 100

    self.predictionVelocity = Vector3.zero
    self.predictionRVelocity = 0

    self.input = false
    self.inputTimer = 0
    self.inputCameraCFrame = CFrame.new()

    self.id = serverId
    self.username = username
    self.chosenCard = false

    self.entityId = nil
    self.talents = {}
    self.lastEntityDamagedBy = nil

    self.networkDebug = {}
    self.networkedActionFunction = nil

    table.insert(players,self)
    return self
end

function player:CreateEntity()
    if self.entityId == nil then
        return
    end
    if not util.isTableNull(self.entity) then
        return
    end
    if getrenv()._G.GameState ~= "Combat" then
        return
    end
    if self.health <= 0 then
        return
    end

    local lookVector = self:LookVector()

    local entityId = getrenv()._G.SpawnCreature({
        Name = self.entityId,
        SpawnCFrame = CFrame.lookAt(self.position,self.position + (lookVector*100)),
        DamageTeam = 1,
        IsBoss = false,
        Pvp = true,
        Multiplayer = true,
        BypassHook = true,
    })

    if not entityId then
        return
    end

    self.entity = getrenv()._G.Entities[entityId]
    self.entity.Character.Name = `Pending Username`
    self.entity.IsPlayer = true

    util.hueShift(self.entity,self.id * 30)

    if self.entityId == "Class5318008" then
        local af = self.entity.ActionFunctions
        local oldSkillParry = af["SkillParry"]
        af["SkillParry"] = function(...)
            if self.networkedActionFunction ~= "SkillParry" then
                --warn("wasn't networked, stopping")
                return
            end
            self.networkedActionFunction = nil
            return oldSkillParry(...)
        end
    end

    --[[local aiControl = getrenv()._G.ScriptDatabase.AIControl
    aiControl.Init(self.entity)--]]

    self:ApplyTalents()
    self:ApplyName(self.username == "Pending Username" and "" or self.username)
end

function player:ApplyName(username) 
    self.username = username
    
    if self.entity == nil then
        return
    end

    self.entity.Character.Name = `{self.username}`

    local nametag = self.entity.Character.Head2.NameTag
    nametag.TextLabel.Text = self.username or ""
    nametag.TextLabel.TextColor3 = Color3.fromRGB(0,190,0)
end

function player:GetTalentCount()
    local talentCount = 0
    for _,_ in ipairs(self.talents) do
        talentCount += 1
    end
    return talentCount
end

function player:ApplyTalents()
    if util.isTableNull(self.entity) then
        self.entity = nil
        return
    end
    for _,talentId in ipairs(self.talents) do
        talentId = tonumber(talentId)
        
        local old = self.entity.Talents[talentId]
        self.entity.Talents[talentId] = true
        if old ~= true and self.entity.TalentFunctions[talentId] then
            self.entity.TalentFunctions[talentId]()
            warn(`called talent function {talentId} for {self.username}`)
        end
    end
end

function player:UpdatePlayer(targetPosition,targetRotation,predictionVelocity,predictionRVelocity,moveDir,instant)
    instant = instant ~= nil and instant or false

    if util.isTableNull(self.entity) then
        self.entity = nil
    end

    self.position = targetPosition
    self.rotation = targetRotation
    self.positionAtNetworked = instant and targetPosition or (self.entity ~= nil and self.entity.RootPart.Position or self.position)
    self.rotationAtNetworked = instant and self.rotation or (self.entity ~= nil and util.directionToDegreesY(util.anglesToLookVector(self.entity.RootPart.Rotation)) or self.rotation)
    self.interpolatedPosition = instant and targetPosition or self.interpolatedPosition
    self.interpolatedRotation = instant and self.rotation or self.interpolatedRotation
    self.predictionVelocity = predictionVelocity
    self.predictionRVelocity = predictionRVelocity
    self.moveDirection = {moveDir.X,moveDir.Y}
    self.sinceUpdate = tick()
end

function player:LookVector()
    return CFrame.Angles(0,math.rad(self.rotation),0).LookVector
end

function player:DestroyEntity()
    if not util.isTableNull(self.entity) then
        self.entity:DieBase()
        self.entity = nil
    end
end

function player:ChangeEntityId(newId)
    if self.entityId == newId then
        return
    end
    self.entityId = newId
    self:DestroyEntity()
end

function player:Input(input,inputTimer,cameraCF)
    if util.isTableNull(self.entity) then
        self.entity = nil
        return
    end
    self.inputCameraCFrame = cameraCF
    self.input = input
    self.inputTimer = inputTimer
end

function player:OnDeath()
    local killedByPlayer = module.findByEntity(self.lastEntityDamagedBy)
    if killedByPlayer then
        killedByPlayer = killedByPlayer.entity
    elseif not killedByPlayer and self.lastEntityDamagedBy == 1 then
        killedByPlayer = getrenv()._G.Entities[1]
    end
    if not killedByPlayer then
        return
    end
    killedByPlayer.Resources.Health += 65
    for _,cooldown in killedByPlayer.Cooldowns do
        cooldown.Charges = cooldown.MaxCharges
        cooldown.Cooldown = 0
    end
end

function player:OnAttackStarted(inputName)
    --print(`{self.username} successfully did input {inputName}`)
    self:UpdatePlayer(self.position,self.rotation,Vector3.zero,0,Vector2.new(self.moveDirection[1],self.moveDirection[2]),true)
end

module.getPlayers = function()
    return players
end

module.getPlayer = function(id)
    for _,p in players do
        if p.id == id then
            return p
        end
    end
end

module.findByEntity = function(hoursEntity)
    for _,p in players do
        if p.entity == hoursEntity then
            return p
        end
    end
end

module.findByEntityId = function(hoursEntityId)
    for _,p in players do
        if p.entity and p.entity.Id == hoursEntityId then
            return p
        end
    end
end

module.getCount = function()
    local len = 0
    for _,p in players do
        len += 1
    end
    return len
end

local rs = game:GetService("RunService")
module.__module = {
    [1] = function()
        networker.OnConnectionSuccess:Connect(function()
            networker.bindFunctionToMessage(networker.messageIds.PLAYER_REGISTER,function(playerId,playerSmallId)
                if playerId == game.Players.LocalPlayer.UserId then
                    return
                end
                warn(`registered new player {playerId} {displayName} {playerSmallId}`)
                local createdNetworkPlayer = player.new(Vector3.zero,Vector3.zero,playerSmallId)

                local success,result = pcall(function()
                    return game:GetService("UserService"):GetUserInfosByUserIdsAsync({
                        playerId,
                    })
                end)

                local displayName = "bob"
                if not success or (success and result[1] == nil) then
                    warn(`an error occured while trying to get the username of {playerId}!`)
                    displayName = tostring(playerId)
                else
                    displayName = result[1][networker.useDisplayName and "DisplayName" or "Username"]
                end

                createdNetworkPlayer:ApplyName(displayName)
            end)
        
            networker.bindFunctionToMessage(networker.messageIds.PLAYER_UPDATE,function(playerId,playerPosition,playerVelocity,playerRotation,playerRotationVelocity,moveDir,instant)
                local targetPlayer = module.getPlayer(playerId)
                if targetPlayer == nil then
                    return
                end
                targetPlayer:UpdatePlayer(playerPosition,playerRotation,playerVelocity,playerRotationVelocity,moveDir,instant)
            end)
        
            networker.bindFunctionToMessage(networker.messageIds.PLAYER_CLASS,function(playerId,class)
                print(`received player class {playerId} {class}`)
                local targetPlayer = module.getPlayer(playerId)
                if targetPlayer == nil then
                    warn(`player {playerId} doesnt exist cannot change class`)
                    return
                end
                targetPlayer:ChangeEntityId(class)
            end)
        
            networker.bindFunctionToMessage(networker.messageIds.PLAYER_INPUT_FUNCTION,function(playerId,playerInput,playerPosition,playerRotation,cameraPosition,cameraRotation,moveDir)
                local targetPlayer = module.getPlayer(playerId)
                if targetPlayer == nil then
                    return
                end
                
                targetPlayer:Input(playerInput,0.5,CFrame.new(cameraPosition) * CFrame.Angles(math.rad(cameraRotation.X),math.rad(cameraRotation.Y),math.rad(cameraRotation.Z)))
                targetPlayer:UpdatePlayer(playerPosition,playerRotation,targetPlayer.predictionVelocity,targetPlayer.predictionRVelocity,moveDir)
            end)
        
            networker.bindFunctionToMessage(networker.messageIds.PLAYER_ACTION_FUNCTION,function(playerId,playerInput)
                local targetPlayer = module.getPlayer(playerId)
                if targetPlayer == nil then
                    return
                end
                if targetPlayer.entity == nil then
                    return
                end
                if not targetPlayer.entity.ActionFunctions[playerInput] then
                    warn("action function doesn't exist")
                    return
                end
                targetPlayer.networkedActionFunction = playerInput
                targetPlayer.entity.ActionFunctions[playerInput]()
            end)
        
            local fixTable = function(targetTable)
                local newTable = {}
                for _,value in targetTable do
                    table.insert(newTable,value)
                end
                return newTable
            end
            
            networker.bindFunctionToMessage(networker.messageIds.DISCONNECT,function(playerId)
                local targetPlayer = module.getPlayer(playerId)
                if targetPlayer == nil then
                    return
                end
                targetPlayer:DestroyEntity()
                table.remove(players,table.find(players,targetPlayer))
                players = fixTable(players)
            end)
        
            networker.bindFunctionToMessage(networker.messageIds.TALENT_GAINED,function(playerId,talentIndex)
                local targetPlayer = module.getPlayer(playerId)
                if targetPlayer == nil then
                    warn(`player {playerId} is null cannot do add talent`)
                    return
                end
                if table.find(targetPlayer.talents,talentIndex) then
                    return -- they already have this, this means they've modified client code
                end
                table.insert(targetPlayer.talents,talentIndex)
                targetPlayer:ApplyTalents()
            end)
        
            networker.bindFunctionToMessage(networker.messageIds.TALENT_CLEAR,function(playerId)
                local targetPlayer = module.getPlayer(playerId)
                if targetPlayer == nil then
                    warn(`player {playerId} is null cannot talent clear`)
                    return
                end
                warn(`cleared {targetPlayer.username}'s talents!`)
                targetPlayer.talents = {}
                targetPlayer:DestroyEntity()
                targetPlayer:CreateEntity()
            end)
        
            networker.bindFunctionToMessage(networker.messageIds.TALENT_CHOSEN,function(id)
                for _,targetPlayer in players do
                    if targetPlayer.id == id then
                        targetPlayer.chosenCard = true
                    end
                end
            end)
        
            networker.bindFunctionToMessage(networker.messageIds.PLAYER_HEALTH,function(playerId,health)
                local targetPlayer = module.getPlayer(playerId)
                if targetPlayer == nil then
                    warn(`player {playerId} is null cannot set health`)
                    return
                end
                targetPlayer.health = health
                if not util.isTableNull(targetPlayer.entity) then
                    targetPlayer.entity.Resources.Health = targetPlayer.health -- kills the player cause the loop wont run if the entity doesnt exist
                end
                if health == 0 then
                    targetPlayer.entity = nil
                    targetPlayer:OnDeath()
                end
            end)
            
            networker.bindFunctionToMessage(networker.messageIds.PLAYER_DAMAGE,function(playerId,damagedById,isPlayer,json)
                local targetPlayer = module.getPlayer(playerId)
                if targetPlayer == nil then
                    print("TARGET PLAYER IS NULL!")
                    return
                end


                if util.isTableNull(targetPlayer.entity) then
                    print("TARGET PLAYER ENTITY IS NULL!")
                    return
                end

                local damagerNetworkPlayer = isPlayer and module.getPlayer(damagedById)
                if damagerNetworkPlayer and util.isTableNull(damagerNetworkPlayer) then
                    print("DAMAGE PLAYER IS NULL")
                    return
                end

                local damager = nil

                if isPlayer then
                    if damagedById ~= localPlayerModule.localPlayerData.smallId and damagerNetworkPlayer then
                        damager = damagerNetworkPlayer.entity
                    end
                    if damagedById == localPlayerModule.localPlayerData.smallId then
                        damager = getrenv()._G.Entities[1]
                    end
                end

                if not isPlayer then
                    local damagerNetworkEntity = networkEntity.getEntityById(damagedById)
                    damager = damagerNetworkEntity.entity
                end

                if damager == nil then
                    print("DAMAGER IS NULL!")
                    return
                end
        
                local decoded = util.decodeJson(json)
                decoded.BypassHook = true
                decoded.Target = targetPlayer.entity.Id
                decoded.Source = damager.Id
                targetPlayer.lastEntityDamagedBy = damager.Id
                getrenv()._G.DamageRequest(decoded)
            end)
        
            --player.new(Vector3.zero,Vector3.zero,"bob the test player",99)
        
            local lerp = function(a,b,t)
                return a+(b-a)*t
            end
            
            local _repeat = function(t: number, length: number)
                return math.clamp(t - math.floor(t / length) * length, 0, length)
            end
            
            local deltaAngle = function(current: number, target: number)
                local delta = _repeat((target - current), 360)
                if (delta > 180) then
                    delta -= 360
                end
                return delta
            end
        
            local elapsedTime = 0
            rs.Heartbeat:Connect(function(dt)
                elapsedTime += dt
        
                for _,networkPlayer in players do
                    networkPlayer:CreateEntity()
                    if getrenv()._G.GameState ~= "Combat" then
                        networkPlayer.entity = nil
                        continue
                    end
                    
                    if util.isTableNull(networkPlayer.entity) then
                        networkPlayer.input = false
                        networkPlayer.inputTimer = 0
                        continue
                    end
        
                    local timeSinceUpdate = tick() - networkPlayer.sinceUpdate
                    if timeSinceUpdate > 1/module.playerUpdateRate and networkPlayer.id == 99 then
                        networkPlayer:UpdatePlayer(Vector3.new(math.random(-10,10),0,math.random(-10,10)),Vector3.new(0,elapsedTime*100,0),Vector3.zero,Vector2.zero,false)
                    end
        
                    local humrp = networkPlayer.entity.RootPart
                    local lookVector = networkPlayer:LookVector()
        
                    networkPlayer.inputTimer = math.clamp(networkPlayer.inputTimer-dt,0,math.huge)
        
                    local x,y,z = networkPlayer.inputCameraCFrame:ToEulerAnglesXYZ()
                    networkPlayer.entity.InputCameraCFrame = CFrame.new(humrp.Position + networkPlayer.inputCameraCFrame.Position) * CFrame.Angles(x,y,z)
                    networkPlayer.entity.Input = networkPlayer.input
                    networkPlayer.entity.InputTimer = networkPlayer.inputTimer
        
                    local inputInput = networkPlayer.entity.Inputs[networkPlayer.input]
                    if type(networkPlayer.input) == "string" and inputInput ~= nil and networkPlayer.inputTimer > 0 then
                        networkPlayer.entity.InputFunctions[inputInput]()
                        if networkPlayer.entity.Input == false then
                            networkPlayer.input = false
                            networkPlayer.inputTimer = 0
                            networkPlayer:OnAttackStarted(inputInput)
                        end
                    end
        
                    local shouldPredict = timeSinceUpdate < (1/module.playerUpdateRate)+0.25
                    local predictionVelocity = (shouldPredict and networkPlayer.predictionVelocity or Vector3.zero)*dt
                    local predictionRVelocity = (shouldPredict and networkPlayer.predictionRVelocity or 0)*dt
        
                    networkPlayer.position += predictionVelocity
                    networkPlayer.rotation += predictionRVelocity
        
                    networkPlayer.positionAtNetworked += predictionVelocity
                    networkPlayer.rotationAtNetworked += predictionRVelocity
        
                    networkPlayer.interpolatedPosition = networkPlayer.positionAtNetworked:Lerp(networkPlayer.position,math.clamp(((timeSinceUpdate)*module.playerUpdateRate)*0.75,0,1))
                    
                    local difference = deltaAngle(networkPlayer.rotationAtNetworked,networkPlayer.rotation)
                    local interpolatedRotationY = lerp(0,difference,math.clamp(((timeSinceUpdate)*module.playerUpdateRate)*0.75,0,1))
                    
                    networkPlayer.interpolatedRotation = networkPlayer.rotationAtNetworked + interpolatedRotationY
        
                    networkPlayer.entity.MoveDirection = networkPlayer.moveDirection
                    networkPlayer.entity.MovePosition = humrp.Position + (lookVector*100)
                    networkPlayer.entity.FacingPosition = humrp.Position + (lookVector*100)
                    networkPlayer.entity.Resources.Health = networkPlayer.health
                    networkPlayer.entity.TimeSpeed = 1
                    networkPlayer.entity.Facing = true
                    networkPlayer.entity.DamageTeam = util.isPvpAllowed() and 3+networkPlayer.id or 1
        
                    networkPlayer:ApplyTalents()
        
                    humrp.Velocity = Vector3.zero
                    humrp.RotVelocity = Vector3.zero
                    humrp.CFrame = CFrame.new(networkPlayer.interpolatedPosition) * CFrame.Angles(0,math.rad(networkPlayer.interpolatedRotation),0)
                end
            end)
        end)
    end
}

module.playerUpdateRate = 20

return module
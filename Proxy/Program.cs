﻿// See https://aka.ms/new-console-template for more information
using Proxy;
using Proxy.Network;
using Proxy.Server.Stage;
using Steamworks;

NetworkHandler.RegisterMessages();
StageManager.RegisterTypes();

uint appid = 480;
SteamClient.Init(appid);

Main.Init();
while (true) {
    try {
        Main.Update();
    } catch(Exception ex) {
        Console.WriteLine(ex.ToString());
    }
}
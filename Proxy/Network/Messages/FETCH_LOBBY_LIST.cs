﻿using Steamworks;
using Steamworks.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Network.Messages {
    /// <summary>
    /// (start, stop) steam server!
    /// </summary>
    public class FETCH_LOBBY_LIST : NetworkMessage {
        public override byte MessageId() {
            return MessageIds.FETCH_LOBBY_LIST;
        }

        public override void RobloxReceived(RobloxReader data) {
            Logger.Log($"Fetching lobby list!",ConsoleColor.Yellow);

            LobbyQuery lobbies = SteamMatchmaking.LobbyList.FilterDistanceWorldwide();
            lobbies.FilterDistanceWorldwide();
            lobbies.WithMaxResults(int.MaxValue);
            lobbies.WithSlotsAvailable(int.MaxValue);
            Task<Lobby[]> returnedLobbies = lobbies.RequestAsync();

            while (!returnedLobbies.IsCompleted) { }

            Logger.Log("Fetched lobby list!",ConsoleColor.DarkBlue);

            RobloxWriter writer = new(MessageId());

            List<LobbyMetadata> actualLobbies = new();
            foreach (Lobby lobby in returnedLobbies.Result) {
                if (lobby.Owner.IsMe)
                    continue;
                LobbyMetadata metadata = new(lobby);
                metadata.Parse(lobby);

                if (metadata.ownerName == null || metadata.ownerId == null)
                    continue;
                actualLobbies.Add(metadata);
            }

            writer.WriteString(actualLobbies.Count.ToString());
            foreach (LobbyMetadata lobbyMetadata in actualLobbies) {
                writer.WriteString($"{lobbyMetadata.ownerName}");
                writer.WriteString($"{lobbyMetadata.ownerId}");
            }

            NetworkSender.SendToRoblox(writer);
        }
    }
}

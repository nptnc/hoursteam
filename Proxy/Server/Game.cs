﻿using Minesweeper.Multiplayer;
using Proxy.Hours;
using Proxy.Network;
using Proxy.Network.DataTypes;
using Proxy.Server.Stage;
using Steamworks.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Server {
    public class Game {
        internal static bool inTalentPopup = false;
        public static HoursStageIds stageId { get; private set; } = HoursStageIds.sinkingSands;
        internal static int endlessLevelId = 1;

        // modifiers
        internal static bool swarm = false;

        internal static void ReturnedToMenu() {
            Logger.Log("Returned to menu.",ConsoleColor.Blue);

            stageId = HoursStageIds.sinkingSands;
            endlessLevelId = 1;
            inTalentPopup = false;
            foreach (Player player in PlayerManager.players) {
                player.hasSelectedTalent = false;
            }

            CreateEntitiesOffStageData(StageManager.GetStageByEnum(stageId)!);
        }

        internal static void TalentSelectionBegan() {
            inTalentPopup = true;
        }

        public static int GetEntityCount() {
            return swarm ? 10 : 5;
        }

        public static void CreateEntitiesOffStageData(Stage.Stage stage) {
            EntityManager.entities = new(stage.stageData!.Count);

            for (int i = 0; i < stage.stageData!.Count; i++) {
                Entity entity = new((ushort)(i+1), stage.stageData![i].entityType, 2);
                Logger.Log($"created {entity.entityType} with id {entity.id}",ConsoleColor.DarkCyan);
            }
        }

        internal static void TalentSelectionStart(bool transitionToNextStage = true) {
            Stage.Stage stage = StageManager.GetStageByEnum(stageId)!;
            if (stage == null) {
                Logger.Error($"No stage was found with enum {stageId}");
                return;
            }

            if (stageId == HoursStageIds.endlessTimeless) {
                endlessLevelId++;
            }
            stage.CreateStageData();

            if (stageId != HoursStageIds.endlessTimeless && stageId != HoursStageIds.lightBeneathClosedEyes && transitionToNextStage) {
                stageId = (HoursStageIds)((int)stageId+1);
                stage = StageManager.GetStageByEnum(stageId)!;
            }

            if (stageId == HoursStageIds.endlessTimeless)
                Logger.Log($"Transitioned to {stage.GetType().Name} level {endlessLevelId}!",ConsoleColor.Blue);
            else
                Logger.Log($"Transitioned to {stage.GetType().Name}!", ConsoleColor.Blue);

            CreateEntitiesOffStageData(stage);

            NetworkWriter stageWriter = new StageData() {
                stage = stage,
            }.Encode();

            NetworkSender.SendToClients(MessageIds.ENEMY_STAGE_DATA, stageWriter.Create(), SendType.Reliable);
        }

        internal static void TalentSelectionStartGame(bool endless) {
            TalentSelectionBegan();

            stageId = endless ? HoursStageIds.endlessTimeless : HoursStageIds.peacockPeaks;
            endlessLevelId = 0;

            Logger.Log($"Talent selection start game! {(endless ? "endless" : "normal")}");

            TalentSelectionStart(false);
        }

        // fixing async bullshit, ignore the if statements
        internal static void CombatUpdate() {
            if (stageId == HoursStageIds.lightBeneathClosedEyes || stageId == HoursStageIds.sinkingSands)
                return;
            bool allDead = true;
            foreach (Entity entity in EntityManager.entities) {
                if (!entity.dead)
                    allDead = false;
            }
            if (stageId == HoursStageIds.lightBeneathClosedEyes || stageId == HoursStageIds.sinkingSands)
                return;
            if (allDead) {
                if (stageId == HoursStageIds.lightBeneathClosedEyes || stageId == HoursStageIds.sinkingSands)
                    return;
                inTalentPopup = true;
                TalentSelectionStart();
                Logger.Log("round ended.");
            }
        }

        internal static void Update() {
            if (!inTalentPopup) {
                CombatUpdate();
                return;
            }
            bool allSelected = true;
            foreach (Player player in PlayerManager.players) {
                if (!player.hasSelectedTalent)
                    allSelected = false;
            }
            if (!allSelected)
                return;
            inTalentPopup = false;
            Logger.Log("talent selection ended");
            foreach (Player player in PlayerManager.players) {
                player.hasSelectedTalent = false;
            }
        }
    }
}

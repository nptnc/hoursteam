# Details

Date : 2024-04-24 14:53:34

Directory d:\\ro-exec\\workspace\\HourSteam

Total : 69 files,  4931 codes, 105 comments, 892 blanks, all 5928 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [Client/Modules/Network/NetworkEntity.lua](/Client/Modules/Network/NetworkEntity.lua) | Lua | 243 | 3 | 33 | 279 |
| [Client/Modules/Network/NetworkPlayer.lua](/Client/Modules/Network/NetworkPlayer.lua) | Lua | 434 | 4 | 75 | 513 |
| [Client/Modules/Network/NetworkStage.lua](/Client/Modules/Network/NetworkStage.lua) | Lua | 50 | 0 | 11 | 61 |
| [Client/Modules/Network/Networker.lua](/Client/Modules/Network/Networker.lua) | Lua | 205 | 0 | 36 | 241 |
| [Client/Modules/Other/PvpArena.lua](/Client/Modules/Other/PvpArena.lua) | Lua | 259 | 3 | 41 | 303 |
| [Client/Modules/Other/UI.lua](/Client/Modules/Other/UI.lua) | Lua | 238 | 1 | 41 | 280 |
| [Client/Modules/Other/Util.lua](/Client/Modules/Other/Util.lua) | Lua | 202 | 0 | 17 | 219 |
| [Client/Modules/Sync/Game.lua](/Client/Modules/Sync/Game.lua) | Lua | 428 | 15 | 86 | 529 |
| [Client/Modules/Sync/Player.lua](/Client/Modules/Sync/Player.lua) | Lua | 207 | 2 | 32 | 241 |
| [Client/boot.lua](/Client/boot.lua) | Lua | 35 | 1 | 2 | 38 |
| [Proxy/Catchup.cs](/Proxy/Catchup.cs) | C# | 11 | 0 | 3 | 14 |
| [Proxy/Logger.cs](/Proxy/Logger.cs) | C# | 24 | 0 | 4 | 28 |
| [Proxy/Main.cs](/Proxy/Main.cs) | C# | 57 | 0 | 9 | 66 |
| [Proxy/Network/LobbyMetadata.cs](/Proxy/Network/LobbyMetadata.cs) | C# | 37 | 0 | 7 | 44 |
| [Proxy/Network/MessageIds.cs](/Proxy/Network/MessageIds.cs) | C# | 36 | 0 | 2 | 38 |
| [Proxy/Network/Messages/CONNECTED_TO_STEAM.cs](/Proxy/Network/Messages/CONNECTED_TO_STEAM.cs) | C# | 15 | 3 | 3 | 21 |
| [Proxy/Network/Messages/Enemies/ENEMY_ANIMATION_CHANGE.cs](/Proxy/Network/Messages/Enemies/ENEMY_ANIMATION_CHANGE.cs) | C# | 50 | 0 | 9 | 59 |
| [Proxy/Network/Messages/Enemies/ENEMY_DAMAGE.cs](/Proxy/Network/Messages/Enemies/ENEMY_DAMAGE.cs) | C# | 55 | 0 | 11 | 66 |
| [Proxy/Network/Messages/Enemies/ENEMY_DEATH.cs](/Proxy/Network/Messages/Enemies/ENEMY_DEATH.cs) | C# | 41 | 3 | 9 | 53 |
| [Proxy/Network/Messages/Enemies/ENEMY_HEALTH_CHANGE.cs](/Proxy/Network/Messages/Enemies/ENEMY_HEALTH_CHANGE.cs) | C# | 51 | 0 | 11 | 62 |
| [Proxy/Network/Messages/Enemies/ENEMY_STAGE_DATA.cs](/Proxy/Network/Messages/Enemies/ENEMY_STAGE_DATA.cs) | C# | 52 | 0 | 8 | 60 |
| [Proxy/Network/Messages/Enemies/ENEMY_STAT_CHANGE.cs](/Proxy/Network/Messages/Enemies/ENEMY_STAT_CHANGE.cs) | C# | 59 | 4 | 12 | 75 |
| [Proxy/Network/Messages/Enemies/ENEMY_UPDATE.cs](/Proxy/Network/Messages/Enemies/ENEMY_UPDATE.cs) | C# | 74 | 0 | 10 | 84 |
| [Proxy/Network/Messages/FETCH_LOBBY_LIST.cs](/Proxy/Network/Messages/FETCH_LOBBY_LIST.cs) | C# | 41 | 3 | 10 | 54 |
| [Proxy/Network/Messages/GAME_END.cs](/Proxy/Network/Messages/GAME_END.cs) | C# | 35 | 3 | 6 | 44 |
| [Proxy/Network/Messages/LEAVE_STEAM.cs](/Proxy/Network/Messages/LEAVE_STEAM.cs) | C# | 18 | 3 | 4 | 25 |
| [Proxy/Network/Messages/Player/PLAYER_ACTION_FUNCTION.cs](/Proxy/Network/Messages/Player/PLAYER_ACTION_FUNCTION.cs) | C# | 53 | 3 | 14 | 70 |
| [Proxy/Network/Messages/Player/PLAYER_CLASS.cs](/Proxy/Network/Messages/Player/PLAYER_CLASS.cs) | C# | 80 | 4 | 13 | 97 |
| [Proxy/Network/Messages/Player/PLAYER_DAMAGE.cs](/Proxy/Network/Messages/Player/PLAYER_DAMAGE.cs) | C# | 61 | 3 | 11 | 75 |
| [Proxy/Network/Messages/Player/PLAYER_DISCONNECT.cs](/Proxy/Network/Messages/Player/PLAYER_DISCONNECT.cs) | C# | 36 | 3 | 7 | 46 |
| [Proxy/Network/Messages/Player/PLAYER_HEALTH.cs](/Proxy/Network/Messages/Player/PLAYER_HEALTH.cs) | C# | 50 | 3 | 12 | 65 |
| [Proxy/Network/Messages/Player/PLAYER_INPUT_FUNCTION.cs](/Proxy/Network/Messages/Player/PLAYER_INPUT_FUNCTION.cs) | C# | 113 | 5 | 14 | 132 |
| [Proxy/Network/Messages/Player/PLAYER_PVP_DAMAGE.cs](/Proxy/Network/Messages/Player/PLAYER_PVP_DAMAGE.cs) | C# | 53 | 0 | 16 | 69 |
| [Proxy/Network/Messages/Player/PLAYER_REGISTER.cs](/Proxy/Network/Messages/Player/PLAYER_REGISTER.cs) | C# | 124 | 6 | 27 | 157 |
| [Proxy/Network/Messages/Player/PLAYER_STAT_CHANGE.cs](/Proxy/Network/Messages/Player/PLAYER_STAT_CHANGE.cs) | C# | 64 | 4 | 14 | 82 |
| [Proxy/Network/Messages/Player/PLAYER_UPDATE.cs](/Proxy/Network/Messages/Player/PLAYER_UPDATE.cs) | C# | 110 | 5 | 18 | 133 |
| [Proxy/Network/Messages/Talent/TALENT_CHOSEN.cs](/Proxy/Network/Messages/Talent/TALENT_CHOSEN.cs) | C# | 49 | 3 | 14 | 66 |
| [Proxy/Network/Messages/Talent/TALENT_CLEAR.cs](/Proxy/Network/Messages/Talent/TALENT_CLEAR.cs) | C# | 42 | 4 | 12 | 58 |
| [Proxy/Network/Messages/Talent/TALENT_GAINED.cs](/Proxy/Network/Messages/Talent/TALENT_GAINED.cs) | C# | 50 | 3 | 15 | 68 |
| [Proxy/Network/Messages/Talent/TALENT_POPUP.cs](/Proxy/Network/Messages/Talent/TALENT_POPUP.cs) | C# | 33 | 3 | 7 | 43 |
| [Proxy/Network/Messages/Talent/TALENT_POPUP_START.cs](/Proxy/Network/Messages/Talent/TALENT_POPUP_START.cs) | C# | 41 | 3 | 12 | 56 |
| [Proxy/Network/NetworkHandler.cs](/Proxy/Network/NetworkHandler.cs) | C# | 166 | 1 | 40 | 207 |
| [Proxy/Network/NetworkMessage.cs](/Proxy/Network/NetworkMessage.cs) | C# | 15 | 0 | 3 | 18 |
| [Proxy/Network/NetworkReader.cs](/Proxy/Network/NetworkReader.cs) | C# | 85 | 0 | 17 | 102 |
| [Proxy/Network/NetworkSender.cs](/Proxy/Network/NetworkSender.cs) | C# | 51 | 0 | 6 | 57 |
| [Proxy/Network/NetworkWriter.cs](/Proxy/Network/NetworkWriter.cs) | C# | 47 | 0 | 13 | 60 |
| [Proxy/Network/Roblox/RobloxReader.cs](/Proxy/Network/Roblox/RobloxReader.cs) | C# | 57 | 0 | 12 | 69 |
| [Proxy/Network/Roblox/RobloxWriter.cs](/Proxy/Network/Roblox/RobloxWriter.cs) | C# | 39 | 0 | 8 | 47 |
| [Proxy/Program.cs](/Proxy/Program.cs) | C# | 16 | 1 | 3 | 20 |
| [Proxy/Properties/PublishProfiles/FolderProfile.pubxml](/Proxy/Properties/PublishProfiles/FolderProfile.pubxml) | XML | 16 | 3 | 0 | 19 |
| [Proxy/Proxy.csproj](/Proxy/Proxy.csproj) | XML | 30 | 0 | 7 | 37 |
| [Proxy/Server/BaseEntity.cs](/Proxy/Server/BaseEntity.cs) | C# | 21 | 0 | 5 | 26 |
| [Proxy/Server/Entity.cs](/Proxy/Server/Entity.cs) | C# | 25 | 0 | 4 | 29 |
| [Proxy/Server/EntityManager.cs](/Proxy/Server/EntityManager.cs) | C# | 17 | 0 | 3 | 20 |
| [Proxy/Server/EntityStat.cs](/Proxy/Server/EntityStat.cs) | C# | 15 | 0 | 3 | 18 |
| [Proxy/Server/Player.cs](/Proxy/Server/Player.cs) | C# | 25 | 0 | 6 | 31 |
| [Proxy/Server/PlayerManager.cs](/Proxy/Server/PlayerManager.cs) | C# | 31 | 0 | 5 | 36 |
| [Proxy/Server/Stage/EntityStageData.cs](/Proxy/Server/Stage/EntityStageData.cs) | C# | 21 | 0 | 3 | 24 |
| [Proxy/Server/Stage/Stage.cs](/Proxy/Server/Stage/Stage.cs) | C# | 25 | 0 | 5 | 30 |
| [Proxy/Server/Stage/StageManager.cs](/Proxy/Server/Stage/StageManager.cs) | C# | 37 | 0 | 4 | 41 |
| [Proxy/Server/Stage/Stages/DowncastDays.cs](/Proxy/Server/Stage/Stages/DowncastDays.cs) | C# | 30 | 0 | 6 | 36 |
| [Proxy/Server/Stage/Stages/EndlessTimeless.cs](/Proxy/Server/Stage/Stages/EndlessTimeless.cs) | C# | 30 | 0 | 6 | 36 |
| [Proxy/Server/Stage/Stages/LightBeneathClosedEyes.cs](/Proxy/Server/Stage/Stages/LightBeneathClosedEyes.cs) | C# | 22 | 0 | 4 | 26 |
| [Proxy/Server/Stage/Stages/MiraiMirror.cs](/Proxy/Server/Stage/Stages/MiraiMirror.cs) | C# | 32 | 0 | 7 | 39 |
| [Proxy/Server/Stage/Stages/MiraiMirror2.cs](/Proxy/Server/Stage/Stages/MiraiMirror2.cs) | C# | 23 | 0 | 4 | 27 |
| [Proxy/Server/Stage/Stages/MiraiMirror3.cs](/Proxy/Server/Stage/Stages/MiraiMirror3.cs) | C# | 23 | 0 | 4 | 27 |
| [Proxy/Server/Stage/Stages/PeacockPeaks.cs](/Proxy/Server/Stage/Stages/PeacockPeaks.cs) | C# | 30 | 0 | 6 | 36 |
| [Proxy/Server/Stage/Stages/SinkingSands.cs](/Proxy/Server/Stage/Stages/SinkingSands.cs) | C# | 19 | 0 | 3 | 22 |
| [README.md](/README.md) | Markdown | 67 | 0 | 7 | 74 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)
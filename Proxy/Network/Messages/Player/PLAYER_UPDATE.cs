﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;

namespace Proxy.Network.Messages {
    /// <summary>
    /// player position, y rotation, positional velocity, rotational velocity and moveDirection
    /// </summary>
    public class PLAYER_UPDATE : NetworkMessage {
        public override byte MessageId() {
            return MessageIds.PLAYER_UPDATE;
        }

        // higher means loss of precision which i guess is fine
        float positionCap = 400;
        float velocityCap = 360;
        float rotationCap = 180; // roblox euler angles is between -180 and 180

        public override void RobloxReceived(RobloxReader data) {
            Vector3 position = data.ReadVector3();
            Vector3 velocity = data.ReadVector3();
            float rotation = data.ReadFloat();
            float rvelocity = data.ReadFloat();
            Vector2 moveDir = data.ReadVector2();
            moveDir = Vector2.Normalize(moveDir);

            NetworkWriter writer = new NetworkWriter();
            writer.Write((short)(position.X / positionCap * short.MaxValue));
            writer.Write((short)(position.Y / positionCap * short.MaxValue));
            writer.Write((short)(position.Z / positionCap * short.MaxValue));
            writer.Write((short)(velocity.X / velocityCap * short.MaxValue));
            writer.Write((short)(velocity.Y / velocityCap * short.MaxValue));
            writer.Write((short)(velocity.Z / velocityCap * short.MaxValue));
            writer.Write((short)(rotation / rotationCap * short.MaxValue));
            writer.Write((short)(rvelocity / velocityCap * short.MaxValue));
            writer.Write((short)(moveDir.X * short.MaxValue));
            writer.Write((short)(moveDir.Y * short.MaxValue));

            NetworkSender.SendToServer(MessageId(), SendType.Unreliable, writer.Create());
            // fml fml fml fml fml fml fml fml fml fml fml fml fml fml
        }

        public override void ReceivedServer(SteamId by, byte[] data) {
            NetworkReader reader = new NetworkReader(data);
            short positionX = reader.ReadShort();
            short positionY = reader.ReadShort();
            short positionZ = reader.ReadShort();
            short velocityX = reader.ReadShort();
            short velocityY = reader.ReadShort();
            short velocityZ = reader.ReadShort();
            short rotation = reader.ReadShort();
            short rvelocity = reader.ReadShort();
            short moveDirX = reader.ReadShort();
            short moveDirY = reader.ReadShort();

            Player? player = PlayerManager.GetPlayerBySteamId(by);
            if (player == null)
                return;

            float positionXF = (float)positionX / short.MaxValue * positionCap;
            float positionYF = (float)positionY / short.MaxValue * positionCap;
            float positionZF = (float)positionZ / short.MaxValue * positionCap;

            float velocityXF = (float)velocityX / short.MaxValue * velocityCap;
            float velocityYF = (float)velocityY / short.MaxValue * velocityCap;
            float velocityZF = (float)velocityZ / short.MaxValue * velocityCap;

            float rotationF = (float)rotation / short.MaxValue * rotationCap;

            float moveDirXF = (float)moveDirX / short.MaxValue;
            float moveDirYF = (float)moveDirY / short.MaxValue;

            player.position = new Vector3(positionXF, positionYF, positionZF);
            player.rotation = new Vector3(0, rotationF, 0);
            player.velocity = new Vector3(velocityXF, velocityYF, velocityZF);
            player.moveDirection = new Vector2(moveDirXF, moveDirYF);

            NetworkWriter writer = new NetworkWriter();
            writer.Write(positionX);
            writer.Write(positionY);
            writer.Write(positionZ);
            writer.Write(velocityX);
            writer.Write(velocityY);
            writer.Write(velocityZ);
            writer.Write(rotation);
            writer.Write(rvelocity);
            writer.Write(moveDirX);
            writer.Write(moveDirY);
            writer.Write(player.smallId);

            NetworkSender.SendToClients(MessageId(), writer.Create(), SendType.Unreliable, new ulong[] { by });
        }

        public override void ReceivedClient(byte[] data) {
            NetworkReader reader = new NetworkReader(data);
            float positionX = (float)reader.ReadShort() / short.MaxValue * positionCap;
            float positionY = (float)reader.ReadShort() / short.MaxValue * positionCap;
            float positionZ = (float)reader.ReadShort() / short.MaxValue * positionCap;
            float velocityX = (float)reader.ReadShort() / short.MaxValue * velocityCap;
            float velocityY = (float)reader.ReadShort() / short.MaxValue * velocityCap;
            float velocityZ = (float)reader.ReadShort() / short.MaxValue * velocityCap;
            float rotation = (float)reader.ReadShort() / short.MaxValue * rotationCap;
            float rvelocity = (float)reader.ReadShort() / short.MaxValue * velocityCap;
            float moveDirX = (float)reader.ReadShort() / short.MaxValue;
            float moveDirY = (float)reader.ReadShort() / short.MaxValue;
            byte smallId = reader.ReadByte();

            RobloxWriter writer = new(MessageId());
            writer.WriteString(smallId.ToString());
            writer.WriteVector3(new Vector3(positionX, positionY, positionZ));
            writer.WriteVector3(new Vector3(velocityX, velocityY, velocityZ));
            writer.WriteString(rotation.ToString());
            writer.WriteString(rvelocity.ToString());
            writer.WriteVector2(Vector2.Normalize(new Vector2(moveDirX, moveDirY)));

            NetworkSender.SendToRoblox(writer);
        }
    }
}

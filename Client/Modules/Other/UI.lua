local module = {}

module.setModules = function(framework)
    networker = framework("Networker")
    pvpArena = framework("PvpArena")
    layerManager = framework("LayerManager")
    localPlayerModule = framework("Player")
    networkPlayer = framework("NetworkPlayer")
    networkStage = framework("NetworkStage")
    util = framework("Util")
end

local debris = game:GetService("Debris")
local parent = game.Players.LocalPlayer.PlayerGui
local rs = game:GetService("RunService")

local initial_elements = {
    screengui = nil,
    text = nil,
    frame = nil,
}

local retries = 0
module.connectingToWebsocket = function()
    initial_elements.text.Text = retries > 0 and `Retrying connection to proxy [{retries}]\n<font size="7">You should probably launch the proxy</font>` or "Connecting to proxy..."
end

module.onFailedToConnect = function()
    retries += 1

    networker.attemptToCreateSocket()
    module.connectingToWebsocket()
end

local uis = game:GetService("UserInputService")
module.__module = {
    [100] = function()
        if layerManager.getLayer() == nil then
            initial_elements.text.Text = `Your executor is unsupported`
            return
        end
        module.connectingToWebsocket()
        networker.attemptToCreateSocket()
    end,
    [5] = function()
        initial_elements.screengui = Instance.new("ScreenGui")
        initial_elements.screengui.Parent = parent
        initial_elements.screengui.IgnoreGuiInset = true
    
        local frame = Instance.new("Frame")
        frame.Parent = initial_elements.screengui
        frame.Size = UDim2.new(0.1,0,0.1,0)
        frame.Transparency = 0.5
        frame.BorderSizePixel = 0
        frame.Position = UDim2.new(0.5,0,0.5,0)
        frame.AnchorPoint = Vector2.new(0.5,0.5)
        frame.BackgroundColor3 = Color3.fromRGB(0,0,0)
        initial_elements.frame = frame
    
        initial_elements.text = Instance.new("TextLabel")
        initial_elements.text.Parent = frame
        initial_elements.text.Position = UDim2.new(0.5,0,0.5,0)
        initial_elements.text.AnchorPoint = Vector2.new(0.5,0.5)
        initial_elements.text.Size = UDim2.new(1,0,1,0)
        initial_elements.text.BackgroundTransparency = 1
        initial_elements.text.TextColor3 = Color3.fromRGB(255,255,255)
        initial_elements.text.RichText = true
        initial_elements.text.Text = retries > 0 and `Retrying connection to proxy [{retries}]\n<font size="7">You should probably launch the proxy</font>` or "Connecting to proxy..."
        initial_elements.text.TextWrapped = true
    end,
    [2] = function()
        networker.OnConnectionSuccess:Connect(function()
            initial_elements.text:Destroy()
            initial_elements.frame:Destroy()
            initial_elements.screengui:Destroy()
            print("ran past there!")

            local screenGui = Instance.new("ScreenGui")
            screenGui.Parent = parent
            screenGui.DisplayOrder = 99999
            screenGui.IgnoreGuiInset = true
        
            local frame = Instance.new("Frame")
            frame.Parent = screenGui
            frame.Size = UDim2.new(0.15,0,0.15,0)
            frame.Transparency = 0.5
            frame.BorderSizePixel = 0
            frame.Position = UDim2.new(0.5,0,0.5,0)
            frame.AnchorPoint = Vector2.new(0.5,0.5)
            frame.BackgroundColor3 = Color3.fromRGB(0,0,0)
        
            local uilist = Instance.new("UIGridLayout")
            uilist.Parent = frame
            uilist.VerticalAlignment = Enum.VerticalAlignment.Top
            uilist.HorizontalAlignment = Enum.HorizontalAlignment.Center
            uilist.CellPadding = UDim2.new(0,0,0,10)
            uilist.CellSize = UDim2.new(1,0,0,20)
            uilist.SortOrder = Enum.SortOrder.LayoutOrder
            
            local elements = {}
            local elementCount = 0
        
            local categories = {}
        
            local registerNewCategory = function(categoryName)
                categories[categoryName] = {
                    elements = {},
                    isVisible = false,
                    destroyElements = function(except)
                        for _,element in categories[categoryName].elements do
                            if table.find(except,element.name) then
                                continue
                            end
                            element.destroy()
                        end
                        elements = {}
                    end
                }
                return categories[categoryName]
            end
        
            local displayCategory = function(visibleCategory)
                for _,category in categories do
                    for _,element in category.elements do
                        element.setVisible(false)
                    end
                    category.isVisible = false
                end
                for _,element in visibleCategory and visibleCategory.elements or {} do
                    element.setVisible(true)
                end
                if visibleCategory then
                    visibleCategory.isVisible = true
                end
            end
        
            local navigator = registerNewCategory("navigator")
            local server = registerNewCategory("server")
            --local serverList = registerNewCategory("server list") -- somehow fix this later, server list doesn't work
            local options = registerNewCategory("options")
        
            local globalTextIndex = 0
            local createText = function(targetText)
                local text = Instance.new("TextLabel")
                text.Parent = frame
                text.Position = UDim2.new(0.5,0,1,0)
                text.AnchorPoint = Vector2.new(0.5,1)
                text.Size = UDim2.new(1,0,1,0)
                text.BackgroundTransparency = 1
                text.TextColor3 = Color3.fromRGB(255,255,255)
                text.Text = targetText
                text.TextWrapped = true
                text.LayoutOrder = globalTextIndex
                table.insert(elements,text)
        
                elementCount += 1
            end
        
            local createButton = function(targetText,category,func)
                local newframe = Instance.new("Frame")
                newframe.Parent = frame
                newframe.BackgroundTransparency = 1
                newframe.LayoutOrder = globalTextIndex + #category.elements
                newframe.Visible = category.isVisible
                table.insert(elements,newframe)
                
                local text = Instance.new("TextButton")
                text.BackgroundColor3 = Color3.fromRGB(0,0,0)
                text.Parent = newframe
                text.Text = targetText
                text.TextColor3 = Color3.fromRGB(255,255,255)
                text.BackgroundTransparency = 0
                text.AnchorPoint = Vector2.new(0.5,0.5)
                text.Size = UDim2.new(0.75,0,1,0)
                text.Position = UDim2.new(0.5,0,0.5,0)
                text.TextScaled = true
                text.Visible = category.isVisible
                text.BorderColor3 = Color3.fromRGB(255,255,255)
                if func then
                    text.MouseButton1Click:Connect(func)
                end
        
                local returnedApi = {}
                returnedApi.name = targetText
                returnedApi.setVisible = function(visible)
                    text.Visible = visible or false
                    newframe.Visible = visible or false
                end
                returnedApi.changeText = function(t)
                    text.Text = t
                end
                returnedApi.destroy = function()
                    newframe:Destroy()
                end
        
                table.insert(category.elements,returnedApi)
                return returnedApi
            end

            local createToggle = function(targetText,category,enabled,func)
                local newframe = Instance.new("Frame")
                newframe.Parent = frame
                newframe.BackgroundTransparency = 1
                newframe.LayoutOrder = globalTextIndex + #category.elements
                newframe.Visible = category.isVisible
                table.insert(elements,newframe)
                
                local text = Instance.new("TextButton")
                text.BackgroundColor3 = Color3.fromRGB(0,0,0)
                text.Parent = newframe
                text.Text = targetText
                text.TextColor3 = Color3.fromRGB(255,255,255)
                text.BackgroundTransparency = 0
                text.AnchorPoint = Vector2.new(0.5,0.5)
                text.Size = UDim2.new(0.75,0,1,0)
                text.Position = UDim2.new(0.5,0,0.5,0)
                text.TextScaled = true
                text.Visible = category.isVisible
                text.BorderColor3 = Color3.fromRGB(255,255,255)

                local uistroke = Instance.new("UIStroke")
                uistroke.Parent = text
                uistroke.ApplyStrokeMode = Enum.ApplyStrokeMode.Border
                uistroke.Color = enabled and Color3.fromRGB(0,255,0) or Color3.fromRGB(255,0,0)
        
                text.MouseButton1Click:Connect(function()
                    enabled = not enabled
                    uistroke.Color = enabled and Color3.fromRGB(0,255,0) or Color3.fromRGB(255,0,0)
        
                    if not func then
                        return
                    end
                    func()
                end)

                local returnedApi = {}
                returnedApi.enabled = enabled
                returnedApi.name = targetText
                returnedApi.setVisible = function(visible)
                    text.Visible = visible or false
                    newframe.Visible = visible or false
                end
                returnedApi.changeText = function(t)
                    text.Text = t
                end
                returnedApi.destroy = function()
                    newframe:Destroy()
                end
        
                func(enabled)

                table.insert(category.elements,returnedApi)
                return returnedApi
            end

            local createSlider = function(targetText,category,startValue,endValue,currentValue,func)
                local newframe = Instance.new("Frame")
                newframe.Parent = frame
                newframe.BackgroundTransparency = 1
                newframe.LayoutOrder = globalTextIndex + #category.elements
                newframe.Visible = category.isVisible
                table.insert(elements,newframe)
                
                local text = Instance.new("TextButton")
                text.BackgroundColor3 = Color3.fromRGB(0,0,0)
                text.Parent = newframe
                text.Text = ""
                text.TextColor3 = Color3.fromRGB(255,255,255)
                text.BackgroundTransparency = 0
                text.AnchorPoint = Vector2.new(0.5,0.5)
                text.Size = UDim2.new(0.75,0,1,0)
                text.Position = UDim2.new(0.5,0,0.5,0)
                text.TextScaled = true
                text.Visible = category.isVisible
                text.BorderColor3 = Color3.fromRGB(255,255,255)

                local awesomeframe = Instance.new("Frame")
                awesomeframe.Parent = text
                awesomeframe.Active = false
                awesomeframe.Selectable = false
                awesomeframe.Size = UDim2.new(1,0,1,0)
                --awesomeframe.BorderSizePixel = 0
                awesomeframe.BackgroundColor3 = Color3.fromRGB(255,255,255)

                local returnedApi = {}

                local sliding = false
                text.MouseButton1Down:Connect(function()
                    sliding = true

                    print("sliding")

                    local loops = {}
                    local term = function()
                        for _,loop in loops do
                            loop:Disconnect()
                        end
                    end
                    table.insert(loops,game:GetService("RunService").Heartbeat:Connect(function()
                        local absoluteX = text.AbsolutePosition.X
                        local endX = absoluteX + text.AbsoluteSize.X
                        local mouseX = uis:GetMouseLocation().X
    
                        local ratio = math.clamp((mouseX - absoluteX) / (endX - absoluteX),0,1)
    
                        local lerp = function(a,b,t)
                            return a + (b - a) * t
                        end
    
                        currentValue = lerp(startValue,endValue,ratio)
  
                        awesomeframe.Size = UDim2.new(ratio,0,1,0)
    
                        func(returnedApi,currentValue)
                    end))
                    table.insert(loops,uis.InputEnded:Connect(function(i,g)
                        if i.UserInputType == Enum.UserInputType.MouseButton1 then
                            print("not sliding")
                            term()
                        end
                    end))
                end)

                returnedApi.changeSliderColor = function(color)
                    awesomeframe.BackgroundColor3 = color
                end
                returnedApi.name = targetText
                returnedApi.setVisible = function(visible)
                    text.Visible = visible or false
                    newframe.Visible = visible or false
                end
                returnedApi.changeText = function(t)
                    text.Text = t
                end
                returnedApi.destroy = function()
                    newframe:Destroy()
                end

                func(returnedApi,currentValue)
        
                table.insert(category.elements,returnedApi)
                return returnedApi
            end
        
            createText("HourSteam",navigator)
            createText(`Press [ to close ui`)
        
            createButton("server",navigator,function()
                displayCategory(server)
            end)
        
            createButton("options",navigator,function()
                displayCategory(options)
            end)
                    
            createButton("close ui",navigator,function()
                screenGui.Enabled = false
                if getrenv()._G.GameState ~= "Combat" then
                    return
                end
                if getrenv()._G.Pause then
                    return
                end
                getrenv()._G.SetCameraLock(not screenGui.Enabled)
            end)
        
           --[[createButton("server list",navigator,function()
                displayCategory(serverList)
                local writer = networker.writer(networker.messageIds.FETCH_LOBBY_LIST)
                networker.sendToServer(writer:free())
            end)--]]
        
            local hideKeybind = Enum.KeyCode.LeftBracket
        
            local creatingServer = false
            local inServer = false
            local createSteamServerButton;createSteamServerButton = createButton("create steam server",server,function()
                if inServer == false and creatingServer == false then
                    createSteamServerButton.changeText("creating steam server")
                    networker.createSteamServer()
                    creatingServer = true
                end
                if inServer and creatingServer == false then
                    networker.leaveSteamServer()
                    inServer = false
                    createSteamServerButton.changeText("create steam server")
                end
            end)
        
            createToggle("Friendly Fire",options,false,function(active)
                networker.pvpEnabled = active
            end)

            createToggle("Show Display Name",options,false,function(active)
                networker.useDisplayName = active
            end)

            createSlider("Chromashift Hue",options,0,360,0,function(self,value)
                networker.hue = value
                self.changeSliderColor(Color3.fromHSV(value/360,1,0.5))
            end)
        
            createButton("start pvp match",server,function()
                pvpArena.start()
            end)
            
            for _,category in categories do
                if category == navigator then
                    continue
                end
                createButton("back",category,function()
                    displayCategory(navigator)
                end)
            end
        
            -- no more category stuff past this point, do it ^
            displayCategory(navigator)
        
            networker.bindFunctionToMessage(networker.messageIds.CONNECTED_TO_STEAM,function()
                createSteamServerButton.changeText("leave server")
                creatingServer = false
                inServer = true
            end)
        
            networker.bindFunctionToMessage(networker.messageIds.FETCH_LOBBY_LIST,function(howMany,...)
                howMany = tonumber(howMany)
        
                serverList.destroyElements({"back"})
        
                local offset = 0
                for i = 1,howMany do
                    local args = {...}
                    local name = args[offset+1]
                    local id = tonumber(args[offset+1])
                    local newCat = registerNewCategory(id)
                    createButton(name,serverList,function()
                        displayCategory(newCat)
                    end)
                    offset += 2
                end
                if howMany == 0 then
                    createButton("no lobbies found",serverList,function()
                    end)
                end
            end)
        
            getrenv()._G.SetCameraLock(not screenGui.Enabled)
        
            uis.InputBegan:Connect(function(i,g)
                if g then return end
        
                if i.KeyCode == hideKeybind then
                    screenGui.Enabled = not screenGui.Enabled
                    if getrenv()._G.GameState ~= "Combat" then
                        return
                    end
                    if getrenv()._G.Pause then
                        return
                    end
                    getrenv()._G.SetCameraLock(not screenGui.Enabled)
                end
            end)
        end)
    end,
}

return module
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Server {
    public class Entity : BaseEntity {
        public ushort id;
        public string entityType;
        public byte team;
        public bool isStageEntity;

        public bool dead = false;
        public int health = 9999;

        public Entity(ushort id,string entityType,byte team,bool isStageEntity = false) {
            this.id = id;
            this.entityType = entityType;
            this.team = team;
            this.isStageEntity = isStageEntity;
            EntityManager.entities.Add(this);
        }

        public void Died() {
            dead = true;
            health = 0;
            Logger.Log($"entity {id} died!");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Network.Messages {
    /// <summary>
    /// (start, stop) steam server!
    /// </summary>
    public class CONNECTED_TO_STEAM : NetworkMessage {
        public override byte MessageId() {
            return MessageIds.CONNECTED_TO_STEAM;
        }

        public override void RobloxReceived(RobloxReader data) {
            NetworkHandler.StartServer();
        }
    }
}

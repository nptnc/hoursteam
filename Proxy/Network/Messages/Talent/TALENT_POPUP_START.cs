﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;
using Proxy.Hours;

namespace Proxy.Network.Messages {
    // TODO: make this message input a boolean for whether or not the player started endless
    // migrating to this boolean instead of vector3 means that in the tutorial level a player couldn't teleport all other players to a target position

    /// <summary>
    /// while in Sandy Blue, we send this message to teleport players to the edge of the map so the game would begin
    /// </summary>
    public class TALENT_POPUP_START : NetworkMessage {
        public override byte MessageId() {
            return MessageIds.TALENT_POPUP_START;
        }

        public override void RobloxReceived(RobloxReader data) {
            bool endless = data.ReadBool();

            NetworkWriter writer = new();
            writer.Write(endless);

            NetworkSender.SendToServer(MessageId(),SendType.Reliable,writer.Create());
        }

        public override void ReceivedServer(SteamId by,byte[] data) {
            NetworkReader reader = new(data);
            bool endless = reader.ReadBool();

            Player? player = PlayerManager.GetPlayerBySteamId(by);
            if (player == null || Game.inTalentPopup || Game.stageId != HoursStageIds.sinkingSands) {
                return;
            }

            Console.WriteLine($"RECEIVED TALENT_POPUP_START {endless}!");

            Game.TalentSelectionStartGame(endless);

            NetworkWriter writer = new();
            writer.Write(endless);

            NetworkSender.SendToClients(MessageId(), writer.Create(), SendType.Reliable, new ulong[] { by });
        }

        public override void ReceivedClient(byte[] data) {
            NetworkReader reader = new(data);
            bool endless = reader.ReadBool();

            Console.WriteLine($"RECEIVED TALENT_POPUP_START {endless} CLIENT!");

            RobloxWriter writer = new(MessageId());
            writer.WriteBool(endless);

            NetworkSender.SendToRoblox(writer);
        }
    }
}

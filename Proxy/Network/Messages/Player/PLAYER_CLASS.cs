﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;
using System.Xml;
using Proxy.Hours;

namespace Proxy.Network.Messages
{
    /// <summary>
    /// occurs when a player changes entity types
    /// when received by the client it sets the players entity type to the host they are playing as
    /// </summary>
    public class PLAYER_CLASS : NetworkMessage
    {
        public override byte MessageId()
        {
            return MessageIds.PLAYER_CLASS;
        }

        public override void RobloxReceived(RobloxReader data)
        {
            bool isNowNull = data.ReadBool();

            NetworkWriter writer = new NetworkWriter();
            writer.Write(isNowNull);
            if (isNowNull == false)
            {
                writer.Write(data.ReadString());
            }

            NetworkSender.SendToServer(MessageId(), SendType.Reliable, writer.Create());
            // fml fml fml fml fml fml fml fml fml fml fml fml fml fml
        }

        public override void ReceivedServer(SteamId by, byte[] data)
        {
            NetworkReader reader = new NetworkReader(data);
            bool isNowNull = reader.ReadBool();

            Player? player = PlayerManager.GetPlayerBySteamId(by);
            if (player == null)
                return;

            NetworkWriter writer = new NetworkWriter();
            writer.Write(player.smallId);
            writer.Write(isNowNull);
            if (isNowNull == false)
            {
                string playerClass = reader.ReadString();
                if (HoursEntityIds.getAll().FirstOrDefault(a => playerClass == a) == null)
                    return;

                player.playerClass = playerClass;
                writer.Write(playerClass);
                Console.WriteLine($"{player.robloxUserId} class is {playerClass}");
            }
            else
            {
                Console.WriteLine($"{player.robloxUserId} class is null");
            }

            NetworkSender.SendToClients(MessageId(), writer.Create(), SendType.Reliable, new ulong[] { by });
        }

        public override void ReceivedClient(byte[] data)
        {
            NetworkReader reader = new(data);
            byte smallId = reader.ReadByte();
            bool isNowNull = reader.ReadBool();

            Player? player = PlayerManager.GetPlayerByByteId(smallId);
            if (player == null) {
                return;
            }

            RobloxWriter writer = new(MessageId());
            writer.WriteString(smallId.ToString());
            if (!isNowNull)
            {
                string playerClass = reader.ReadString();
                if (HoursEntityIds.getAll().FirstOrDefault(a => playerClass == a) == null)
                    return;

                writer.WriteString(playerClass);
            }
            else
                Console.WriteLine($"received {player.robloxUserId} class null");

            NetworkSender.SendToRoblox(writer);
        }
    }
}

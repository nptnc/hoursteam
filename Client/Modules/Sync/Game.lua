local module = {}

module.setModules = function(framework)
    util = framework("Util")
    networkEntity = framework("NetworkEntity")
    networkStage = framework("NetworkStage")
    networkPlayer = framework("NetworkPlayer")
    networker = framework("Networker")
    localPlayerModule = framework("Player")
end

local lastMapName = "SandyBlue"
local getCurrentStage = function()
    if getrenv()._G.GameState == "MainScreen" then
        return "SandyBlue"
    end
    return lastMapName
end

local healthMultiplierBalancing = 0.8
local arenaHealthMultiplierBalancing = 0.6

module.justDied = false
module.currentStage = nil

local rs = game:GetService("RunService")
module.__module = {
    [100] = function()
        module.currentStage = getCurrentStage()
    end,
    [2] = function()
        networker.OnConnectionSuccess:Connect(function()
            local talentui = getrenv()._G.AllGui:WaitForChild("Talents",math.huge) -- wait for child fixes an error if you execute too early
            
            local talentScreen = function(yes)
                local toHide = {"Talent1","Talent2","Talent3","Skip"}
                for _,object in toHide do
                    talentui[object].Visible = not yes
                end
                talentui.Instruction.Text = yes and "Waiting for others." or "Choose an upgrade."
            end
        
            module.currentStage = getCurrentStage()
        
            local sandyBlueEntityList = {
                Entities = {"Dummy","DummyWalk"},
                Bosses = {},
            }
        
            local lastCapturedEntityList = sandyBlueEntityList
        
            local alreadyNetworked = false
            local iHaveChosen = nil
            local talentPopupVisible = false
            local old = getrenv()._G.TalentPopup
            getrenv()._G.TalentPopup = function(...)
                iHaveChosen = nil
        
                talentPopupVisible = true
        
                alreadyNetworked = false
        
                if lastMapName == "SandyBlue" and alreadyNetworked == false then
                    local writer2 = networker.writer(networker.messageIds.TALENT_POPUP_START)
                    -- TODO: check if getrenv()._G.Endless is valid
                    writer2:write(getrenv()._G.ArenaMode)
                    networker.sendToServer(writer2:free())
                end

                if lastMapName ~= "SandyBlue" and networker.IsServer then
                    local writer2 = networker.writer(networker.messageIds.ROUND_END)
                    networker.sendToServer(writer2:free())
                end
        
                talentPopupVisible = true
                old(...)
                talentScreen(false)
            end

            local oldparticle = getrenv()._G.SpawnParticle
            getrenv()._G.SpawnParticle = function(arg)
                local ent = getrenv()._G.Entities[arg.Source]
                if ent and ent.ResidentWhite then
                    arg.Color = Color3.new(0.7, 0.7, 0.7);
                end
                
                -- basically when an endless entity is spawned we move it to the server location, but the spawn beam doesn't follow that.
                -- this patch makes it so it does
                if ent and arg.Name == "Particle" and arg.Orientation and arg.Color.R == 1 and arg.Color.G == 1 and arg.Color.B == 1 and arg.Duration == 0.5 and arg.Model == "Block" then
                    arg.Position = Vector3.new(ent.RootPart.Position.X,-3,ent.RootPart.Position.Z)
                end
                return oldparticle(arg)
            end
                    
            local oldDamage = getrenv()._G.DamageRequest
            getrenv()._G.DamageRequest = function(...)
                local args = {...}
                args = args[1]
        
                if args.BypassHook then
                    return oldDamage(...)
                end
        
                local victimNetworkPlayer = networkPlayer.findByEntityId(args.Target)
                local victimNetworkEntity = networkEntity.findByEntityId(args.Target)
                
                local attackerNetworkPlayer = networkPlayer.findByEntityId(args.Source)
                local attackerNetworkEntity = networkEntity.findByEntityId(args.Source)

                local tryingToDamageLocal = args.Target == 1
                local localTryingToDamage = args.Source == 1

                local newArgs = util.deepCopy(args)
                newArgs.BypassHook = nil
                newArgs.WasDamageNetworked = nil
                local encodedDamage = util.encodeJson(newArgs)

                if localTryingToDamage and victimNetworkPlayer and not args.WasDamageNetworked then
                    print(`requesting damage on player {victimNetworkPlayer.username}`)
                    local writerPvp = networker.writer(networker.messageIds.PLAYER_PVP_DAMAGE)
                    writerPvp:write(victimNetworkPlayer.id)
                    writerPvp:write(encodedDamage)
                    networker.sendToServer(writerPvp:free())
                    return
                end

                if localTryingToDamage and victimNetworkEntity and not args.WasDamageNetworked then
                    print(`requesting damage on entity {victimNetworkEntity.id}`)
                    local writer = networker.writer(networker.messageIds.ENEMY_DAMAGE)
                    writer:write(victimNetworkEntity.id)
                    writer:write(encodedDamage)
                    networker.sendToServer(writer:free())
                    if networker.IsServer then
                        return oldDamage(...)
                    end
                    return
                end

                if victimNetworkPlayer and not args.WasDamageNetworked then
                    print("prevented network player from being damaged!")
                    return
                end

                if victimNetworkEntity and (attackerNetworkPlayer or (attackerNetworkEntity and not networker.IsServer)) and not args.WasDamageNetworked then
                    print("prevented network entity from being damaged")
                    return
                end

                if tryingToDamageLocal and attackerNetworkPlayer and not args.WasDamageNetworked then
                    print("prevented local damage!")
                    return
                end

                if tryingToDamageLocal and (attackerNetworkEntity or attackerNetworkPlayer) then
                    local writer = networker.writer(networker.messageIds.PLAYER_DAMAGE)
                    writer:write(attackerNetworkEntity and attackerNetworkEntity.id or attackerNetworkPlayer.id)
                    writer:write(attackerNetworkPlayer ~= nil)
                    writer:write(encodedDamage)
                    networker.sendToServer(writer:free())
                    print("got damaged, networking it!")
                end

                return oldDamage(...)
            end
        
            local oldInitGame = getrenv()._G.InitGame
            getrenv()._G.InitGame = function(...)
                return oldInitGame(...)
            end
        
            -- TODO: cleanup this function, its ass
            local oldSpawnCreature = getrenv()._G.SpawnCreature
            getrenv()._G.SpawnCreature = function(args)
                if args.BypassHook then
                    return oldSpawnCreature(args)
                end
                
                local preChangeName = args.Name
        
                local targetStage = networkStage.getStageByName(module.currentStage)
                if not targetStage then
                    print(`network stage {module.currentStage} doesn't exist.`)
                    return oldSpawnCreature(args)
                end
        
                local doesLocalExist = getrenv()._G.Entities[1] ~= nil
                if not doesLocalExist then
                    return oldSpawnCreature(args)
                end
        
                local localPlayerEntity = getrenv()._G.Entities[1]
                local isBoss = args.Name == lastCapturedEntityList.Bosses[1]
                local isDream = module.currentStage == "Dream"
                local isArena = module.currentStage == "Arena"
                local serverEntityData = targetStage.entities[networkStage.currentSpawnId]
                
                local replaceEnemy = serverEntityData and serverEntityData.name or nil
                if not isDream and not replaceEnemy then
                    warn(`stage data doesn't contain a result at index {networkStage.currentSpawnId}, not replacing {args.Name} on network stage {targetStage.fullName}!`)
                    return oldSpawnCreature(args)
                end
        
                local spawnCFrame = not isBoss and (serverEntityData and serverEntityData.position) or nil
                if replaceEnemy ~= nil and not getrenv()._G.CreatureDatabase[replaceEnemy] then
                    warn(`{replaceEnemy} does not exist in creature database!`)
                    return oldSpawnCreature(args)
                end
        
                local classes = {"Class01","Class0","Class1","Class2","Class3","Class4","Class5","Class6","Class7","Class8","Class9","Class5318008","Class999","ClassDuck","Class1338"}
                if isArena and table.find(classes,args.Name) then
                    args.Name = replaceEnemy
        
                    if spawnCFrame then
                        args.SpawnCFrame = CFrame.new(spawnCFrame)
                    end
        
                    local entityId = oldSpawnCreature(args)
                    local entity = getrenv()._G.Entities[entityId]

                    local oldTalentFunctions = util.deepCopy(entity.TalentFunctions)
                    entity.TalentFunctions = {} -- prevent hours from applying talents to this entity

                    local predictedNetworkId = networkStage.currentSpawnId
                    networkStage.currentSpawnId += 1

                    entity.Stats.MaxHealth[1] *= arenaHealthMultiplierBalancing
                    entity.Stats.MaxHealth[2] *= arenaHealthMultiplierBalancing
                    entity.Resources.Health = entity.Stats.MaxHealth[1]
        
                    -- THIS IS VERY VITAL CODE DO NOT REMOVE IT, EVEN IF ITS BAD
                    local sdb = getrenv()._G.ScriptDatabase
                    task.spawn(function()
                        for i = 1,10 do
                            task.wait()
                        end
        
                        if networker.IsServer then
                            warn("FULLY REPLACED AI")
                            for name,values in getrenv()._G.Multiplayers[entity.Name].Stored do
                                local finalValues = {}
                                for index,_ in values do
                                    table.insert(finalValues,index)
                                end
                                entity.Stored[name] = finalValues[math.random(1,#finalValues)]
                            end
                            entity.ProcessAI = sdb.Multiplayer.ProcessAI
                        end

                        -- run our own talent logic
                        entity.TalentFunctions = oldTalentFunctions

                        local highestTalents = 0
                        for _,player in networkPlayer.getPlayers() do
                            local count = player:GetTalentCount()
                            if count > highestTalents then
                                highestTalents = count
                            end
                        end
                        
                        local localTalentCount = 0
                        for _,talent in getrenv()._G.Entities[1].Talents do
                            if talent then
                                localTalentCount += 1
                            end
                        end
                        
                        if localTalentCount > highestTalents then
                            highestTalents = localTalentCount
                        end

                        local remainingTalents = {}
                        for i = 1, 24 do
                            table.insert(remainingTalents, i);
                        end

                        entity.Talents = {}

                        if not networker.IsServer then
                            local foundEntity = networkEntity.getEntityById(predictedNetworkId)
                            if foundEntity ~= nil then
                                foundEntity:ApplyNetworkedTalents()
                            else
                                warn(`network entity doesn't exist!, cannot apply networked talents!`)
                            end
                        else
                            print(`giving enemy {predictedNetworkId}, {highestTalents} talents`)

                            for _ = 1,highestTalents do
                                local count = #remainingTalents
                                if count <= 0 then
                                    break
                                end
                                local index = math.random(1,count)
                                local talentIndex = remainingTalents[index]

                                entity.Talents[talentIndex] = true
                                if entity.TalentFunctions[talentIndex] then
                                    entity.TalentFunctions[talentIndex]()
                                end

                                print(`gave enemy {predictedNetworkId} talent {talentIndex}`)

                                local writer = networker.writer(networker.messageIds.ENEMY_TALENT_GAINED)
                                writer:write(predictedNetworkId)
                                writer:write(talentIndex)
                                networker.sendToServer(writer:free())
                                
                                table.remove(remainingTalents,index)
                            end
                        end
                    end)
        
                    networkEntity.setEntityData(replaceEnemy,predictedNetworkId,entity,spawnCFrame)
            
                    warn("arena enemy was replaced!")
                    return entityId
                end
        
                if isArena then
                    return oldSpawnCreature(args)
                end
        
                local mapEntityTable = getrenv()._G.GameState == "Combat" and lastCapturedEntityList.Entities or {"Dummy","DummyWalk"}
                if not isArena and not isDream and not isBoss and not table.find(mapEntityTable,args.Name) then
                    warn(`not replacing entity [{args.Name} -> {replaceEnemy}] as they weren't spawned by the game, the game entity list is`)
                    for index,value in mapEntityTable do
                        warn(index,value)
                    end
                    return oldSpawnCreature(args)
                end
        
                if isDream and args.Name == getrenv()._G.Class then
                    local entities = getrenv()._G.Entities

                    local alive = 0
                    local spawnClone = function(templateEntity,networkId)
                        alive += 1

                        local newId = oldSpawnCreature({
                            Name = templateEntity.Name,
                            SpawnCFrame = CFrame.new(Vector3.new(0, templateEntity.RootPart and templateEntity.RootPart.Position.Y or templateEntity.Position.Y, 0)),
                            IsBoss = true,
                            HealthBar = 2,
                            DamageTeam = 2,
                            Pvp = true,
                            Multiplayer = true,
                        });

                        local ent = entities[newId]

                        for v27, v28 in pairs(entities[newId].Parts) do
                            v28.Color = entities[newId].PartProps[v27][1];
                            v28.Transparency = entities[newId].PartProps[v27][4];
                            v28.Size = entities[newId].PartProps[v27][6];
                        end;

                        for _, v30 in pairs(entities[newId].Status) do
                            v30.Id = newId;
                        end;

                        for index,talent in templateEntity.Talents do
                            if not talent then
                                continue
                            end
                            ent.Talents[index] = true
                            if ent.TalentFunctions[index] then
                                ent.TalentFunctions[index]()
                            end
                        end

                        entities[newId].Targetable = true;
                        entities[newId].IsBoss = true;
                        entities[newId].StoredIndex = 60;
                        entities[newId].PendingState = "";
                        entities[newId].Stored = {};
                        entities[newId].AITimer = 0;
                        entities[newId].Player = false;
                        for v31, _ in pairs(getrenv()._G.AIControl.Stored) do
                            entities[newId].Stored[v31] = 1;
                        end;

                        local v33 = math.clamp(entities[newId].Stats.MaxHealth[2], 50, 200) * 1.5;
                        v33 = math.clamp(v33 / (networkPlayer.getCount() + 1),50,math.huge)
                        entities[newId].Stats.MaxHealth[2] = v33;
                        entities[newId].Stats.MaxHealth[1] = v33;
                        entities[newId].Resources.Health = entities[newId].Stats.MaxHealth[1];
                        
                        local sdb = getrenv()._G.ScriptDatabase
                        for name,values in getrenv()._G.Multiplayers[entities[newId].Name].Stored do
                            local finalValues = {}
                            for index,_ in values do
                                table.insert(finalValues,index)
                            end
                            entities[newId].Stored[name] = finalValues[math.random(1,#finalValues)]
                        end
                        entities[newId].ProcessAI = sdb.Multiplayer.ProcessAI

                        -- more code from hours, i don't know how to trigger ResidentWhite so i just stole the code that changes the color
                        for v40, v41 in pairs(ent.Parts) do
                            ent.PartProps[v40][1] = Color3.new(0.7, 0.7, 0.7);
                            v41.Color = ent.PartProps[v40][1];
                            if ent.PartProps[v40][2] ~= Enum.Material.ForceField then
                                ent.PartProps[v40][2] = "Neon";
                                v41.Material = ent.PartProps[v40][2];
                            end;
                        end;

                        ent.ResidentWhite = true

                        local oldDie = entities[newId].DieBase
                        entities[newId].DieBase = function(...)
                            alive -= 1
                            return oldDie(...)
                        end

                        networkEntity.new(ent.Name,networkId + 100,ent.DamageTeam,ent)
                        warn(`created a clone of {networkId} actual id: {networkId + 100}`)
                    end
        
                    for _,p in networkPlayer.getPlayers() do
                        -- create a fake entity so even if they are dead it still summons a clone
                        local faketalents = {}
                        for _,talent in p.talents do
                            faketalents[talent] = true
                        end

                        spawnClone({
                            Name = p.entityId,
                            Talents = faketalents,
                            Position = Vector3.new(0,3,0)
                        },p.id)
                    end
                    spawnClone(localPlayerEntity,localPlayerModule.localPlayerData.smallId)
        
                    args.HealthBar = 1
                    local entityId = oldSpawnCreature(args)
                    local entity = getrenv()._G.Entities[entityId]
        
                    local loops = {}
                    table.insert(loops,rs.Heartbeat:Connect(function(dt)
                        entity.RootPart.CFrame = CFrame.new(0,1000,0)
                        entity.RootPart.Velocity = Vector3.zero

                        if entities[entityId] == nil then
                            warn("cause dead")
                            for _,loop in loops do
                                loop:Disconnect()
                            end
                            return
                        end

                        if entity.Dead == false then
                            entity.Resources.Health = alive > 0 and 9999 or 0
                        end
                    end))
                    warn("dream clone spawned, spawning our own!")
        
                    return entityId
                end
        
                local onlySync = {"Residentc","Residenta"}
                if isDream and not table.find(onlySync,args.Name) then
                    warn(`dream spawned {args.Name}`)

                    local entId = oldSpawnCreature(args)
                    local ent = getrenv()._G.Entities[entId]

                    local balancing = 0.5
                    ent.Stats.MaxHealth[1] *= balancing
                    ent.Stats.MaxHealth[2] *= balancing
                    ent.Resources.Health = ent.Stats.MaxHealth[1]

                    return entId
                end
        
                warn(`replaced enemy {args.Name} with server enemy {replaceEnemy}, isboss: {args.IsBoss}`)
                args.Name = replaceEnemy
                if spawnCFrame then
                    args.SpawnCFrame = CFrame.new(spawnCFrame)
                end
        
                -- adds the different colored boss noobs back (at the end of stage 3)
                if table.find(targetStage.name,"Lagoon") and replaceEnemy == "Noob" then
                    local pair = {
                        Water = "Super",
                        Black = "Guest",
                        Lagoon = "Noob",
                    }
                    replaceEnemy = pair[getrenv()._G.CurrentStage]
                    args.Name = replaceEnemy
                end
        
                local entityId = oldSpawnCreature(args)
                local entity = getrenv()._G.Entities[entityId]

                local bossHealthMultiplierBalancing = 0.7
                entity.Stats.MaxHealth[1] *= isBoss and bossHealthMultiplierBalancing or healthMultiplierBalancing
                entity.Stats.MaxHealth[2] *= isBoss and bossHealthMultiplierBalancing or healthMultiplierBalancing
                entity.Resources.Health = entity.Stats.MaxHealth[1]
                
                networkEntity.setEntityData(replaceEnemy,networkStage.currentSpawnId,entity,spawnCFrame)
        
                networkStage.currentSpawnId += 1
                return entityId
            end
        
            local talentOld = getrenv()._G.TalentChosen
            getrenv()._G.TalentChosen = function(...)
                local args = {...}
                talentScreen(true)
        
                iHaveChosen = args[1]
                local writer = networker.writer(networker.messageIds.TALENT_CHOSEN)
                writer:write(args[1])
                networker.sendToServer(writer:free())
        
                task.spawn(function()
                    repeat
                        task.wait()
                    until getrenv()._G.Map ~= nil
        
                    lastCapturedEntityList = {
                        Entities = table.clone(getrenv()._G.Map.Enemies),
                        Bosses = table.clone(getrenv()._G.Map.Bosses),
                    }
                end)
            end
        
            local oldAddTalent = getrenv()._G.AddTalent
            getrenv()._G.AddTalent = function(talentIndex)
                table.insert(localPlayerModule.talentsToAdd,talentIndex)
                return oldAddTalent(talentIndex)
            end
        
            networker.bindFunctionToMessage(networker.messageIds.TALENT_POPUP_START,function(endless)
                if not getrenv()._G.Map or not getrenv()._G.Map.Map then
                    return
                end
                if getrenv()._G.Map.Map.Name ~= "SandyBlue" then
                    return -- extremely weird this should never happen
                end
                
                print(`Talent popup start, {endless and "endless" or "normal"}`)
                local localEntity = getrenv()._G.Entities[1]
        
                if localEntity == nil then
                    return
                end
        
                alreadyNetworked = true
        
                localEntity.Character[game.Players.LocalPlayer.Name].HumanoidRootPart.CFrame = CFrame.new(0,0,endless and 100 or -100)
            end)
        
            local oldGameEnd = getrenv()._G.EndGame
            getrenv()._G.EndGame = function(...)
                if module.justDied then
                    print("just died, not game end")
                    getrenv()._G.GameEnd = false
                    module.justDied = false
                    return
                end
                print("GAME END DISPOSING OF ENEMIES")
        
                local playerClassWriter = networker.writer(networker.messageIds.PLAYER_CLASS)
                playerClassWriter:write(true)
                networker.sendToServer(playerClassWriter:free())
        
                lastCapturedEntityList = sandyBlueEntityList
        
                if networker.IsServer then
                    local writer = networker.writer(networker.messageIds.GAME_END)
                    networker.sendToServer(writer:free())
                end
        
                local writer5 = networker.writer(networker.messageIds.TALENT_CLEAR)
                networker.sendToServer(writer5:free())
        
                --networkEntity.disposeAll()
                return oldGameEnd(...)
            end
        
            local lastState = getrenv()._G.GameState
            rs.Heartbeat:Connect(function()
                module.currentStage = getCurrentStage()
                lastState = getrenv()._G.GameState
        
                if networker.connected then
                    getrenv()._G.TimeEnabled = false

                    if getrenv()._G.GameState == "Combat" then
                        game.Players.LocalPlayer.PlayerGui.AllGui.AllFrame.Special.Visible = false
                        game.Players.LocalPlayer.PlayerGui.AllGui.AllFrame.Health.AnchorPoint = Vector2.new(0,0.5)
                        game.Players.LocalPlayer.PlayerGui.AllGui.AllFrame.Health.Position = UDim2.new(0,0,0.5,0)
                    end
                end
                if getrenv()._G.GameState ~= "Combat" then
                    networkStage.resetSpawnId()
                end

                local allAreChosen = true
                local playerCount = 0
                for _,player in networkPlayer.getPlayers() do
                    playerCount += 1
                    if player.chosenCard ~= true then
                        allAreChosen = false
                    end
                end
                if playerCount > 0 then
                    getrenv()._G.Pause = false
                end

                if talentPopupVisible and not allAreChosen and iHaveChosen then
                    talentui.Instruction.RichText = true
                    talentui.Instruction.Text = "Waiting for others."
                    for _,player in networkPlayer.getPlayers() do
                        local color = player.chosenCard and "rgb(0,255,0)" or "rgb(255,0,0)"
                        talentui.Instruction.Text ..= `\n<font color="{color}">{player.username}</font>`
                    end
                end

                if talentPopupVisible and allAreChosen and iHaveChosen then
                    setthreadidentity(2)
        
                    talentOld(iHaveChosen)
                    talentScreen(false)
                    
                    for _,player in networkPlayer.getPlayers() do
                        -- this stops players from loading then getting interpolated to their updated position (it just looks ugly)
                        player.position = Vector3.zero
                        player.rotation = 0
                        player.predictionVelocity = Vector3.zero
                        player.predictionRVelocity = 0
                        player.positionAtNetworked = player.position
                        player.interpolatedPosition = player.position
                        player.health = 10000 -- just in case they die upon spawning for some reason
                        player.chosenCard = false
                    end
                    
                    talentPopupVisible = false
                    iHaveChosen = nil
        
                    setthreadidentity(8)
                end
        
                if getrenv()._G.Map and getrenv()._G.Map.Map and getrenv()._G.Map.Map.Name ~= "" then
                    lastMapName = getrenv()._G.Map.Map.Name
                end
            end)
        end)
    end
}

return module
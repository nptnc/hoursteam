﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;

namespace Proxy.Network.Messages {
    /// <summary>
    /// when a talent is selected, it is sent to other clients so they know they can continue
    /// </summary>
    public class TALENT_CHOSEN : NetworkMessage {
        public override byte MessageId() {
            return MessageIds.TALENT_CHOSEN;
        }

        public override void RobloxReceived(RobloxReader data) {
            int talentName = data.ReadInt();

            NetworkWriter writer = new NetworkWriter();
            writer.Write((byte)talentName);

            NetworkSender.SendToServer(MessageId(),SendType.Reliable,writer.Create());
        }

        public override void ReceivedServer(SteamId by,byte[] data) {
            NetworkReader reader = new NetworkReader(data);
            byte talentName = reader.ReadByte();

            Player? player = PlayerManager.GetPlayerBySteamId(by);
            if (player == null) {
                return;
            }

            NetworkWriter writer = new NetworkWriter();
            writer.Write(player.smallId);
            writer.Write(talentName);

            player.hasSelectedTalent = true;

            NetworkSender.SendToClients(MessageId(), writer.Create(), SendType.Reliable, new ulong[] { by });
        }

        public override void ReceivedClient(byte[] data) {
            NetworkReader reader = new NetworkReader(data);
            byte id = reader.ReadByte();
            byte talentName = reader.ReadByte();

            Player? player = PlayerManager.GetPlayerByByteId(id);
            if (player == null) {
                return;
            }

            Console.WriteLine($"{player.robloxUserId} chose talent {talentName}");

            RobloxWriter writer = new(MessageId());
            writer.WriteString(id.ToString());
            writer.WriteString(talentName.ToString());

            NetworkSender.SendToRoblox(writer);
        }
    }
}

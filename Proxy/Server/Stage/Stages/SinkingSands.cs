﻿using Proxy.Hours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Server.Stage.Stages {
    public class SinkingSands : Stage {
        public override void CreateStageData() {
            stageData = new() {
                new EntityStageData("Dummy",new Vector3(0,0,-10)),
                new EntityStageData("DummyWalk",new Vector3(0,0,10)),
            };
        }

        public override HoursStageIds StageId() {
            return HoursStageIds.sinkingSands;
        }

        public override string[] Name() {
            return new[] { "SandyBlue" };
        }
    }
}

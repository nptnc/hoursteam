﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;

namespace Proxy.Network.Messages {
    /// <summary>
    /// when the game ends, this message is sent
    /// this clears the players talent table and respawns their entity so they no longer have any talents
    /// </summary>
    public class TALENT_CLEAR : NetworkMessage {
        public override byte MessageId() {
            return MessageIds.TALENT_CLEAR;
        }

        public override void RobloxReceived(RobloxReader data) {
            NetworkSender.SendToServer(MessageId(),SendType.Reliable,new byte[] { });
        }

        public override void ReceivedServer(SteamId by,byte[] data) {
            Player? player = PlayerManager.GetPlayerBySteamId(by);
            if (player == null) {
                return;
            }

            Console.WriteLine($"{player.robloxUserId} cleared talents [server]");

            NetworkWriter writer = new NetworkWriter();
            writer.Write(player.smallId);

            NetworkSender.SendToClients(MessageId(), writer.Create(), SendType.Reliable, new ulong[] { by });
        }

        public override void ReceivedClient(byte[] data) {
            NetworkReader reader = new NetworkReader(data);
            byte id = reader.ReadByte();

            Player? player = PlayerManager.GetPlayerByByteId(id);
            if (player == null) {
                return;
            }

            Console.WriteLine($"{player.robloxUserId} cleared talents");

            RobloxWriter writer = new(MessageId());
            writer.WriteString(id.ToString());

            NetworkSender.SendToRoblox(writer);
        }
    }
}

﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;

namespace Proxy.Network.Messages
{
    /// <summary>
    /// local entities are entities spawned by the game and their entity id is replaced with the corresponding "server entity id"
    /// </summary>
    public class ENEMY_UPDATE : NetworkMessage
    {
        private const float positionCap = 250;
        private const float velocityCap = 300;
        private const float rotationCap = 180; // roblox euler angles is between -180 and 180

        public override byte MessageId()
        {
            return MessageIds.ENEMY_UPDATE;
        }

        public override void RobloxReceived(RobloxReader data)
        {
            int entityId = data.ReadInt();
            Vector3 position = data.ReadVector3();
            float rotation = data.ReadFloat();
            Vector3 velocity = data.ReadVector3();
            float rotVelocity = data.ReadFloat();
            Vector2 moveDir = data.ReadVector2();

            NetworkWriter writer = new();
            writer.Write((ushort)entityId);
            writer.Write((short)(position.X / positionCap * short.MaxValue));
            writer.Write((short)(position.Y / positionCap * short.MaxValue));
            writer.Write((short)(position.Z / positionCap * short.MaxValue));
            writer.Write((short)(rotation / rotationCap * short.MaxValue));
            writer.Write((short)(velocity.X / velocityCap * short.MaxValue));
            writer.Write((short)(velocity.Y / velocityCap * short.MaxValue));
            writer.Write((short)(velocity.Z / velocityCap * short.MaxValue));
            writer.Write(rotVelocity);
            writer.Write(moveDir);

            NetworkSender.SendToServer(MessageId(), SendType.Unreliable, writer.Create());
        }

        public override void ReceivedServer(SteamId by, byte[] data)
        {
            if (by != SteamClient.SteamId)
                return; // they arent the host

            NetworkSender.SendToClients(MessageId(), data, SendType.Unreliable, new ulong[] { by });
        }

        public override void ReceivedClient(byte[] data)
        {
            NetworkReader reader = new NetworkReader(data);
            ushort id = reader.ReadUShort();
            float positionX = (float)reader.ReadShort() / short.MaxValue * positionCap;
            float positionY = (float)reader.ReadShort() / short.MaxValue * positionCap;
            float positionZ = (float)reader.ReadShort() / short.MaxValue * positionCap;
            float rotation = (float)reader.ReadShort() / short.MaxValue * rotationCap;
            float velocityX = (float)reader.ReadShort() / short.MaxValue * velocityCap;
            float velocityY = (float)reader.ReadShort() / short.MaxValue * velocityCap;
            float velocityZ = (float)reader.ReadShort() / short.MaxValue * velocityCap;
            float rotVelocity = reader.ReadFloat();
            Vector2 moveDir = reader.ReadVector2();

            RobloxWriter writer = new(MessageId());
            writer.WriteString(id.ToString());
            writer.WriteVector3(new Vector3(positionX, positionY, positionZ));
            writer.WriteString(rotation.ToString());
            writer.WriteVector3(new Vector3(velocityX, velocityY, velocityZ));
            writer.WriteString(rotVelocity.ToString());
            writer.WriteVector2(moveDir);

            NetworkSender.SendToRoblox(writer);
        }
    }
}
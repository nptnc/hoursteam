﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;

namespace Proxy.Network.Messages {
    /// <summary>
    /// for player attacks, also known as inputs
    /// </summary>
    public class PLAYER_INPUT_FUNCTION : NetworkMessage {
        public override byte MessageId() {
            return MessageIds.PLAYER_INPUT_FUNCTION;
        }

        // higher means loss of precision which i guess is fine
        float positionCap = 250;
        float rotationCap = 180; // roblox euler angles is between -180 and 180
        float cameraPositionCap = 20;

        public override void RobloxReceived(RobloxReader data) {
            string input = data.ReadString();
            Vector3 position = data.ReadVector3();
            float rotation = data.ReadFloat();
            Vector3 camposition = data.ReadVector3();
            Vector3 camrotation = data.ReadVector3();
            Vector2 moveDir = data.ReadVector2();
            moveDir = Vector2.Normalize(moveDir);

            NetworkWriter writer = new NetworkWriter();
            writer.Write((short)(position.X / positionCap * short.MaxValue));
            writer.Write((short)(position.Y / positionCap * short.MaxValue));
            writer.Write((short)(position.Z / positionCap * short.MaxValue));
            writer.Write((sbyte)(camposition.X / cameraPositionCap * sbyte.MaxValue));
            writer.Write((sbyte)(camposition.Y / cameraPositionCap * sbyte.MaxValue));
            writer.Write((sbyte)(camposition.Z / cameraPositionCap * sbyte.MaxValue));
            writer.Write((short)(rotation / rotationCap * short.MaxValue));
            writer.Write((short)(camrotation.X / rotationCap * short.MaxValue));
            writer.Write((short)(camrotation.Y / rotationCap * short.MaxValue));
            writer.Write((short)(camrotation.Z / rotationCap * short.MaxValue));
            writer.Write((short)(moveDir.X * short.MaxValue));
            writer.Write((short)(moveDir.Y * short.MaxValue));
            writer.Write(input);

            NetworkSender.SendToServer(MessageId(), SendType.Reliable, writer.Create());
            // fml fml fml fml fml fml fml fml fml fml fml fml fml fml
        }

        public override void ReceivedServer(SteamId by, byte[] data) {
            NetworkReader reader = new(data);
            short positionX = reader.ReadShort();
            short positionY = reader.ReadShort();
            short positionZ = reader.ReadShort();
            short campositionX = reader.ReadShort();
            short campositionY = reader.ReadShort();
            short campositionZ = reader.ReadShort();
            short rotation = reader.ReadShort();
            short camrotationX = reader.ReadShort();
            short camrotationY = reader.ReadShort();
            short camrotationZ = reader.ReadShort();
            short moveDirX = reader.ReadShort();
            short moveDirY = reader.ReadShort();
            string input = reader.ReadString();

            Player? player = PlayerManager.GetPlayerBySteamId(by);
            if (player == null)
                return;

            Logger.Log($"{player.robloxUserId} {input}");

            NetworkWriter writer = new();
            writer.Write(positionX);
            writer.Write(positionY);
            writer.Write(positionZ);
            writer.Write(campositionX);
            writer.Write(campositionY);
            writer.Write(campositionZ);
            writer.Write(rotation);
            writer.Write(camrotationX);
            writer.Write(camrotationY);
            writer.Write(camrotationZ);
            writer.Write(moveDirX);
            writer.Write(moveDirY);
            writer.Write(player.smallId);
            writer.Write(input);

            NetworkSender.SendToClients(MessageId(), writer.Create(), SendType.Reliable, new ulong[] { by });
        }

        public override void ReceivedClient(byte[] data) {
            NetworkReader reader = new(data);
            float positionX = (float)reader.ReadShort() / short.MaxValue * positionCap;
            float positionY = (float)reader.ReadShort() / short.MaxValue * positionCap;
            float positionZ = (float)reader.ReadShort() / short.MaxValue * positionCap;
            float camPositionX = (float)reader.ReadShort() / short.MaxValue * cameraPositionCap;
            float camPositionY = (float)reader.ReadShort() / short.MaxValue * cameraPositionCap;
            float camPositionZ = (float)reader.ReadShort() / short.MaxValue * cameraPositionCap;
            float rotation = (float)reader.ReadShort() / short.MaxValue * rotationCap;
            float camRotationX = (float)reader.ReadShort() / short.MaxValue * rotationCap;
            float camRotationY = (float)reader.ReadShort() / short.MaxValue * rotationCap;
            float camRotationZ = (float)reader.ReadShort() / short.MaxValue * rotationCap;
            float moveDirX = (float)reader.ReadShort() / short.MaxValue;
            float moveDirY = (float)reader.ReadShort() / short.MaxValue;
            byte smallId = reader.ReadByte();
            string input = reader.ReadString();

            RobloxWriter writer = new(MessageId());
            writer.WriteString(smallId.ToString());
            writer.WriteString(input);
            writer.WriteVector3(new Vector3(positionX, positionY, positionZ));
            writer.WriteString(rotation.ToString());
            writer.WriteVector3(new Vector3(camPositionX, camPositionY, camPositionZ));
            writer.WriteVector3(new Vector3(camRotationX, camRotationY, camRotationZ));
            writer.WriteVector2(Vector2.Normalize(new Vector2(moveDirX, moveDirY)));

            NetworkSender.SendToRoblox(writer);
        }
    }
}

﻿using Proxy.Hours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Server.Stage {
    public class StageManager {
        public static List<Stage> stages { get; private set; } = new();

        public static Stage? GetStageByEnum(HoursStageIds stageId) {
            foreach (Stage stage in stages) {
                if (stage.StageId() == stageId)
                    return stage;
            }
            return null;
        }

        public static void RegisterTypes() {
            if (stages.Count > 0) {
                return;
            }
            foreach (Type type in Assembly.GetExecutingAssembly().GetTypes()) {
                if (type.IsSubclassOf(typeof(Stage))) {
                    Stage? stage = Activator.CreateInstance(type) as Stage;
                    if (stage == null)
                        continue;
                    stages.Add(stage);
                    stage.CreateStageData();

                    string stageName = "";
                    for (int i = 0; i < stage.Name().Length; i++) {
                        string targetName = stage.Name()[i];
                        if (i == stage.Name().Length-1) {
                            stageName += targetName;
                            continue;
                        }
                        stageName += targetName + ", ";
                    }
                    Logger.Log($"registered {type.Name} as stage with valid maps: {stageName}", ConsoleColor.DarkYellow);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Proxy {
    public class RobloxReader {
        private string[] seperations;
        private int offset;

        public RobloxReader(string read) {
            seperations = read.Split(Main.seperator[0]);
        }

        public string Next() {
            string value = seperations[offset];
            offset++;
            return value;
        }

        public bool HasMoreData() {
            if (offset < seperations.Length) {
                return true;
            }
            return false;
        }

        public bool ReadBool() {
            bool value = Next() == "1";
            return value;
        }

        public Vector3 ReadVector3() {
            string value = seperations[offset];
            offset++;
            string[] parts = value.Split('_');
            return new Vector3(float.Parse(parts[0]), float.Parse(parts[1]), float.Parse(parts[2]));
        }

        public Vector2 ReadVector2() {
            string value = seperations[offset];
            offset++;
            string[] parts = value.Split('_');
            return new Vector2(float.Parse(parts[0]), float.Parse(parts[1]));
        }

        public int ReadInt() {
            int value = int.Parse(Next());
            return value;
        }

        public float ReadFloat() {
            float value = float.Parse(Next());
            return value;
        }

        public ulong ReadUlong() {
            ulong value = ulong.Parse(Next());
            return value;
        }

        public string ReadString() {
            return Next();
        }
    }
}

﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using Steamworks.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Network.Messages {
    /// <summary>
    /// verbatim
    /// definition: "in exactly the same words as were used originally."
    /// nptnc said verbatim, 'Player PVP Damage'
    /// </summary>
    public class PLAYER_PVP_DAMAGE : NetworkMessage {
        public override byte MessageId() {
            return MessageIds.PLAYER_PVP_DAMAGE;
        }

        public override void RobloxReceived(RobloxReader data) {
            byte target = (byte)data.ReadInt();
            string encodedDamageJson = data.ReadString();

            NetworkWriter writer = new();
            writer.Write(target);
            writer.Write(encodedDamageJson);

            NetworkSender.SendToServer(MessageId(), SendType.Reliable, writer.Create());
        }

        public override void ReceivedServer(SteamId by, byte[] data) {
            Player? player = PlayerManager.GetPlayerBySteamId(by);
            if (player == null)
                return;

            NetworkReader reader = new(data);
            byte target = reader.ReadByte();
            string damageJson = reader.ReadString();

            Player? targetPlayer = PlayerManager.GetPlayerByByteId(target);
            if (targetPlayer == null)
                return;

            Logger.Log($"{player.robloxUserId} damaged {targetPlayer.robloxUserId} {damageJson}");

            NetworkWriter writer = new();
            writer.Write(player.smallId);
            writer.Write(damageJson);

            NetworkSender.SendToClient(MessageId(), targetPlayer.steamId, SendType.Reliable, writer.Create());
        }

        public override void ReceivedClient(byte[] data) {
            NetworkReader reader = new(data);
            byte playerId = reader.ReadByte();
            string damageJson = reader.ReadString();

            Player? player = PlayerManager.GetPlayerByByteId(playerId);
            if (player == null)
                return;

            Logger.Log($"{player.robloxUserId} is damaging you {damageJson}");

            RobloxWriter writer = new(MessageId());
            writer.WriteString(playerId.ToString());
            writer.WriteString(damageJson);

            NetworkSender.SendToRoblox(writer);
        }
    }
}

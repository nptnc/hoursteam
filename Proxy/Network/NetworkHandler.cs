﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using Steamworks.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Network {
    internal class NetworkHandler {
        internal static SocketSteamServer? socket;
        internal static SocketSteamClient? clientSocket;
        internal static Lobby? lobby;

        internal static List<NetworkMessage> messages = new List<NetworkMessage>();

        internal static bool IsServer {
            get {
                return socket != null;
            }
            private set { }
        }

        internal static void RegisterMessages() {
            try {
                foreach (Type type in Assembly.GetExecutingAssembly().GetTypes()) {
                    if (type.IsSubclassOf(typeof(NetworkMessage))) {
                        NetworkMessage? message = Activator.CreateInstance(type) as NetworkMessage;
                        if (message == null)
                            continue;
                        messages.Add(message);
                        Logger.Log($"registered {type.Name} as message {message.MessageId()}",ConsoleColor.Yellow);
                    }
                }
            } catch(Exception c) {
                Console.WriteLine(c.ToString());
            }

            SteamFriends.OnGameLobbyJoinRequested += (Lobby lobby, SteamId steamid) => {
                if (clientSocket != null) {
                    clientSocket.Close();
                    clientSocket = null;
                    Console.WriteLine("closing client...");
                }
                if (socket != null) {
                    StopServer();
                }

                Console.WriteLine("joining!");

                ConnectTo(steamid);
            };
        }

        public class SocketSteamServer : SocketManager {
            public Dictionary<SteamId,Connection> connectedSteamIds = new Dictionary<SteamId, Connection>();
            
            public override void OnConnected(Connection connection, ConnectionInfo info) {
                base.OnConnected(connection, info);
                Console.WriteLine($"client connected to steam socket");
            }

            public override void OnConnecting(Connection connection, ConnectionInfo info) {
                base.OnConnecting(connection, info);
            }

            public override void OnDisconnected(Connection connection, ConnectionInfo info) {
                KeyValuePair<SteamId,Connection>? pair = connectedSteamIds.FirstOrDefault(key => key.Value == connection);

                // i hate this nesting
                if (pair != null) {
                    Player? player = PlayerManager.GetPlayerBySteamId(pair.Value.Key);
                    if (player != null) {
                        NetworkWriter writer = new();
                        writer.Write(player.smallId);

                        NetworkSender.SendToClients(MessageIds.DISCONNECT, writer.Create(), SendType.Reliable, new ulong[] { pair.Value.Key });
                    }
                    connectedSteamIds.Remove(pair.Value.Key);
                }

                base.OnDisconnected(connection, info);
            }

            public override void OnMessage(Connection connection, NetIdentity identity, IntPtr data, int size, long messageNum, long recvTime, int channel) {
                if (!connectedSteamIds.ContainsKey(identity.SteamId)) {
                    connectedSteamIds.Add(identity.SteamId, connection);
                }

                byte[] byteData = new byte[size];
                Marshal.Copy(data, byteData, 0, size);

                byte messageId = byteData[0];
                byte[] actualData = new byte[byteData.Length - 1];

                for (int i = 1; i < byteData.Length; i++) {
                    actualData[i - 1] = byteData[i];
                }

                HandleMessage(messageId, actualData, true, identity.SteamId);

                base.OnMessage(connection, identity, data, size, messageNum, recvTime, channel);
            }
        }

        public class SocketSteamClient : ConnectionManager {
            public override void OnConnected(ConnectionInfo info) {
                base.OnConnected(info);

                socket?.connectedSteamIds.Add(SteamClient.SteamId,clientSocket!.Connection);
                Console.WriteLine("successfully connected (steam client)");

                RobloxWriter writer = new(MessageIds.CONNECTED_TO_STEAM);
                NetworkSender.SendToRoblox(writer);
            }

            public override void OnConnecting(ConnectionInfo info) {
                base.OnConnecting(info);
            }

            public override void OnDisconnected(ConnectionInfo info) {
                base.OnDisconnected(info);
            }

            public override void OnMessage(IntPtr data, int size, long messageNum, long recvTime, int channel) {
                byte[] byteData = new byte[size];
                Marshal.Copy(data, byteData, 0, size);

                byte messageId = byteData[0];
                byte[] actualData = new byte[byteData.Length - 1];

                for (int i = 1; i < byteData.Length; i++) {
                    actualData[i - 1] = byteData[i];
                }

                HandleMessage(messageId, actualData, false);
                base.OnMessage(data, size, messageNum, recvTime, channel);
            }
        }

        internal static void HandleMessage(int messageId, byte[] actualData, bool server, ulong steamId = 0) {
            foreach (NetworkMessage message in messages) {
                if (message.MessageId() != messageId) {
                    continue;
                }
                if (server) {
                    message.ReceivedServer(steamId, actualData);
                    continue;
                }
                message.ReceivedClient(actualData);
            }
        }

        internal static void ConnectTo(SteamId steamId) {
            clientSocket = SteamNetworkingSockets.ConnectRelay<SocketSteamClient>(steamId);
            Console.WriteLine($"client connecting to {steamId}");
        }

        internal static void Disconnect() {
            clientSocket?.Close();
            clientSocket = null;

            PlayerManager.players = new();
            Player.globalSmallId = 0;
            EntityManager.entities = new();
            StopServer();
            Game.ReturnedToMenu();

            Logger.Log($"Completely reset proxy.",ConsoleColor.Yellow);
        }

        internal static async void StartServer() {
            if (socket != null)
                return;
            if (clientSocket != null)
                return;
            Logger.Log($"Created steam server!", ConsoleColor.Red);
            socket = SteamNetworkingSockets.CreateRelaySocket<SocketSteamServer>();

            lobby = await SteamMatchmaking.CreateLobbyAsync(8);
            lobby.Value.SetPublic();
            if (lobby != null) {
                Logger.Log("setting lobby metadata!");
                LobbyMetadata.Create((Lobby)lobby);
            }

            ConnectTo(SteamClient.SteamId);
        }

        private static void StopServer() {
            socket?.Close();
            socket = null;

            lobby?.Leave();
            lobby = null;
        }

        internal static void Update() {
            socket?.Receive(32);
            clientSocket?.Receive(32);
        }
    }
}

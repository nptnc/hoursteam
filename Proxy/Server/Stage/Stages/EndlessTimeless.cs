﻿using Proxy.Hours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Server.Stage.Stages {
    public class EndlessTimeless : Stage {
        public List<string> randomEnemies = new();

        public override HoursStageIds StageId() {
            return HoursStageIds.endlessTimeless;
        }

        private static int GetEntityCount(int stageId) {
            // like man... why...
            int entityCount = 1;
            if (stageId >= 6) {
                entityCount++;
                stageId -= 6;
                while (stageId >= 5) {
                    stageId -= 5;
                    entityCount++;
                }
            }
            return entityCount;
        }


        // TODO: find out if the same enemies can spawn
        public override void CreateStageData() {
            randomEnemies = HoursEntityIds.getAll().ToList();

            List<string> remainingRandomEnemies = new();
            foreach (string enemy in randomEnemies) {
                remainingRandomEnemies.Add(enemy);
            }

            // on stage 6, 11, 16, etc entity cap increases

            /*for (int i = 0; i < 30; i++) {
                Logger.Log($"the entity count when level is {i+1} {GetEntityCount(i+1)}");
            }*/

            int entityCount = GetEntityCount(Game.endlessLevelId);
            Logger.Log($"entity count is {entityCount}");

            stageData = new();
            for (int i = 0; i < entityCount; i++) {
                int randomIndex = new Random().Next(0, remainingRandomEnemies.Count - 1);
                stageData.Add(new EntityStageData(remainingRandomEnemies[randomIndex]));
                remainingRandomEnemies.RemoveAt(randomIndex);
                remainingRandomEnemies.Sort();
            }
        }

        public override string[] Name() {
            return new[] { "Arena" };
        }
    }
}

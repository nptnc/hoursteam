﻿using Minesweeper.Multiplayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Network.DataTypes {
    public class PlayerRegisterData : NetworkDataType {
        public byte smallId;
        public ulong steamId;
        public ulong robloxId;

        public PlayerRegisterData(byte[]? data = null) {
            if (data == null)
                return;
            Decode(data);
        }

        public override void Decode(byte[] data) {
            NetworkReader reader = new(data);
            robloxId = reader.ReadUlong();
            steamId = reader.ReadUlong();
            smallId = reader.ReadByte();
        }

        public override NetworkWriter Encode() {
            NetworkWriter writer = new();
            writer.Write(robloxId);
            writer.Write(steamId);
            writer.Write(smallId);
            return writer;
        }
    }
}

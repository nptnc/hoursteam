local stage = {}

local stages = {}
stage.__index = stage

stage.new = function(stageName,stageEntityArray,fullStageName)
    local self = setmetatable({},stage)
    self.name = stageName
    self.fullName = fullStageName
    self.entities = stageEntityArray
    table.insert(stages,self)
    return self
end

local module = {}

module.setModules = function(framework)
    networker = framework("Networker")
end

module.currentSpawnId = 1

module.__module = {
    [2] = function()
        networker.OnConnectionSuccess:Connect(function()
            networker.bindFunctionToMessage(networker.messageIds.ENEMY_STAGE_DATA,function(stageName,amountOfEntities,...)
                amountOfEntities = tonumber(amountOfEntities)
                
                local args = {...}
                local offset = 1
                local entities = {}
                for i = 1,amountOfEntities do
                    local name = args[offset]
                    local positionString = string.split(args[offset+1],"_")
                    local position = Vector3.new(positionString[1],positionString[2],positionString[3])
                    table.insert(entities,{
                        name = name,
                        position = position,
                    })
                    warn(`{stageName} stage data entity {i} is {name} at {position}`)
                    offset += 2
                end
        
                for _,otherStage in stages do
                    if otherStage.fullName ~= stageName then
                        continue
                    end
                    otherStage.entities = entities
                    return
                end
                stage.new(string.split(stageName,"."),entities,stageName)
            end)
        end)
    end,
}

module.resetSpawnId = function()
    module.currentSpawnId = 1
end

module.getStageByName = function(stageName)
    for _,s in stages do
        if table.find(s.name,stageName) then
            return s
        end
    end
end

module.getStages = function()
    return stages
end

return module
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Server {
    public class EntityManager {
        public static List<Entity> entities = new();

        public static Entity? GetEntityById(ushort id) {
            foreach (Entity player in entities) {
                if (player.id == id)
                    return player;
            }
            return null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Server.Stage {
    public class EntityStageData {
        public string entityType = "";
        public Vector3 position;
        public Vector3 rotation;
        
        public EntityStageData(string name,Vector3? pos = null) {
            if (pos == null) {
                pos = Stage.GetRandomPosition();
            }
            entityType = name;
            position = (Vector3)pos;
            rotation = Vector3.Zero;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Network.Messages {
    /// <summary>
    /// (start, stop) steam server!
    /// </summary>
    public class LEAVE_STEAM : NetworkMessage {
        public override byte MessageId() {
            return MessageIds.LEAVE_STEAM;
        }

        public override void RobloxReceived(RobloxReader data) {
            Logger.Log($"Left steam server!",ConsoleColor.Red);
            NetworkHandler.Disconnect();

            RobloxWriter writer = new(MessageId());
            NetworkSender.SendToRoblox(writer);
        }
    }
}

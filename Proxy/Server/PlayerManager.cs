﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Server {
    public class PlayerManager {
        public static List<Player> players = new List<Player>();

        public static Player? GetPlayerByUserId(ulong userId) {
            foreach (Player player in players) {
                if (player.robloxUserId == userId)
                    return player;
            }
            return null;
        }

        public static Player? GetPlayerBySteamId(ulong steamId) {
            foreach (Player player in players) {
                if (player.steamId == steamId)
                    return player;
            }
            return null;
        }

        public static Player? GetPlayerByByteId(byte steamId) {
            foreach (Player player in players) {
                if (player.smallId == steamId)
                    return player;
            }
            return null;
        }
    }
}

﻿using Minesweeper.Multiplayer;
using Proxy.Server;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Steamworks.Data;
using System.Numerics;

namespace Proxy.Network.Messages
{
    /// <summary>
    /// safe guard against health desync
    /// </summary>
    public class ENEMY_DEATH : NetworkMessage
    {
        public override byte MessageId()
        {
            return MessageIds.ENEMY_DEATH;
        }

        public override void RobloxReceived(RobloxReader data)
        {
            int entityId = data.ReadInt();

            NetworkWriter writer = new();
            writer.Write((ushort)entityId);

            NetworkSender.SendToServer(MessageId(), SendType.Reliable, writer.Create());
        }

        public override void ReceivedServer(SteamId by, byte[] data)
        {
            NetworkReader reader = new(data);
            ushort entityId = reader.ReadUShort();

            Entity? entity = EntityManager.GetEntityById(entityId);
            if (entity != null)
                entity.Died();

            NetworkSender.SendToClients(MessageId(), data, SendType.Reliable, new ulong[] { by });
        }

        public override void ReceivedClient(byte[] data)
        {
            NetworkReader reader = new(data);
            ushort id = reader.ReadUShort();

            RobloxWriter writer = new(MessageId());
            writer.WriteString(id.ToString());

            NetworkSender.SendToRoblox(writer);
        }
    }
}